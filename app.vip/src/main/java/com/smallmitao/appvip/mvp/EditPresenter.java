package com.smallmitao.appvip.mvp;

import com.smallmitao.appvip.bean.EditModel;
import com.smallmitao.appvip.bean.ShopModel;
import com.smallmitao.appvip.mvp.contract.EditContract;
import com.smallmitao.libbase.di.scope.FragmentScope;
import com.smallmitao.libbase.http.RetrofitHelper;
import com.smallmitao.libbase.http.appinterface.HttpInter;
import com.smallmitao.libbase.manager.app.BaseUserManager;
import com.smallmitao.libbase.mvp.RxPresenter;
import com.smallmitao.libbase.util.Toastor;
import com.zhouyou.http.callback.SimpleCallBack;
import com.zhouyou.http.exception.ApiException;
import com.zhouyou.http.model.HttpParams;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * date: 2018/8/10
 * @author Jackshao
 *
 */

@FragmentScope
public class EditPresenter extends RxPresenter<EditContract.View> implements EditContract.Presenter {

  private RetrofitHelper mHelper;
  private List<EditModel> modelList = new ArrayList<>();

  @Inject
  public EditPresenter(RetrofitHelper retrofitHelper) {
    this.mHelper = retrofitHelper;
  }

  @Override
  public void requestEdit(String page,String pageSize) {
    HttpParams httpParams = new HttpParams();
//    httpParams.put(HttpInter.GoodsGetList.IS_ON_SALE,"1");
//    httpParams.put(HttpInter.GoodsGetList.REVIEW_STATUS,"3");
    httpParams.put(HttpInter.GoodsGetList.PAGE,page);
    httpParams.put(HttpInter.GoodsGetList.PAGE_SIZE,pageSize);
    addDisposable(mHelper
      .postRequest(HttpInter.GoodsGetList.PATH)
      .params(httpParams)
      .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getToken())
      .execute(new SimpleCallBack<ShopModel>() {

        @Override
        public void onError(ApiException e) {
          Toastor.showToast(e.getMessage());
        }

        @Override
        public void onSuccess(ShopModel o) {
          getView().getShopModel(true,o.getList());
        }
      }));
  }
}
