package com.smallmitao.appvip.mvp.contract;

import com.smallmitao.appvip.bean.EditModel;
import com.smallmitao.appvip.bean.ShopModel;
import com.smallmitao.libbase.mvp.BasePresenter;
import com.smallmitao.libbase.mvp.BaseView;

import java.util.List;

/**
 *
 * @author Jackshao
 */
public interface EditContract {

    interface View extends BaseView {
        void getShopModel(boolean isOk, List<ShopModel.ListBean> mList);
    }

    interface Presenter extends BasePresenter<View> {
        /**
         * 请求
         */
        void requestEdit(String page,String pageSize);
    }
}
