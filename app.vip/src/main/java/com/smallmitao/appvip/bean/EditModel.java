package com.smallmitao.appvip.bean;

/**
 *
 * @author Jackshao
 * @date 2019/1/3
 */

public class EditModel {

  private String mTime;
  private String mTitle;

  public EditModel(String mTime, String mTitle) {
    this.mTime = mTime;
    this.mTitle = mTitle;
  }

  public String getTime() {
    return mTime;
  }

  public void setTime(String mTime) {
    this.mTime = mTime;
  }

  public String getTitle() {
    return mTitle;
  }

  public void setTitle(String mTitle) {
    this.mTitle = mTitle;
  }
}
