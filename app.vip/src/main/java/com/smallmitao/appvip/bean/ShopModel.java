package com.smallmitao.appvip.bean;


import java.util.List;

/**
 *
 * @author Jackshao
 * @date 2019/1/2
 */

public class ShopModel {


  private List<ListBean> list;

  public List<ListBean> getList() {
    return list;
  }

  public void setList(List<ListBean> list) {
    this.list = list;
  }

  public static class ListBean {

    private int goods_id;
    private String goods_name;
    private String market_price;
    private String shop_price;
    private String goods_img_url;

    public int getGoods_id() {
      return goods_id;
    }

    public void setGoods_id(int goods_id) {
      this.goods_id = goods_id;
    }

    public String getGoods_name() {
      return goods_name;
    }

    public void setGoods_name(String goods_name) {
      this.goods_name = goods_name;
    }

    public String getMarket_price() {
      return market_price;
    }

    public void setMarket_price(String market_price) {
      this.market_price = market_price;
    }

    public String getShop_price() {
      return shop_price;
    }

    public void setShop_price(String shop_price) {
      this.shop_price = shop_price;
    }

    public String getGoods_img_url() {
      return goods_img_url;
    }

    public void setGoods_img_url(String goods_img_url) {
      this.goods_img_url = goods_img_url;
    }

  }

}
