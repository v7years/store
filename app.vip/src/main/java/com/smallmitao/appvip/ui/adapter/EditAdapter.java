package com.smallmitao.appvip.ui.adapter;


import android.graphics.Paint;
import android.widget.ImageView;
import android.widget.TextView;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.smallmitao.appvip.R;
import com.smallmitao.appvip.bean.ShopModel;
import com.smallmitao.libbase.util.GlideUtils;

/**
 * @author Jackshao
 * @date 2018/9/18
 */

public class EditAdapter extends BaseQuickAdapter<ShopModel.ListBean,BaseViewHolder> {


  public EditAdapter() {
    super(R.layout.item_edit);
  }

  @Override
  protected void convert(BaseViewHolder helper, ShopModel.ListBean item) {
    TextView priceTV = helper.getView(R.id.mark_price_tv);
    helper.setText(R.id.name_tv,"(事例) "+item.getGoods_name());
    helper.setText(R.id.price_tv,"￥"+item.getShop_price());
    helper.setText(R.id.mark_price_tv,"原价 ￥"+item.getMarket_price());
    priceTV.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
    ImageView head_img_iv = helper.getView(R.id.img_iv);
    GlideUtils.getInstance().loadNormalImage(head_img_iv,item.getGoods_img_url());
  }

}
