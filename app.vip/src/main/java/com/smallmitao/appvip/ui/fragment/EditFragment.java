package com.smallmitao.appvip.ui.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.smallmitao.appvip.R;
import com.smallmitao.appvip.R2;
import com.smallmitao.appvip.bean.ShopModel;
import com.smallmitao.appvip.di.componet.DaggerEditComponent;
import com.smallmitao.appvip.di.componet.EditComponent;
import com.smallmitao.appvip.di.module.EditModule;
import com.smallmitao.appvip.mvp.EditPresenter;
import com.smallmitao.appvip.mvp.contract.EditContract;
import com.smallmitao.appvip.ui.adapter.EditAdapter;
import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.manager.ShopStatusManager;
import com.smallmitao.libbase.mvp.BaseMvpFragment;
import com.smallmitao.libbase.util.Toastor;
import com.smallmitao.libbridge.router.BridgeRouter;

import java.util.List;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author Jackshao
 * @date 2019/1/2
 */

@Route(path = BridgeRouter.PAGE_FRAGMENT_VIP, name = "编辑")
public class EditFragment extends BaseMvpFragment<EditPresenter> implements EditContract.View, OnRefreshListener, OnLoadMoreListener {

  @BindView(R2.id.title_tv)
  TextView mTitleTv;
  @BindView(R2.id.toolbar)
  Toolbar mToolbar;
  @BindView(R2.id.title_rightTv)
  TextView mTitleRightTv;
  @BindView(R2.id.recycle_view)
  RecyclerView mRecycleView;
  @BindView(R2.id.refresh_layout)
  SmartRefreshLayout mRefreshLayout;

  @Inject
  EditAdapter mEditAdapter;

  @Inject
  LinearLayoutManager mLinearLayoutManager;

  private int mPage = 1;
  private int mSize = 20;
  private View mNotDataView;

  @Override
  protected int getLayout() {
    return R.layout.fragment_vip;
  }

  @Override
  protected void initImmersionBar() {
    super.initImmersionBar();
    mImmersionBar.titleBar(mToolbar).init();
  }

  @Override
  protected void initInject() {
    getFragmentComponent().inject(this);
  }

  protected EditComponent getFragmentComponent() {
    return DaggerEditComponent.builder()
      .baseAppComponent(BaseApp.getInstance().getBaseAppComponent())
      .editModule(new EditModule(this))
      .build();
  }

  @Override
  protected void initData() {
    mTitleTv.setText("编辑");
    mTitleRightTv.setText("添加商品");
    mNotDataView = getLayoutInflater().inflate(R.layout.app_no_data_view, (ViewGroup) mRecycleView.getParent(), false);
    mTitleRightTv.setVisibility(View.VISIBLE);
    mRefreshLayout.setEnableLoadMore(true);
    mRefreshLayout.setOnRefreshListener(this);
    mRefreshLayout.setOnLoadMoreListener(this);
    mRecycleView.setLayoutManager(mLinearLayoutManager);
    mRecycleView.setAdapter(mEditAdapter);
    mPresenter.requestEdit(String.valueOf(mPage), String.valueOf(mSize));
    mEditAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
      @Override
      public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        Toastor.showToast("示例商品仅供预览，请先申请注册商家资质，敬请谅解。");
      }
    });
  }

  @Override
  public void getShopModel(boolean isOk, List<ShopModel.ListBean> mList) {
    if (isOk) {
      mRefreshLayout.finishRefresh(true);
      setData(mList);
    } else {
      if (mPage == 1) {
        mRefreshLayout.finishRefresh(false);
      } else {
        mRefreshLayout.finishLoadMore(false);
      }
    }
  }

  @Override
  public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
    mPage++;
    mPresenter.requestEdit(String.valueOf(mPage), String.valueOf(mSize));
  }

  @Override
  public void onRefresh(@NonNull RefreshLayout refreshLayout) {
    mPage = 1;
    mPresenter.requestEdit(String.valueOf(mPage), String.valueOf(mSize));
  }

  private void setData(List<ShopModel.ListBean> data) {

    final int size = data == null ? 0 : data.size();
    boolean isRefresh = mPage == 1;
    if (isRefresh) {
      mEditAdapter.replaceData(data);
    } else {
      if (size > 0) {
        mEditAdapter.addData(data);
      }
    }
    if (size < mSize) {
      //第一页如果不够一页就不显示没有更多数据布局
      mRefreshLayout.finishLoadMoreWithNoMoreData();
      if (size == 0) {
        mEditAdapter.setEmptyView(mNotDataView);
      } else {
        if (mPage > 1) {
          Toastor.showToast("没有更多数据了");
        }
      }
    } else {
      mRefreshLayout.finishLoadMore();
    }
  }


  @OnClick(R2.id.title_rightTv)
  public void onClick() {
    ARouter.getInstance()
      .build(Uri.parse(BridgeRouter.PAGE_ACTIVITY_ADD_SHOP))
      .navigation();
//    int status = ShopStatusManager.getInstance().getShopStatus().getStatus();
//    if (status==1 || status==3){
//      ARouter.getInstance()
//        .build(Uri.parse(BridgeRouter.PAGE_ACTIVITY_ADD_SHOP))
//        .navigation();
//    }else {
//      Toastor.showToast("您目前还不是小蜜淘商家,请申请再添加");
//    }
  }
}
