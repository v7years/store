package com.smallmitao.appvip.di.componet;

import android.app.Activity;

import com.smallmitao.appvip.di.module.EditModule;
import com.smallmitao.appvip.ui.fragment.EditFragment;
import com.smallmitao.libbase.di.component.BaseAppComponent;
import com.smallmitao.libbase.di.scope.FragmentScope;

import dagger.Component;


/**
 * @author Jackshao
 */
@FragmentScope
@Component(dependencies = BaseAppComponent.class, modules = EditModule.class)
public interface EditComponent {

    Activity getActivity();

    void inject(EditFragment editFragment);
}
