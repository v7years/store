package com.smallmitao.appvip.di.module;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.smallmitao.appvip.ui.adapter.EditAdapter;
import com.smallmitao.libbase.di.scope.FragmentScope;

import dagger.Module;
import dagger.Provides;


/**
 * @author Jackshao
 */
@Module
public class EditModule {

    private Fragment mFragment;

    public EditModule(Fragment mFragment) {
        this.mFragment = mFragment;
    }

    @FragmentScope
    @Provides
    Activity provideActivity() {
        return mFragment.getActivity();
    }

    @FragmentScope
    @Provides
    LinearLayoutManager provideLinearLayoutManager(){
        return new LinearLayoutManager(mFragment.getActivity());
    }

    @FragmentScope
    @Provides
    EditAdapter provideEditAdapter(){
        return new EditAdapter();
    }

}
