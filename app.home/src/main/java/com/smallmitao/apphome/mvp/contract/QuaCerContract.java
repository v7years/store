package com.smallmitao.apphome.mvp.contract;

import android.os.Bundle;

import com.smallmitao.libbase.mvp.BasePresenter;
import com.smallmitao.libbase.mvp.BaseView;

/**
 * @author Jackshao
 */
public interface QuaCerContract {

    interface View extends BaseView {
        void getFile(String path);
    }

    interface Presenter extends BasePresenter<View> {
        void requestPic();
        void lubanPic(String path);
        void uploadFile(String path);
        void requestApplyShop(Bundle bundle);
    }
}
