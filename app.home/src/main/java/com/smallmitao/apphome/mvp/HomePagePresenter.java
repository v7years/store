package com.smallmitao.apphome.mvp;

import android.app.Activity;
import com.smallmitao.apphome.bean.OrderListBean;
import com.smallmitao.apphome.mvp.contract.HomePageContract;
import com.smallmitao.libbase.di.scope.FragmentScope;
import com.smallmitao.libbase.http.RetrofitHelper;
import com.smallmitao.libbase.http.appinterface.HttpInter;
import com.smallmitao.libbase.manager.ShopStatusManager;
import com.smallmitao.libbase.manager.app.BaseUserManager;
import com.smallmitao.libbase.mvp.RxPresenter;
import com.smallmitao.libbase.util.Toastor;
import com.smallmitao.libbridge.router.bean.ShopStatusBean;
import com.zhouyou.http.callback.SimpleCallBack;
import com.zhouyou.http.exception.ApiException;
import javax.inject.Inject;

/**
 * date: 2018/8/10
 * @author Jackshao
 * 首页
 */

@FragmentScope
public class HomePagePresenter extends RxPresenter<HomePageContract.View> implements HomePageContract.Presenter {

  private RetrofitHelper mHelper;
  private Activity mActivity;


  @Inject
  public HomePagePresenter(Activity mActivity, RetrofitHelper retrofitHelper) {
    this.mHelper = retrofitHelper;
    this.mActivity = mActivity;
  }


  @Override
  public void requestShop() {
    addDisposable(mHelper
      .postRequest(HttpInter.ShopStatus.PATH)
      .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getToken())
      .execute(new SimpleCallBack<ShopStatusBean>() {

        @Override
        public void onError(ApiException e) {
        }

        @Override
        public void onSuccess(ShopStatusBean o) {
          getView().getShop(o.getStatus(),o.getReason());
          ShopStatusManager.getInstance().setShopStatus(o);
        }
      }));
  }

  @Override
  public void requestOrder(String page,String pageSize) {
    addDisposable(mHelper
      .getRequest(HttpInter.OrderList.PATH)
      .params(HttpInter.OrderList.status,"3")
      .params(HttpInter.OrderList.page,page)
      .params(HttpInter.OrderList.pagesize,pageSize)
      .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getToken())
      .execute(new SimpleCallBack<OrderListBean>() {

        @Override
        public void onError(ApiException e) {
          Toastor.showToast(e.getMessage());
          getView().getOrderList(false,null);
        }

        @Override
        public void onSuccess(OrderListBean o) {
          getView().getOrderList(true,o);
        }
      }));
  }

}
