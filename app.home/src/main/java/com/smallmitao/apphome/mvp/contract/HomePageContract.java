package com.smallmitao.apphome.mvp.contract;


import com.smallmitao.apphome.bean.OrderListBean;
import com.smallmitao.libbase.mvp.BasePresenter;
import com.smallmitao.libbase.mvp.BaseView;

/**
 * 首页
 * @author Jackshao
 */
public interface HomePageContract {

    interface View extends BaseView {
        void getShop(int status ,String reason);
        void getOrderList(boolean isOk, OrderListBean bean);
    }

    interface Presenter extends BasePresenter<View> {
        void requestShop();
        void requestOrder(String page,String pageSize);
    }
}
