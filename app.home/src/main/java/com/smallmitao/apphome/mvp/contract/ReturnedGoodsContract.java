package com.smallmitao.apphome.mvp.contract;


import com.smallmitao.apphome.bean.CateBean;
import com.smallmitao.libbase.mvp.BasePresenter;
import com.smallmitao.libbase.mvp.BaseView;

import java.util.List;

/**
 * @author Jackshao
 */
public interface ReturnedGoodsContract {

    interface View extends BaseView {
        void getCate(CateBean cateBean);
        void getCateList(List<CateBean.ListBean> list);
    }

    interface Presenter extends BasePresenter<View> {
        void request();
        void requestThh();
        void requestSpDw();
    }
}
