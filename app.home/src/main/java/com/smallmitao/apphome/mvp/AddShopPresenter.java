package com.smallmitao.apphome.mvp;

import android.app.Activity;
import android.support.annotation.NonNull;
import com.smallmitao.apphome.bean.AddShopBean;
import com.smallmitao.apphome.bean.MoreUplLoadBean;
import com.smallmitao.apphome.mvp.contract.AddShopContract;
import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.di.scope.ActivityScope;
import com.smallmitao.libbase.http.RetrofitHelper;
import com.smallmitao.libbase.http.api.BaseApiResult;
import com.smallmitao.libbase.http.appinterface.HttpInter;
import com.smallmitao.libbase.manager.app.BaseUserManager;
import com.smallmitao.libbase.mvp.RxPresenter;
import com.smallmitao.libbase.util.DialogUtil;
import com.smallmitao.libbase.util.FileUtil;
import com.smallmitao.libbase.util.LuBanUtils;
import com.smallmitao.libbase.util.Toastor;
import com.smallmitao.libbridge.router.bean.UpLoadBean;
import com.zhouyou.http.callback.SimpleCallBack;
import com.zhouyou.http.exception.ApiException;
import com.zhouyou.http.model.HttpParams;

import org.json.JSONArray;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import top.zibin.luban.Luban;

/**
 * date: 2018/8/10
 * @author Jackshao
 */

@ActivityScope
public class AddShopPresenter extends RxPresenter<AddShopContract.View> implements AddShopContract.Presenter {

  private RetrofitHelper mHelper;
  private CompositeDisposable mDisposable;
  private DialogUtil dialog ;
  private Activity mActivity;


  @Inject
  public AddShopPresenter(Activity mActivity,RetrofitHelper retrofitHelper) {
    this.mHelper = retrofitHelper;
    this.mActivity = mActivity;
    mDisposable = new CompositeDisposable();
  }

  @Override
  public void requestGys() {
    addDisposable(mHelper
      .postRequest(HttpInter.GetSupplierShop.PATH)
      .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getToken())
      .execute(new SimpleCallBack<AddShopBean>() {

        @Override
        public void onError(ApiException e) {
          Toastor.showToast(e.getMessage());
        }

        @Override
        public void onSuccess(AddShopBean o) {
          getView().getGys(o);
        }
      }));
  }

  @Override
  public void requestAdd(HttpParams httpParams) {
    addDisposable(mHelper
      .postRequest(HttpInter.GoodsAdd.PATH)
      .params(httpParams)
      .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getToken())
      .execute(new SimpleCallBack<BaseApiResult>() {

        @Override
        public void onError(ApiException e) {
          Toastor.showToast(e.getMessage());
        }

        @Override
        public void onSuccess(BaseApiResult o) {
          Toastor.showToast("添加商品成功");
          mActivity.finish();
        }
      }));
  }


  @Override
  public void detachView() {
    super.detachView();
    if (mDisposable!=null && !mDisposable.isDisposed()){
      mDisposable.dispose();
    }
  }

  /**
   * 多张照片上传
   */
  public void requestMoreImg(List<File> mFiles1,List<File> mFiles2){
    if (mFiles1.size()>0){
      onLuBanImage(1,mFiles1);
    }
    if (mFiles2.size()>0){
      onLuBanImage(2,mFiles2);
    }
  }


  /**
   * 压缩多张图片
   * @param mFiles
   */
  public void onLuBanImage(final int type,final List<File> mFiles){
    LuBanUtils.getInstance().withRx(mDisposable, mFiles, new LuBanUtils.OnLFinishListener() {
      @Override
      public void luBanFinish(final List<File> onResultFiles) {
        requestLoadUp(type,onResultFiles);
      }
    });
  }

  /**
   * 上传多张照片
   * @param mFiles
   */
  public void requestLoadUp(final int type,final List<File> mFiles) {
    if (mFiles.size()>0){
      new Thread() {
        @Override
        public void run() {
          upLoadUrl(type,mFiles);
        }
      }.start();
    }
  }

  public void upLoadUrl(final int type,List<File> mFiles){
    HttpParams httpParams = new HttpParams();
    for (File file : mFiles){
      httpParams.put("file[]",file,null);
    }
    httpParams.put(HttpInter.MoreUnloadImg.type,"goods");
    addDisposable(mHelper
      .postRequest(HttpInter.MoreUnloadImg.PATH)
      .params(httpParams)
      .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getToken())
      .execute(new SimpleCallBack<MoreUplLoadBean>() {

        @Override
        public void onError(ApiException e) {
          Toastor.showToast(e.getMessage()+",请重新选择上传");
        }

        @Override
        public void onSuccess(MoreUplLoadBean o) {
          getView().getMoreImg(type,o);
        }
      }));
  }


  /**
   * 上传封面
   * @param path
   */
  public void uploadFile(String path) {
    addDisposable(mHelper
      .postRequest(HttpInter.UploadImg.PATH)
      .params(HttpInter.UploadImg.TYPE,"shop")
      .params(HttpInter.UploadImg.NAME,new File(path),null)
      .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getToken())
      .execute(new SimpleCallBack<UpLoadBean>() {

        @Override
        public void onError(ApiException e) {
          Toastor.showToast(e.getMessage()+",请重新选择上传");
          if (dialog!=null){
            dialog.cancelDialog();
          }
        }

        @Override
        public void onSuccess(UpLoadBean o) {
          if (dialog!=null){
            dialog.cancelDialog();
          }
          Toastor.showToast("上传成功！");
          getView().getFmFile(o.getUrl());
        }
      }));
  }

  public void getLunBanPath(String path) {
    if (dialog == null){
      dialog = new DialogUtil(mActivity,"正在上传中...");
    }
    if (!dialog.isShowing()){
      dialog.showDialog();
    }
    final String newPath = FileUtil.createFile(BaseApp.getInstance());
    List<String> mList = new ArrayList<>();
    mList.add(path);
    addDisposable(Flowable.just(mList)
      .observeOn(Schedulers.io())
      .map(new Function<List<String>, List<File>>() {
        @Override public List<File> apply(@NonNull List<String> list) throws Exception {
          // 同步方法直接返回压缩后的文件
          return Luban.with(mActivity).load(list).setTargetDir(newPath).get();
        }
      })
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(new Consumer<List<File>>() {
        @Override
        public void accept(List<File> files) throws Exception {
          uploadFile(files.get(0).getAbsolutePath());
        }
      }));
  }



}
