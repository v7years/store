package com.smallmitao.apphome.mvp.contract;

import com.smallmitao.libbase.mvp.BasePresenter;
import com.smallmitao.libbase.mvp.BaseView;

/**
 * @author Jackshao
 */
public interface ApplyShopContract {

    interface View extends BaseView {
        void getFile(int type,String path);
    }

    interface Presenter extends BasePresenter<View> {
        void requestPic();
        void lubanPic(int type,String path);
        void uploadFile(int type,String path);
    }
}
