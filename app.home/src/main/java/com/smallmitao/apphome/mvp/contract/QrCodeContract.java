package com.smallmitao.apphome.mvp.contract;


import com.smallmitao.libbase.mvp.BasePresenter;
import com.smallmitao.libbase.mvp.BaseView;

/**
 * @author Jackshao
 */
public interface QrCodeContract {

    interface View extends BaseView {
        void getQrCode(String data);
    }

    interface Presenter extends BasePresenter<View> {
        void requestRqCode();
    }
}
