package com.smallmitao.apphome.mvp;

import com.smallmitao.apphome.bean.CateBean;
import com.smallmitao.apphome.mvp.contract.ReturnedGoodsContract;
import com.smallmitao.libbase.di.scope.ActivityScope;
import com.smallmitao.libbase.http.RetrofitHelper;
import com.smallmitao.libbase.http.appinterface.HttpInter;
import com.smallmitao.libbase.manager.app.BaseUserManager;
import com.smallmitao.libbase.mvp.RxPresenter;
import com.smallmitao.libbase.util.Toastor;
import com.zhouyou.http.callback.SimpleCallBack;
import com.zhouyou.http.exception.ApiException;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

/**
 * date: 2018/8/10
 * @author Jackshao
 */

@ActivityScope
public class ReturnedGoodsPresenter extends RxPresenter<ReturnedGoodsContract.View> implements ReturnedGoodsContract.Presenter {

  private RetrofitHelper mHelper;
  private List<CateBean.ListBean> data = new ArrayList<>();

  @Inject
  public ReturnedGoodsPresenter(RetrofitHelper retrofitHelper) {
    this.mHelper = retrofitHelper;
  }

  @Override
  public void request() {
    addDisposable(mHelper
      .getRequest(HttpInter.GetCate.PATH)
      .params(HttpInter.GetCate.cate_id,"1")
      .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getToken())
      .execute(new SimpleCallBack<CateBean>() {

        @Override
        public void onError(ApiException e) {
          Toastor.showToast(e.getMessage());
        }

        @Override
        public void onSuccess(CateBean o) {
          getView().getCate(o);
        }
      }));
  }

  @Override
  public void requestThh() {
    data.clear();
    data.add(new CateBean.ListBean(0,"仅支持换货，不支持退货",false));
    data.add(new CateBean.ListBean(1,"30天无忧退换货",false));
    data.add(new CateBean.ListBean(2,"特殊商品，不支持无理由退换货",false));
    getView().getCateList(data);
  }

  @Override
  public void requestSpDw() {
    data.clear();
    data.add(new CateBean.ListBean(0,"小蜜淘自营仓配送",false));
    data.add(new CateBean.ListBean(1,"平台商家直接发货",false));
    getView().getCateList(data);
  }

}
