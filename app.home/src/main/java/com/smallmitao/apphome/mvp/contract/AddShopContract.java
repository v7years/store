package com.smallmitao.apphome.mvp.contract;


import com.smallmitao.apphome.bean.AddShopBean;
import com.smallmitao.apphome.bean.MoreUplLoadBean;
import com.smallmitao.libbase.mvp.BasePresenter;
import com.smallmitao.libbase.mvp.BaseView;
import com.zhouyou.http.model.HttpParams;

/**
 * @author Jackshao
 */
public interface AddShopContract {

    interface View extends BaseView {
        void getGys(AddShopBean model);
        void getMoreImg(int type, MoreUplLoadBean model);
        void getFmFile(String path);
    }

    interface Presenter extends BasePresenter<View> {
        void requestGys();
        void requestAdd(HttpParams httpParams);
    }
}
