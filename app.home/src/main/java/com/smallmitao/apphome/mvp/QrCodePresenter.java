package com.smallmitao.apphome.mvp;

import com.smallmitao.apphome.bean.QrCodeBean;
import com.smallmitao.apphome.mvp.contract.QrCodeContract;
import com.smallmitao.libbase.di.scope.ActivityScope;
import com.smallmitao.libbase.http.RetrofitHelper;
import com.smallmitao.libbase.http.appinterface.HttpInter;
import com.smallmitao.libbase.manager.app.BaseUserManager;
import com.smallmitao.libbase.mvp.RxPresenter;
import com.smallmitao.libbase.util.Toastor;
import com.zhouyou.http.callback.SimpleCallBack;
import com.zhouyou.http.exception.ApiException;
import javax.inject.Inject;

/**
 * date: 2018/8/10
 * @author Jackshao
 * 首页
 */

@ActivityScope
public class QrCodePresenter extends RxPresenter<QrCodeContract.View> implements QrCodeContract.Presenter {

  private RetrofitHelper mHelper;


  @Inject
  public QrCodePresenter(RetrofitHelper retrofitHelper) {
    this.mHelper = retrofitHelper;
  }

  @Override
  public void requestRqCode() {
    addDisposable(mHelper
      .getRequest(HttpInter.StoreQrCode.PATH)
      .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getToken())
      .execute(new SimpleCallBack<QrCodeBean>() {

        @Override
        public void onError(ApiException e) {
          Toastor.showToast(e.getMessage());
        }

        @Override
        public void onSuccess(QrCodeBean o) {
          getView().getQrCode(o.getUrl());
        }
      }));
  }
}
