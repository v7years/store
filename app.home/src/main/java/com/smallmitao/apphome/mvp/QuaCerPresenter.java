package com.smallmitao.apphome.mvp;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.facade.callback.NavCallback;
import com.alibaba.android.arouter.launcher.ARouter;
import com.smallmitao.apphome.bean.ApplyIdBean;
import com.smallmitao.apphome.mvp.contract.QuaCerContract;
import com.smallmitao.apphome.ui.activity.ApplyShopActivity;
import com.smallmitao.apphome.ui.activity.HandIDCardActivity;
import com.smallmitao.libbase.base.AppManager;
import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.di.scope.ActivityScope;
import com.smallmitao.libbase.http.RetrofitHelper;
import com.smallmitao.libbase.http.api.BaseApiResult;
import com.smallmitao.libbase.http.appinterface.HttpInter;
import com.smallmitao.libbase.manager.app.BaseUserManager;
import com.smallmitao.libbase.mvp.RxPresenter;
import com.smallmitao.libbase.util.DialogUtil;
import com.smallmitao.libbase.util.FileUtil;
import com.smallmitao.libbase.util.Toastor;
import com.smallmitao.libbridge.router.BridgeRouter;
import com.smallmitao.libbridge.router.bean.UpLoadBean;
import com.zhouyou.http.callback.SimpleCallBack;
import com.zhouyou.http.exception.ApiException;
import com.zhouyou.http.model.HttpParams;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import top.zibin.luban.Luban;

/**
 * date: 2018/8/10
 * @author Jackshao
 * 首页
 */

@ActivityScope
public class QuaCerPresenter extends RxPresenter<QuaCerContract.View> implements QuaCerContract.Presenter {

  private RetrofitHelper mHelper;
  private Activity mActivity;
  private DialogUtil dialog ;


  @Inject
  public QuaCerPresenter(Activity mActivity, RetrofitHelper retrofitHelper) {
    this.mHelper = retrofitHelper;
    this.mActivity = mActivity;
  }

  @Override
  public void requestPic() {

  }

  @Override
  public void lubanPic(String path) {
    getLunBanPath(path);
  }

  @Override
  public void uploadFile(String path) {
    addDisposable(mHelper
      .postRequest(HttpInter.UploadImg.PATH)
      .params(HttpInter.UploadImg.TYPE,"shop")
      .params(HttpInter.UploadImg.NAME,new File(path),null)
      .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getToken())
      .execute(new SimpleCallBack<UpLoadBean>() {

        @Override
        public void onError(ApiException e) {
          Toastor.showToast(e.getMessage()+",请重新选择上传");
          if (dialog!=null){
            dialog.cancelDialog();
          }
        }

        @Override
        public void onSuccess(UpLoadBean o) {
          if (dialog!=null){
            dialog.cancelDialog();
          }
          Toastor.showToast("上传成功！");
          getView().getFile(o.getUrl());
        }
      }));
  }


  @Override
  public void requestApplyShop(Bundle bundle) {

    HttpParams httpParams = new HttpParams();
    httpParams.put(HttpInter.ApplyShop.real_name,bundle.getString("id_name"));
    httpParams.put(HttpInter.ApplyShop.identity_num,bundle.getString("id_card"));
    httpParams.put(HttpInter.ApplyShop.front_identity,bundle.getString("id_sfz_1"));
    httpParams.put(HttpInter.ApplyShop.back_identity,bundle.getString("id_sfz_2"));
    httpParams.put(HttpInter.ApplyShop.hold_identity,bundle.getString("id_sc_sfz"));
    httpParams.put(HttpInter.ApplyShop.business_licence,bundle.getString("id_yy_zz"));
    httpParams.put(HttpInter.ApplyShop.brand_name,bundle.getString("id_yy_zz_name"));
    httpParams.put(HttpInter.ApplyShop.type,"1");
    httpParams.put(HttpInter.ApplyShop.qq,bundle.getString("id_qq"));
    httpParams.put(HttpInter.ApplyShop.email,bundle.getString("id_email"));

    addDisposable(mHelper
      .postRequest(HttpInter.ApplyShop.PATH)
      .params(httpParams)
      .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getToken())
      .execute(new SimpleCallBack<ApplyIdBean>() {

        @Override
        public void onError(ApiException e) {
          Toastor.showToast(e.getMessage());
        }

        @Override
        public void onSuccess(ApplyIdBean o) {
          Toastor.showToast("资料提交成功，我们会尽快审核，春节期间可能延后，敬请谅解。");

          ARouter.getInstance()
            .build(Uri.parse(BridgeRouter.PAGE_ACTIVITY_APPLY))
            .withString("shop_id",o.getShop_sn())
            .navigation(mActivity, new NavCallback() {
              @Override
              public void onArrival(Postcard postcard) {
                AppManager.getInstance().finishActivity(ApplyShopActivity.class);
                AppManager.getInstance().finishActivity(HandIDCardActivity.class);
                mActivity.finish();
              }
            });
        }
      }));
  }


  public void getLunBanPath(String path) {
    if (dialog == null){
      dialog = new DialogUtil(mActivity,"正在上传中...");
    }
    if (!dialog.isShowing()){
      dialog.showDialog();
    }
    final String newPath = FileUtil.createFile(BaseApp.getInstance());
    List<String> mList = new ArrayList<>();
    mList.add(path);
    addDisposable(Flowable.just(mList)
      .observeOn(Schedulers.io())
      .map(new Function<List<String>, List<File>>() {
        @Override public List<File> apply(@NonNull List<String> list) throws Exception {
          // 同步方法直接返回压缩后的文件
          return Luban.with(mActivity).load(list).setTargetDir(newPath).get();
        }
      })
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(new Consumer<List<File>>() {
        @Override
        public void accept(List<File> files) throws Exception {
          uploadFile(files.get(0).getAbsolutePath());
        }
      }));
  }

}
