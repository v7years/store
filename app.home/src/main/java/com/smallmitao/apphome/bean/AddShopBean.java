package com.smallmitao.apphome.bean;

/**
 *
 * @author Jackshao
 * @date 2019/1/23
 */

public class AddShopBean {

  /**
   * suppliers_id : 8
   * suppliers_name : 上海百味林企业管理有限公司
   */

  private int suppliers_id;
  private String suppliers_name;

  public int getSuppliers_id() {
    return suppliers_id;
  }

  public void setSuppliers_id(int suppliers_id) {
    this.suppliers_id = suppliers_id;
  }

  public String getSuppliers_name() {
    return suppliers_name;
  }

  public void setSuppliers_name(String suppliers_name) {
    this.suppliers_name = suppliers_name;
  }
}
