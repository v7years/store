package com.smallmitao.apphome.bean;

/**
 *
 * @author Jackshao
 * @date 2019/1/23
 */

public class ApplyIdBean {

  /**
   * shop_sn : SH2019012417191026606
   * status : 0
   */

  private String shop_sn;
  private int status;

  public String getShop_sn() {
    return shop_sn;
  }

  public void setShop_sn(String shop_sn) {
    this.shop_sn = shop_sn;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }
}
