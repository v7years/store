package com.smallmitao.apphome.bean;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 *
 * @author Jackshao
 * @date 2019/1/23
 */

public class CateBean {

  private List<ListBean> list;


  public List<ListBean> getList() {
    return list;
  }

  public void setList(List<ListBean> list) {
    this.list = list;
  }


  public static class ListBean implements Parcelable{
    /**
     * cat_id : 15
     * cat_name : 电子书
     */

    private int cat_id;

    public ListBean(int cat_id, String cat_name, boolean isChoose) {
      this.cat_id = cat_id;
      this.cat_name = cat_name;
      this.isChoose = isChoose;
    }

    private String cat_name;
    private  boolean isChoose;

    public int getCat_id() {
      return cat_id;
    }

    public void setCat_id(int cat_id) {
      this.cat_id = cat_id;
    }

    public String getCat_name() {
      return cat_name;
    }

    public void setCat_name(String cat_name) {
      this.cat_name = cat_name;
    }

    public boolean isChoose() {
      return isChoose;
    }

    public void setChoose(boolean choose) {
      isChoose = choose;
    }

    @Override
    public int describeContents() {
      return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
      dest.writeInt(this.cat_id);
      dest.writeString(this.cat_name);
      dest.writeByte(this.isChoose ? (byte) 1 : (byte) 0);
    }

    public ListBean() {
    }


    protected ListBean(Parcel in) {
      this.cat_id = in.readInt();
      this.cat_name = in.readString();
      this.isChoose = in.readByte() != 0;
    }

    public static final Creator<ListBean> CREATOR = new Creator<ListBean>() {
      @Override
      public ListBean createFromParcel(Parcel source) {
        return new ListBean(source);
      }

      @Override
      public ListBean[] newArray(int size) {
        return new ListBean[size];
      }
    };
  }
}
