package com.smallmitao.apphome.bean;

/**
 *
 * @author Jackshao
 * @date 2019/1/23
 */

public class QrCodeBean {

  /**
   * url : http://api.smallmitao.com/app/shop/QRcode?shop_id=10
   */

  private String url;

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }
}
