package com.smallmitao.apphome.bean;

import java.util.List;

/**
 *
 * @author Jackshao
 * @date 2019/1/23
 */

public class OrderListBean {


  /**
   * current_page : 1
   * data : [{"order_id":136720,"buy_money":"0.00","coupons":"0.00","order_sn":"S2019011215003493193","add_time":"2019-01-12 15:00:34","user_id":2099,"order_status":1,"shipping_status":2,"pay_status":2,"suppliers_id":77,"goods_amount":"37.00","cost_amount":"0.00","shipping_fee":"0.00","surplus":"0.00","order_amount":"37.00","return_amount":"0.00","confirm_take_time":1547284548,"parent_id":123078,"jifendikou":"0.00","suppliers_name":"小蜜淘自营","order_info":[{"parent_id":136720,"order_id":136390,"goods":[{"rec_id":140125,"order_id":136390,"return_number":null,"goods_id":1335,"goods_name":"花香郁 米浆沐浴露 500ml","goods_number":1,"market_price":"37.00","goods_price":"37.00","goods_attr":"","shop_point":0,"goods_thumb":"data/gallery_album/540/thumb_img/1531725681475674904.jpg","suppliers_id":77,"comment_list":[]}]}],"total_point":"0.00","is_comment":0,"status":"待评论","status_type":4,"total_price":37,"real_pay":0,"consignee":"15669067169","consignee_phone":"15669067169","address_detail":"浙江省杭州市拱墅区测试"}]
   * first_page_url : http://develop.shopapi.smallmitao.com/order/orderlist?page=1
   * from : 1
   * last_page : 52
   * last_page_url : http://develop.shopapi.smallmitao.com/order/orderlist?page=52
   * next_page_url : http://develop.shopapi.smallmitao.com/order/orderlist?page=2
   * path : http://develop.shopapi.smallmitao.com/order/orderlist
   * per_page : 1
   * prev_page_url : null
   * to : 1
   * total : 52
   */

  private String current_page;
  private String first_page_url;
  private String from;
  private String last_page;
  private String last_page_url;
  private String next_page_url;
  private String path;
  private String per_page;
  private Object prev_page_url;
  private String to;
  private String total;
  private List<DataBean> data;

  public String getCurrent_page() {
    return current_page;
  }

  public void setCurrent_page(String current_page) {
    this.current_page = current_page;
  }

  public String getFirst_page_url() {
    return first_page_url;
  }

  public void setFirst_page_url(String first_page_url) {
    this.first_page_url = first_page_url;
  }

  public String getFrom() {
    return from;
  }

  public void setFrom(String from) {
    this.from = from;
  }

  public String getLast_page() {
    return last_page;
  }

  public void setLast_page(String last_page) {
    this.last_page = last_page;
  }

  public String getLast_page_url() {
    return last_page_url;
  }

  public void setLast_page_url(String last_page_url) {
    this.last_page_url = last_page_url;
  }

  public String getNext_page_url() {
    return next_page_url;
  }

  public void setNext_page_url(String next_page_url) {
    this.next_page_url = next_page_url;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public String getPer_page() {
    return per_page;
  }

  public void setPer_page(String per_page) {
    this.per_page = per_page;
  }

  public Object getPrev_page_url() {
    return prev_page_url;
  }

  public void setPrev_page_url(Object prev_page_url) {
    this.prev_page_url = prev_page_url;
  }

  public String getTo() {
    return to;
  }

  public void setTo(String to) {
    this.to = to;
  }

  public String getTotal() {
    return total;
  }

  public void setTotal(String total) {
    this.total = total;
  }

  public List<DataBean> getData() {
    return data;
  }

  public void setData(List<DataBean> data) {
    this.data = data;
  }

  public static class DataBean {
    /**
     * order_id : 136720
     * buy_money : 0.00
     * coupons : 0.00
     * order_sn : S2019011215003493193
     * add_time : 2019-01-12 15:00:34
     * user_id : 2099
     * order_status : 1
     * shipping_status : 2
     * pay_status : 2
     * suppliers_id : 77
     * goods_amount : 37.00
     * cost_amount : 0.00
     * shipping_fee : 0.00
     * surplus : 0.00
     * order_amount : 37.00
     * return_amount : 0.00
     * confirm_take_time : 1547284548
     * parent_id : 123078
     * jifendikou : 0.00
     * suppliers_name : 小蜜淘自营
     * order_info : [{"parent_id":136720,"order_id":136390,"goods":[{"rec_id":140125,"order_id":136390,"return_number":null,"goods_id":1335,"goods_name":"花香郁 米浆沐浴露 500ml","goods_number":1,"market_price":"37.00","goods_price":"37.00","goods_attr":"","shop_point":0,"goods_thumb":"data/gallery_album/540/thumb_img/1531725681475674904.jpg","suppliers_id":77,"comment_list":[]}]}]
     * total_point : 0.00
     * is_comment : 0
     * status : 待评论
     * status_type : 4
     * total_price : 37
     * real_pay : 0
     * consignee : 15669067169
     * consignee_phone : 15669067169
     * address_detail : 浙江省杭州市拱墅区测试
     */

    private String order_id;
    private String buy_money;
    private String coupons;
    private String order_sn;
    private String add_time;
    private String user_id;
    private String order_status;
    private String shipping_status;
    private String pay_status;
    private String suppliers_id;
    private String goods_amount;
    private String cost_amount;
    private String shipping_fee;
    private String surplus;
    private String order_amount;
    private String return_amount;
    private String confirm_take_time;
    private String parent_id;
    private String jifendikou;
    private String suppliers_name;
    private String total_point;
    private String is_comment;
    private String status;
    private String status_type;
    private String total_price;
    private String real_pay;
    private String consignee;
    private String consignee_phone;
    private String address_detail;
    private List<OrderInfoBean> order_info;

    public String getOrder_id() {
      return order_id;
    }

    public void setOrder_id(String order_id) {
      this.order_id = order_id;
    }

    public String getBuy_money() {
      return buy_money;
    }

    public void setBuy_money(String buy_money) {
      this.buy_money = buy_money;
    }

    public String getCoupons() {
      return coupons;
    }

    public void setCoupons(String coupons) {
      this.coupons = coupons;
    }

    public String getOrder_sn() {
      return order_sn;
    }

    public void setOrder_sn(String order_sn) {
      this.order_sn = order_sn;
    }

    public String getAdd_time() {
      return add_time;
    }

    public void setAdd_time(String add_time) {
      this.add_time = add_time;
    }

    public String getUser_id() {
      return user_id;
    }

    public void setUser_id(String user_id) {
      this.user_id = user_id;
    }

    public String getOrder_status() {
      return order_status;
    }

    public void setOrder_status(String order_status) {
      this.order_status = order_status;
    }

    public String getShipping_status() {
      return shipping_status;
    }

    public void setShipping_status(String shipping_status) {
      this.shipping_status = shipping_status;
    }

    public String getPay_status() {
      return pay_status;
    }

    public void setPay_status(String pay_status) {
      this.pay_status = pay_status;
    }

    public String getSuppliers_id() {
      return suppliers_id;
    }

    public void setSuppliers_id(String suppliers_id) {
      this.suppliers_id = suppliers_id;
    }

    public String getGoods_amount() {
      return goods_amount;
    }

    public void setGoods_amount(String goods_amount) {
      this.goods_amount = goods_amount;
    }

    public String getCost_amount() {
      return cost_amount;
    }

    public void setCost_amount(String cost_amount) {
      this.cost_amount = cost_amount;
    }

    public String getShipping_fee() {
      return shipping_fee;
    }

    public void setShipping_fee(String shipping_fee) {
      this.shipping_fee = shipping_fee;
    }

    public String getSurplus() {
      return surplus;
    }

    public void setSurplus(String surplus) {
      this.surplus = surplus;
    }

    public String getOrder_amount() {
      return order_amount;
    }

    public void setOrder_amount(String order_amount) {
      this.order_amount = order_amount;
    }

    public String getReturn_amount() {
      return return_amount;
    }

    public void setReturn_amount(String return_amount) {
      this.return_amount = return_amount;
    }

    public String getConfirm_take_time() {
      return confirm_take_time;
    }

    public void setConfirm_take_time(String confirm_take_time) {
      this.confirm_take_time = confirm_take_time;
    }

    public String getParent_id() {
      return parent_id;
    }

    public void setParent_id(String parent_id) {
      this.parent_id = parent_id;
    }

    public String getJifendikou() {
      return jifendikou;
    }

    public void setJifendikou(String jifendikou) {
      this.jifendikou = jifendikou;
    }

    public String getSuppliers_name() {
      return suppliers_name;
    }

    public void setSuppliers_name(String suppliers_name) {
      this.suppliers_name = suppliers_name;
    }

    public String getTotal_point() {
      return total_point;
    }

    public void setTotal_point(String total_point) {
      this.total_point = total_point;
    }

    public String getIs_comment() {
      return is_comment;
    }

    public void setIs_comment(String is_comment) {
      this.is_comment = is_comment;
    }

    public String getStatus() {
      return status;
    }

    public void setStatus(String status) {
      this.status = status;
    }

    public String getStatus_type() {
      return status_type;
    }

    public void setStatus_type(String status_type) {
      this.status_type = status_type;
    }

    public String getTotal_price() {
      return total_price;
    }

    public void setTotal_price(String total_price) {
      this.total_price = total_price;
    }

    public String getReal_pay() {
      return real_pay;
    }

    public void setReal_pay(String real_pay) {
      this.real_pay = real_pay;
    }

    public String getConsignee() {
      return consignee;
    }

    public void setConsignee(String consignee) {
      this.consignee = consignee;
    }

    public String getConsignee_phone() {
      return consignee_phone;
    }

    public void setConsignee_phone(String consignee_phone) {
      this.consignee_phone = consignee_phone;
    }

    public String getAddress_detail() {
      return address_detail;
    }

    public void setAddress_detail(String address_detail) {
      this.address_detail = address_detail;
    }

    public List<OrderInfoBean> getOrder_info() {
      return order_info;
    }

    public void setOrder_info(List<OrderInfoBean> order_info) {
      this.order_info = order_info;
    }

    public static class OrderInfoBean {
      /**
       * parent_id : 136720
       * order_id : 136390
       * goods : [{"rec_id":140125,"order_id":136390,"return_number":null,"goods_id":1335,"goods_name":"花香郁 米浆沐浴露 500ml","goods_number":1,"market_price":"37.00","goods_price":"37.00","goods_attr":"","shop_point":0,"goods_thumb":"data/gallery_album/540/thumb_img/1531725681475674904.jpg","suppliers_id":77,"comment_list":[]}]
       */

      private int parent_id;
      private int order_id;
      private List<GoodsBean> goods;

      public int getParent_id() {
        return parent_id;
      }

      public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
      }

      public int getOrder_id() {
        return order_id;
      }

      public void setOrder_id(int order_id) {
        this.order_id = order_id;
      }

      public List<GoodsBean> getGoods() {
        return goods;
      }

      public void setGoods(List<GoodsBean> goods) {
        this.goods = goods;
      }

      public static class GoodsBean {
        /**
         * rec_id : 140125
         * order_id : 136390
         * return_number : null
         * goods_id : 1335
         * goods_name : 花香郁 米浆沐浴露 500ml
         * goods_number : 1
         * market_price : 37.00
         * goods_price : 37.00
         * goods_attr :
         * shop_point : 0
         * goods_thumb : data/gallery_album/540/thumb_img/1531725681475674904.jpg
         * suppliers_id : 77
         * comment_list : []
         */

        private String rec_id;
        private String order_id;
        private Object return_number;
        private String goods_id;
        private String goods_name;
        private String goods_number;
        private String market_price;
        private String goods_price;
        private String goods_attr;
        private String shop_point;
        private String goods_thumb;
        private String suppliers_id;
        private String goods_brief;
        private List<?> comment_list;

        public String getRec_id() {
          return rec_id;
        }

        public void setRec_id(String rec_id) {
          this.rec_id = rec_id;
        }

        public String getOrder_id() {
          return order_id;
        }

        public void setOrder_id(String order_id) {
          this.order_id = order_id;
        }

        public Object getReturn_number() {
          return return_number;
        }

        public void setReturn_number(Object return_number) {
          this.return_number = return_number;
        }

        public String getGoods_id() {
          return goods_id;
        }

        public void setGoods_id(String goods_id) {
          this.goods_id = goods_id;
        }

        public String getGoods_name() {
          return goods_name;
        }

        public void setGoods_name(String goods_name) {
          this.goods_name = goods_name;
        }

        public String getGoods_number() {
          return goods_number;
        }

        public void setGoods_number(String goods_number) {
          this.goods_number = goods_number;
        }

        public String getMarket_price() {
          return market_price;
        }

        public void setMarket_price(String market_price) {
          this.market_price = market_price;
        }

        public String getGoods_price() {
          return goods_price;
        }

        public void setGoods_price(String goods_price) {
          this.goods_price = goods_price;
        }

        public String getGoods_attr() {
          return goods_attr;
        }

        public void setGoods_attr(String goods_attr) {
          this.goods_attr = goods_attr;
        }

        public String getShop_point() {
          return shop_point;
        }

        public void setShop_point(String shop_point) {
          this.shop_point = shop_point;
        }

        public String getGoods_thumb() {
          return goods_thumb;
        }

        public void setGoods_thumb(String goods_thumb) {
          this.goods_thumb = goods_thumb;
        }

        public String getSuppliers_id() {
          return suppliers_id;
        }

        public void setSuppliers_id(String suppliers_id) {
          this.suppliers_id = suppliers_id;
        }

        public List<?> getComment_list() {
          return comment_list;
        }

        public void setComment_list(List<?> comment_list) {
          this.comment_list = comment_list;
        }

        public String getGoods_brief() {
          return goods_brief;
        }

        public void setGoods_brief(String goods_brief) {
          this.goods_brief = goods_brief;
        }
      }
    }
  }
}
