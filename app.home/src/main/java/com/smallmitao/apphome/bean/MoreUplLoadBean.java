package com.smallmitao.apphome.bean;

import java.util.List;

/**
 *
 * @author Jackshao
 * @date 2019/1/23
 */

public class MoreUplLoadBean {

  private List<String> url;

  public List<String> getUrl() {
    return url;
  }

  public void setUrl(List<String> url) {
    this.url = url;
  }
}
