package com.smallmitao.apphome.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.smallmitao.apphome.R;
import com.smallmitao.apphome.R2;
import com.smallmitao.apphome.di.componet.ActivityComponent;
import com.smallmitao.apphome.di.componet.DaggerActivityComponent;
import com.smallmitao.apphome.di.module.ActivityModule;
import com.smallmitao.apphome.mvp.ApplyShopPresenter;
import com.smallmitao.apphome.mvp.contract.ApplyShopContract;
import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.mvp.BaseMvpActivity;
import com.smallmitao.libbase.util.EditTextSizeCheckUtil;
import com.smallmitao.libbase.util.Toastor;
import com.smallmitao.libbase.util.Validator;
import com.smallmitao.libbridge.router.BridgeRouter;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.OnClick;
import cn.bingoogolapple.photopicker.activity.BGAPhotoPickerActivity;
import cn.bingoogolapple.photopicker.activity.BGAPhotoPickerPreviewActivity;
import cn.bingoogolapple.photopicker.widget.BGASortableNinePhotoLayout;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * @author Jackshao
 * @date 2019/1/3
 */

@Route(path = BridgeRouter.PAGE_ACTIVITY_APPLY_SHOP, name = "申请商家资质 第一步")
public class ApplyShopActivity extends BaseMvpActivity<ApplyShopPresenter> implements ApplyShopContract.View, EasyPermissions.PermissionCallbacks, BGASortableNinePhotoLayout.Delegate {

  @BindView(R2.id.title_tv)
  TextView mTitleTv;
  @BindView(R2.id.toolbar)
  Toolbar mToolbar;
  @BindView(R2.id.back_btn)
  ImageView backBtn;
  @BindView(R2.id.sfz_pic1)
  BGASortableNinePhotoLayout sfzPic1;
  @BindView(R2.id.sfz_pic2)
  BGASortableNinePhotoLayout sfzPic2;
  @BindView(R2.id.name_et)
  EditText nameEt;
  @BindView(R2.id.sfz_et)
  EditText sfzEt;
  @BindView(R2.id.next_btn)
  Button nextBtn;
  @BindView(R2.id.qq_et)
  EditText qqEt;
  @BindView(R2.id.email_et)
  EditText emailEt;


  private static final int FM_CHOOSE_PHOTO = 111;
  private static final int FM_PHOTO_PREVIEW = 222;

  private static final int MAIN_CHOOSE_PHOTO = 112;
  private static final int MAIN_PHOTO_PREVIEW = 223;

  private static final int PRC_PHOTO_PICKER = 1;



  private String mSfzPath1;
  private String mSfzPath2;

  private EditTextSizeCheckUtil mEditTextSizeCheckUtil;


  @Override
  protected int getLayout(@Nullable Bundle savedInstanceState) {
    return R.layout.activity_apply_shop;
  }

  @Override
  protected void initInject() {
    getFragmentComponent().inject(this);
  }

  public ActivityComponent getFragmentComponent() {
    return DaggerActivityComponent.builder()
      .baseAppComponent(BaseApp.getInstance().getBaseAppComponent())
      .activityModule(new ActivityModule(this))
      .build();
  }

  @Override
  protected void initImmersionBar() {
    super.initImmersionBar();
    mImmersionBar.titleBar(mToolbar).keyboardEnable(true).init();
  }

  @Override
  protected void initData(@Nullable Bundle savedInstanceState) {
    mTitleTv.setText("卖家资质申请");
    backBtn.setVisibility(View.VISIBLE);

    mEditTextSizeCheckUtil = new EditTextSizeCheckUtil(this);
    mEditTextSizeCheckUtil.setButtonEdit(nextBtn, nameEt, sfzEt,qqEt,emailEt);

    sfzPic1.setMaxItemCount(1);
    sfzPic1.setPlusEnable(true);
    sfzPic1.setDelegate(this);

    sfzPic2.setMaxItemCount(1);
    sfzPic2.setPlusEnable(true);
    sfzPic2.setDelegate(this);
  }

  /**
   * 选择图片的入口
   *
   * @param type
   */
  @AfterPermissionGranted(PRC_PHOTO_PICKER)
  private void choicePhotoWrapper(int type) {
    String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    if (EasyPermissions.hasPermissions(this, perms)) {
      // 拍照后照片的存放目录，改成你自己拍照后要存放照片的目录。如果不传递该参数的话就没有拍照功能
      File takePhotoDir = new File(Environment.getExternalStorageDirectory(), "smallmitaoBusinessTakePhoto");
      int maxCount = 1;
      int mResult = 0;
      switch (type) {
        case 1:
          maxCount = sfzPic1.getMaxItemCount() - sfzPic1.getItemCount();
          mResult = FM_CHOOSE_PHOTO;
          break;
        case 2:
          maxCount = sfzPic2.getMaxItemCount() - sfzPic2.getItemCount();
          mResult = MAIN_CHOOSE_PHOTO;
          break;
        default:
          break;
      }
      Intent photoPickerIntent = new BGAPhotoPickerActivity.IntentBuilder(this)
        // 拍照后照片的存放目录，改成你自己拍照后要存放照片的目录。如果不传递该参数的话则不开启图库里的拍照功能
        .cameraFileDir(takePhotoDir)
        // 图片选择张数的最大值
        .maxChooseCount(maxCount)
        // 当前已选中的图片路径集合
        .selectedPhotos(null)
        // 滚动列表时是否暂停加载图片
        .pauseOnScroll(false)
        .build();
      startActivityForResult(photoPickerIntent, mResult);
    } else {
      EasyPermissions.requestPermissions(this, "图片选择需要以下权限:\n\n1.访问设备上的照片\n\n2.拍照", PRC_PHOTO_PICKER, perms);
    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
  }


  @OnClick({R2.id.back_btn, R2.id.next_btn})
  public void onClick(View view) {
    if (view.getId() == R.id.back_btn) {
      finish();
    } else if (view.getId() == R.id.next_btn) {
      if (!TextUtils.isEmpty(mSfzPath1) && !TextUtils.isEmpty(mSfzPath2)) {

        if (!Validator.isIDCard(sfzEt.getText().toString())) {
          Toastor.showToast("身份证输入不正确");
          return;
        }

        if (!Validator.isEmail(emailEt.getText().toString().trim())) {
          Toastor.showToast("邮箱输入不正确！");
          return;
        }
        Bundle mBundle = new Bundle();
        mBundle.putString("id_name",nameEt.getText().toString());
        mBundle.putString("id_card",sfzEt.getText().toString());
        mBundle.putString("id_qq",qqEt.getText().toString());
        mBundle.putString("id_email",emailEt.getText().toString());
        mBundle.putString("id_sfz_1",mSfzPath1);
        mBundle.putString("id_sfz_2",mSfzPath2);
        ARouter.getInstance()
          .build(Uri.parse(BridgeRouter.PAGE_ACTIVITY_HAND_ID_CARD))
          .withBundle("apply",mBundle)
          .navigation();
      }else {
        Toastor.showToast("请上传身份证正反面！");
      }
    }
  }

  @Override
  public void onClickAddNinePhotoItem(BGASortableNinePhotoLayout sortableNinePhotoLayout, View view, int position, ArrayList<String> models) {
    if (sortableNinePhotoLayout == sfzPic1) {
      choicePhotoWrapper(1);
    } else if (sortableNinePhotoLayout == sfzPic2) {
      choicePhotoWrapper(2);
    }
  }

  @Override
  public void onClickDeleteNinePhotoItem(BGASortableNinePhotoLayout sortableNinePhotoLayout, View view, int position, String model, ArrayList<String> models) {
    if (sortableNinePhotoLayout == sfzPic1) {
      sfzPic1.removeItem(position);
      mSfzPath1 = "";
    } else if (sortableNinePhotoLayout == sfzPic2) {
      sfzPic2.removeItem(position);
      mSfzPath2 = "";
    }
  }

  @Override
  public void onClickNinePhotoItem(BGASortableNinePhotoLayout sortableNinePhotoLayout, View view, int position, String model, ArrayList<String> models) {
    int maxCount = 0;
    int mResult = 0;
    if (sortableNinePhotoLayout == sfzPic1) {
      maxCount = sfzPic1.getMaxItemCount();
      mResult = FM_PHOTO_PREVIEW;
    } else if (sortableNinePhotoLayout == sfzPic2) {
      maxCount = sfzPic2.getMaxItemCount();
      mResult = MAIN_PHOTO_PREVIEW;
    }
    Intent photoPickerPreviewIntent = new BGAPhotoPickerPreviewActivity.IntentBuilder(this)
      // 当前预览的图片路径集合
      .previewPhotos(models)
      // 当前已选中的图片路径集合
      .selectedPhotos(models)
      // 图片选择张数的最大值
      .maxChooseCount(maxCount)
      // 当前预览图片的索引
      .currentPosition(position)
      // 是否是拍完照后跳转过来
      .isFromTakePhoto(false)
      .build();
    startActivityForResult(photoPickerPreviewIntent, mResult);
  }

  @Override
  public void onNinePhotoItemExchanged(BGASortableNinePhotoLayout sortableNinePhotoLayout, int fromPosition, int toPosition, ArrayList<String> models) {

  }

  @Override
  public void onPermissionsGranted(int requestCode, List<String> perms) {
    if (requestCode == PRC_PHOTO_PICKER) {
      Toast.makeText(this, "您拒绝了「图片选择」所需要的相关权限!", Toast.LENGTH_SHORT).show();
    }
  }

  @Override
  public void onPermissionsDenied(int requestCode, List<String> perms) {

  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == RESULT_OK && requestCode == FM_CHOOSE_PHOTO) {
      sfzPic1.setData(BGAPhotoPickerActivity.getSelectedPhotos(data));
      String path = BGAPhotoPickerActivity.getSelectedPhotos(data).get(0);
      mPresenter.getLunBanPath(1, path);
    } else if (resultCode == RESULT_OK && requestCode == MAIN_CHOOSE_PHOTO) {
      sfzPic2.setData(BGAPhotoPickerActivity.getSelectedPhotos(data));
      String path = BGAPhotoPickerActivity.getSelectedPhotos(data).get(0);
      mPresenter.getLunBanPath(2, path);
    } else if (requestCode == FM_PHOTO_PREVIEW) {
      sfzPic1.setData(BGAPhotoPickerPreviewActivity.getSelectedPhotos(data));
    } else if (requestCode == MAIN_PHOTO_PREVIEW) {
      sfzPic2.setData(BGAPhotoPickerPreviewActivity.getSelectedPhotos(data));
    }
  }

  @Override
  public void getFile(int type, String path) {
    if (type == 1) {
      mSfzPath1 = path;
    } else if (type == 2) {
      mSfzPath2 = path;
    }
  }
}
