package com.smallmitao.apphome.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.smallmitao.apphome.R;
import com.smallmitao.apphome.R2;
import com.smallmitao.apphome.di.componet.ActivityComponent;
import com.smallmitao.apphome.di.componet.DaggerActivityComponent;
import com.smallmitao.apphome.di.module.ActivityModule;
import com.smallmitao.apphome.mvp.HandIdCardPresenter;
import com.smallmitao.apphome.mvp.contract.HandIdCardContract;
import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.mvp.BaseMvpActivity;
import com.smallmitao.libbase.util.Toastor;
import com.smallmitao.libbridge.router.BridgeRouter;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.OnClick;
import cn.bingoogolapple.photopicker.activity.BGAPhotoPickerActivity;
import cn.bingoogolapple.photopicker.activity.BGAPhotoPickerPreviewActivity;
import cn.bingoogolapple.photopicker.widget.BGASortableNinePhotoLayout;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * @author Jackshao
 * @date 2019/1/3
 */

@Route(path = BridgeRouter.PAGE_ACTIVITY_HAND_ID_CARD, name = "申请商家资质 第二步 手持身份证")
public class HandIDCardActivity extends BaseMvpActivity<HandIdCardPresenter> implements HandIdCardContract.View, EasyPermissions.PermissionCallbacks, BGASortableNinePhotoLayout.Delegate {

  @BindView(R2.id.title_tv)
  TextView mTitleTv;
  @BindView(R2.id.toolbar)
  Toolbar mToolbar;
  @BindView(R2.id.back_btn)
  ImageView backBtn;
  @BindView(R2.id.sfz_pic1)
  BGASortableNinePhotoLayout sfzPic1;

  private static final int FM_CHOOSE_PHOTO = 111;
  private static final int FM_PHOTO_PREVIEW = 222;

  private static final int PRC_PHOTO_PICKER = 1;
  private String mScSfz;

  @Autowired
  Bundle apply;

  @Override
  protected int getLayout(@Nullable Bundle savedInstanceState) {
    return R.layout.activity_handle_id_card;
  }

  @Override
  protected void initInject() {
    getFragmentComponent().inject(this);
  }

  public ActivityComponent getFragmentComponent() {
    return DaggerActivityComponent.builder()
      .baseAppComponent(BaseApp.getInstance().getBaseAppComponent())
      .activityModule(new ActivityModule(this))
      .build();
  }

  @Override
  protected void initImmersionBar() {
    super.initImmersionBar();
    mImmersionBar.titleBar(mToolbar).init();
  }

  @Override
  protected void initData(@Nullable Bundle savedInstanceState) {
    ARouter.getInstance().inject(this);
    mTitleTv.setText("卖家资质申请");
    backBtn.setVisibility(View.VISIBLE);

    sfzPic1.setMaxItemCount(1);
    sfzPic1.setPlusEnable(true);
    sfzPic1.setDelegate(this);
  }

  /**
   * 选择图片的入口
   * @param type
   */
  @AfterPermissionGranted(PRC_PHOTO_PICKER)
  private void choicePhotoWrapper(int type) {
    String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    if (EasyPermissions.hasPermissions(this, perms)) {
      // 拍照后照片的存放目录，改成你自己拍照后要存放照片的目录。如果不传递该参数的话就没有拍照功能
      File takePhotoDir = new File(Environment.getExternalStorageDirectory(), "smallmitaoBusinessTakePhoto");
      int maxCount =  sfzPic1.getMaxItemCount() - sfzPic1.getItemCount();
      int mResult = FM_CHOOSE_PHOTO;
      Intent photoPickerIntent = new BGAPhotoPickerActivity.IntentBuilder(this)
        // 拍照后照片的存放目录，改成你自己拍照后要存放照片的目录。如果不传递该参数的话则不开启图库里的拍照功能
        .cameraFileDir(takePhotoDir)
        // 图片选择张数的最大值
        .maxChooseCount(maxCount)
        // 当前已选中的图片路径集合
        .selectedPhotos(null)
        // 滚动列表时是否暂停加载图片
        .pauseOnScroll(false)
        .build();
      startActivityForResult(photoPickerIntent, mResult);
    } else {
      EasyPermissions.requestPermissions(this, "图片选择需要以下权限:\n\n1.访问设备上的照片\n\n2.拍照", PRC_PHOTO_PICKER, perms);
    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
  }


  @OnClick({R2.id.back_btn,R2.id.next_btn})
  public void onClick(View view) {
    if (view.getId() == R.id.back_btn) {
      finish();
    } else if (view.getId() == R.id.next_btn){
      if (!TextUtils.isEmpty(mScSfz)) {
        apply.putString("id_sc_sfz",mScSfz);
        ARouter.getInstance()
          .build(Uri.parse(BridgeRouter.PAGE_ACTIVITY_QUA_CER))
          .withBundle("apply",apply)
          .navigation();
      }else {
        Toastor.showToast("请上传手持身份证照片！");
      }
    }
  }

  @Override
  public void onClickAddNinePhotoItem(BGASortableNinePhotoLayout sortableNinePhotoLayout, View view, int position, ArrayList<String> models) {
    choicePhotoWrapper(1);
  }

  @Override
  public void onClickDeleteNinePhotoItem(BGASortableNinePhotoLayout sortableNinePhotoLayout, View view, int position, String model, ArrayList<String> models) {
    sfzPic1.removeItem(position);
    mScSfz = "";
  }

  @Override
  public void onClickNinePhotoItem(BGASortableNinePhotoLayout sortableNinePhotoLayout, View view, int position, String model, ArrayList<String> models) {
    int maxCount = sfzPic1.getMaxItemCount();
    int mResult = FM_PHOTO_PREVIEW;
    Intent photoPickerPreviewIntent = new BGAPhotoPickerPreviewActivity.IntentBuilder(this)
      // 当前预览的图片路径集合
      .previewPhotos(models)
      // 当前已选中的图片路径集合
      .selectedPhotos(models)
      // 图片选择张数的最大值
      .maxChooseCount(maxCount)
      // 当前预览图片的索引
      .currentPosition(position)
      // 是否是拍完照后跳转过来
      .isFromTakePhoto(false)
      .build();
    startActivityForResult(photoPickerPreviewIntent, mResult);
  }

  @Override
  public void onNinePhotoItemExchanged(BGASortableNinePhotoLayout sortableNinePhotoLayout, int fromPosition, int toPosition, ArrayList<String> models) {

  }

  @Override
  public void onPermissionsGranted(int requestCode, List<String> perms) {
    if (requestCode == PRC_PHOTO_PICKER) {
      Toast.makeText(this, "您拒绝了「图片选择」所需要的相关权限!", Toast.LENGTH_SHORT).show();
    }
  }

  @Override
  public void onPermissionsDenied(int requestCode, List<String> perms) {

  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == RESULT_OK && requestCode == FM_CHOOSE_PHOTO) {
      sfzPic1.setData(BGAPhotoPickerActivity.getSelectedPhotos(data));
      String path = BGAPhotoPickerActivity.getSelectedPhotos(data).get(0);
      mPresenter.getLunBanPath(path);
    }else if (requestCode == FM_PHOTO_PREVIEW) {
      sfzPic1.setData(BGAPhotoPickerPreviewActivity.getSelectedPhotos(data));
    }
  }


  @Override
  public void getFile(String path) {
    mScSfz = path;
  }
}
