package com.smallmitao.apphome.ui.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.smallmitao.apphome.R;
import com.smallmitao.apphome.bean.CateBean;

/**
 * @author Jackshao
 * @date 2018/9/18
 */

public class ReturnedGoodsAdapter extends BaseQuickAdapter<CateBean.ListBean,BaseViewHolder> {

  public ReturnedGoodsAdapter() {
    super(R.layout.item_returned_goods);
  }

  @Override
  protected void convert(BaseViewHolder helper, CateBean.ListBean item) {
    helper.setText(R.id.title_tv,item.getCat_name());
    helper.setBackgroundRes(R.id.is_choose_tv,item.isChoose()?R.mipmap.login_info_press:R.mipmap.login_info_normal);
  }

}
