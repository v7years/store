package com.smallmitao.apphome.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.smallmitao.apphome.R;
import com.smallmitao.apphome.R2;
import com.smallmitao.apphome.bean.CateBean;
import com.smallmitao.apphome.di.componet.ActivityComponent;
import com.smallmitao.apphome.di.componet.DaggerActivityComponent;
import com.smallmitao.apphome.di.module.ActivityModule;
import com.smallmitao.apphome.mvp.ReturnedGoodsPresenter;
import com.smallmitao.apphome.mvp.contract.ReturnedGoodsContract;
import com.smallmitao.apphome.ui.adapter.ReturnedGoodsAdapter;
import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.mvp.BaseMvpActivity;
import com.smallmitao.libbridge.router.BridgeRouter;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author Jackshao
 * @date 2019/1/3
 */

@Route(path = BridgeRouter.PAGE_ACTIVITY_RETURN_GOODS, name = "二级页面选择")
public class ReturnedGoodsActivity extends BaseMvpActivity<ReturnedGoodsPresenter> implements ReturnedGoodsContract.View {

  @BindView(R2.id.title_tv)
  TextView mTitleTv;
  @BindView(R2.id.toolbar)
  Toolbar mToolbar;
  @BindView(R2.id.back_btn)
  ImageView backBtn;
  @BindView(R2.id.recycle_view)
  RecyclerView mRecycleView;

  @Autowired
  String title;

  @Autowired
  int type;

  private ReturnedGoodsAdapter mReturnedGoodsAdapter;

  @Override
  protected void initInject() {
    getFragmentComponent().inject(this);
  }

  public ActivityComponent getFragmentComponent() {
    return DaggerActivityComponent.builder()
      .baseAppComponent(BaseApp.getInstance().getBaseAppComponent())
      .activityModule(new ActivityModule(this))
      .build();
  }

  @Override
  protected int getLayout(@Nullable Bundle savedInstanceState) {
    return R.layout.activity_ewturn_goods;
  }

  @Override
  protected void initImmersionBar() {
    super.initImmersionBar();
    mImmersionBar.titleBar(mToolbar).init();
  }

  @Override
  protected void initData(@Nullable Bundle savedInstanceState) {
    ARouter.getInstance().inject(this);
    backBtn.setVisibility(View.VISIBLE);

    mRecycleView.setLayoutManager(new LinearLayoutManager(this));
    mReturnedGoodsAdapter = new ReturnedGoodsAdapter();
    mRecycleView.setAdapter(mReturnedGoodsAdapter);

    switch (type){
      case 0:
        mTitleTv.setText("商品单位");
        mPresenter.requestSpDw();
        break;
      case 1:
        mTitleTv.setText("退换货");
        mPresenter.requestThh();
        break;
      case 2:
        mTitleTv.setText("商品分类");
        mPresenter.request();
        break;
      default:
        break;
    }
    mReturnedGoodsAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
      @Override
      public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        Intent mIntent = getIntent();
        mIntent.putExtra("mReturnedGoods",(CateBean.ListBean)adapter.getItem(position));
        setResult(Activity.RESULT_OK,mIntent);
        finish();
      }
    });
  }

  @OnClick({R2.id.back_btn})
  public void onClick(View view) {
    if (view.getId() == R.id.back_btn) {
      finish();
    }
  }


  @Override
  public void getCate(CateBean cateBean) {
    getCateList(cateBean.getList());
  }

  @Override
  public void getCateList(List<CateBean.ListBean> list) {
    if (list.size()==0){
      return;
    }
    List<CateBean.ListBean> mData = new ArrayList<>();
    for (int i =0;i<list.size();i++){
      CateBean.ListBean bean = list.get(i);
      if (TextUtils.isEmpty(title)){
        bean.setChoose(i==0);
        mData.add(bean);
      }else {
        bean.setChoose(title.equals(bean.getCat_name()));
        mData.add(bean);
      }
    }
    mReturnedGoodsAdapter.replaceData(mData);
  }


}
