package com.smallmitao.apphome.ui.adapter;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.smallmitao.apphome.R;
import com.smallmitao.apphome.bean.OrderListBean;

/**
 * @author Jackshao
 * @date 2018/9/18
 */

public class OrderListAdapter extends BaseQuickAdapter<OrderListBean.DataBean,BaseViewHolder> {

  private OrderItemAdapter mOrderItemAdapter;

  public OrderListAdapter() {
    super(R.layout.item_order_list);
  }

  @Override
  protected void convert(BaseViewHolder helper, OrderListBean.DataBean item) {
    helper.setText(R.id.name_tv,"收货人："+item.getConsignee());
    helper.setText(R.id.phone_tv,"联系人号码："+item.getConsignee_phone());
    helper.setText(R.id.address_tv,"收货地址: "+item.getAddress_detail());
    RecyclerView recyclerView = helper.getView(R.id.recycle_view);
    recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
    mOrderItemAdapter = new OrderItemAdapter();
    recyclerView.setAdapter(mOrderItemAdapter);
    TextView shop_count_tv = helper.getView(R.id.shop_count_tv);
    helper.setText(R.id.total_tv,"￥"+item.getTotal_price());
    helper.setText(R.id.yf_tv,"￥"+item.getShipping_fee());
    LinearLayout title_tv2 = helper.getView(R.id.title_tv2);
    if (item.getOrder_info()!=null && item.getOrder_info().size()>0 && item.getOrder_info().get(0).getGoods().size()>0){
      mOrderItemAdapter.replaceData(item.getOrder_info().get(0).getGoods());
      recyclerView.setVisibility(View.VISIBLE);
      shop_count_tv.setVisibility(View.VISIBLE);
      title_tv2.setVisibility(View.VISIBLE);
      helper.setText(R.id.shop_count_tv,"共"+item.getOrder_info().get(0).getGoods().size()+"件商品");
    }else {
      recyclerView.setVisibility(View.GONE);
      shop_count_tv.setVisibility(View.GONE);
      title_tv2.setVisibility(View.GONE);
    }
    helper.addOnClickListener(R.id.commit_btn);

  }


}
