package com.smallmitao.apphome.ui.adapter;

import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.smallmitao.apphome.R;
import com.smallmitao.apphome.bean.OrderListBean;
import com.smallmitao.libbase.util.GlideUtils;

/**
 * @author Jackshao
 * @date 2018/9/18
 */

public class OrderItemAdapter extends BaseQuickAdapter<OrderListBean.DataBean.OrderInfoBean.GoodsBean,BaseViewHolder> {

  public OrderItemAdapter() {
    super(R.layout.item_order_item);
  }

  @Override
  protected void convert(BaseViewHolder helper, OrderListBean.DataBean.OrderInfoBean.GoodsBean item) {
    helper.setText(R.id.name_tv,item.getGoods_name());
    helper.setText(R.id.ms_tv,item.getGoods_brief());
    helper.setText(R.id.goods_count_tv,"x"+item.getGoods_number());
    helper.setText(R.id.price_tv,"￥"+item.getGoods_price());
    helper.setGone(R.id.view,helper.getAdapterPosition() != this.mData.size()-1);
    ImageView img_iv = helper.getView(R.id.img_iv);
    GlideUtils.getInstance().loadCircleRadioImage(img_iv,item.getGoods_thumb(),5);
  }

}
