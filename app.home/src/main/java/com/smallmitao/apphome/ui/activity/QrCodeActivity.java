package com.smallmitao.apphome.ui.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.smallmitao.apphome.R;
import com.smallmitao.apphome.R2;
import com.smallmitao.apphome.di.componet.ActivityComponent;
import com.smallmitao.apphome.di.componet.DaggerActivityComponent;
import com.smallmitao.apphome.di.module.ActivityModule;
import com.smallmitao.apphome.mvp.QrCodePresenter;
import com.smallmitao.apphome.mvp.contract.QrCodeContract;
import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.mvp.BaseMvpActivity;
import com.smallmitao.libbridge.router.BridgeRouter;
import com.uuzuche.lib_zxing.activity.CodeUtils;
import net.lucode.hackware.magicindicator.buildins.UIUtil;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author Jackshao
 * @date 2019/1/3
 */

@Route(path = BridgeRouter.PAGE_ACTIVITY_QR_CODE, name = "二维码")
public class QrCodeActivity extends BaseMvpActivity<QrCodePresenter> implements QrCodeContract.View {

  @BindView(R2.id.title_tv)
  TextView mTitleTv;
  @BindView(R2.id.toolbar)
  Toolbar mToolbar;
  @BindView(R2.id.back_btn)
  ImageView backBtn;
  @BindView(R2.id.code_iv)
  ImageView mIconIv;
  private Bitmap mBitmap;

  @Override
  protected int getLayout(@Nullable Bundle savedInstanceState) {
    return R.layout.activity_qr_code;
  }

  @Override
  protected void initInject() {
    getFragmentComponent().inject(this);
  }

  public ActivityComponent getFragmentComponent() {
    return DaggerActivityComponent.builder()
      .baseAppComponent(BaseApp.getInstance().getBaseAppComponent())
      .activityModule(new ActivityModule(this))
      .build();
  }

  @Override
  protected void initImmersionBar() {
    super.initImmersionBar();
    mImmersionBar.titleBar(mToolbar).init();
  }

  @Override
  protected void initData(@Nullable Bundle savedInstanceState) {
    mTitleTv.setText("二维码");
    backBtn.setVisibility(View.VISIBLE);
    mPresenter.requestRqCode();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (mBitmap != null) {
      mBitmap.recycle();
      mBitmap = null;
    }
  }

  @OnClick({R2.id.back_btn, R2.id.refresh_tv})
  public void onClick(View view) {
    if (view.getId() == R.id.back_btn) {
      finish();
    } else if (view.getId() == R.id.refresh_tv) {
      mPresenter.requestRqCode();
    }
  }

  @Override
  public void getQrCode(String data) {
    setCode(data);
  }

  public void setCode(String code) {
    if (!TextUtils.isEmpty(code)) {
      mBitmap = CodeUtils.createImage(code, UIUtil.dip2px(this, 158)
        , UIUtil.dip2px(this, 158), null);
      mIconIv.setImageBitmap(mBitmap);
    }
  }
}
