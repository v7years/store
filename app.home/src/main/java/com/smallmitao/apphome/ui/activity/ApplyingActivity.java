package com.smallmitao.apphome.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.smallmitao.apphome.R;
import com.smallmitao.apphome.R2;
import com.smallmitao.libbase.base.BaseActivity;
import com.smallmitao.libbridge.router.BridgeRouter;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author Jackshao
 * @date 2019/1/3
 */

@Route(path = BridgeRouter.PAGE_ACTIVITY_APPLY, name = "申请中")
public class ApplyingActivity extends BaseActivity {

  @BindView(R2.id.title_tv)
  TextView mTitleTv;
  @BindView(R2.id.toolbar)
  Toolbar mToolbar;
  @BindView(R2.id.back_btn)
  ImageView backBtn;
  @BindView(R2.id.ddh_tv)
  TextView ddhTv;

  @Autowired
  String shop_id;

  @Override
  protected int getLayout(@Nullable Bundle savedInstanceState) {
    return R.layout.activity_applying;
  }

  @Override
  protected void initImmersionBar() {
    super.initImmersionBar();
    mImmersionBar.titleBar(mToolbar).init();
  }

  @Override
  protected void initData(@Nullable Bundle savedInstanceState) {
    mTitleTv.setText("申请中");
    backBtn.setVisibility(View.VISIBLE);
    ddhTv.setText("申请单号: "+shop_id);
  }

  @OnClick({R2.id.back_btn,R2.id.next_btn})
  public void onClick(View view) {
    if (view.getId() == R.id.back_btn) {
      finish();
    }else if (view.getId() == R.id.next_btn){
      finish();
    }
  }

}
