package com.smallmitao.apphome.ui.fragment;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.smallmitao.apphome.R;
import com.smallmitao.apphome.R2;
import com.smallmitao.apphome.bean.OrderListBean;
import com.smallmitao.apphome.di.componet.DaggerFragmentComponent;
import com.smallmitao.apphome.di.componet.FragmentComponent;
import com.smallmitao.apphome.di.module.FragmentModule;
import com.smallmitao.apphome.mvp.HomePagePresenter;
import com.smallmitao.apphome.mvp.contract.HomePageContract;
import com.smallmitao.apphome.ui.adapter.OrderListAdapter;
import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.manager.ShopStatusManager;
import com.smallmitao.libbase.mvp.BaseMvpFragment;
import com.smallmitao.libbase.util.Toastor;
import com.smallmitao.libbridge.router.BridgeRouter;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author Jackshao
 * @date 2019/1/2
 */

@Route(path = BridgeRouter.PAGE_FRAGMENT_HOME, name = "首页")
public class HomePageFragment extends BaseMvpFragment<HomePagePresenter> implements HomePageContract.View , OnRefreshListener,OnLoadMoreListener {


  @BindView(R2.id.toolbar)
  Toolbar mToolbar;
  @BindView(R2.id.title_tv)
  TextView mTitleTv;
  @BindView(R2.id.no_sh_lay)
  LinearLayout mNoShLay;
  @BindView(R2.id.recycle_view)
  RecyclerView mRecycleView;
  @BindView(R2.id.refresh_layout)
  SmartRefreshLayout mRefreshLayout;
  @BindView(R2.id.sh_title_tv)
  TextView sh_title_tv;
  @BindView(R2.id.reason_tv)
  TextView reasonTv;
  @BindView(R2.id.apply_btn)
  Button apply_btn;
  @BindView(R2.id.list_title_tv)
  TextView list_title_tv;


  private int mPage = 1;
  private int mSize = 20;
  private View mNotDataView;
  private OrderListAdapter mOrderListAdapter;

  @Override
  protected int getLayout() {
    return R.layout.fragment_home_page;
  }

  @Override
  protected void initInject() {
    getFragmentComponent().inject(this);
  }

  protected FragmentComponent getFragmentComponent() {
    return DaggerFragmentComponent.builder()
      .baseAppComponent(BaseApp.getInstance().getBaseAppComponent())
      .fragmentModule(new FragmentModule(this))
      .build();
  }

  @Override
  protected void initData() {
    mTitleTv.setText("首页");
    mNotDataView = getLayoutInflater().inflate(R.layout.app_no_data_view, (ViewGroup) mRecycleView.getParent(), false);
    mOrderListAdapter = new OrderListAdapter();
    mRecycleView.setLayoutManager(new LinearLayoutManager(getActivity()));
    mRecycleView.setAdapter(mOrderListAdapter);
    mRefreshLayout.setOnRefreshListener(this);
    mRefreshLayout.setOnLoadMoreListener(this);
    mRefreshLayout.setEnableLoadMore(true);
    int status = ShopStatusManager.getInstance().getShopStatus().getStatus();
    String reason = ShopStatusManager.getInstance().getShopStatus().getReason();
    setShopInfo(status,reason);

    mOrderListAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
      @Override
      public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        Toastor.showToast("过年期间，商家忙碌中~");
      }
    });
  }

  @Override
  public void onResume() {
    super.onResume();
    mPresenter.requestShop();
  }

  public void setShopInfo(int status,String reason){
    String reason2 = ShopStatusManager.getInstance().getShopStatus().getReason();
    mNoShLay.setVisibility(status==3?View.GONE:View.VISIBLE);
    mRefreshLayout.setVisibility(status==3?View.VISIBLE:View.GONE);
    list_title_tv.setVisibility(status==3?View.VISIBLE:View.GONE);
    String title="";
    String apply="";
    apply_btn.setEnabled(true);
    switch (status){
      case -1:
        title = "你还不是小蜜淘商家";
        reason = "请点击申请为小蜜淘商家";
        apply = "申请成为商家";
        break;
      case 0:
        title = "审核中";
        reason = "正在审核中,请稍等";
        apply = "正在审核中";
        apply_btn.setEnabled(false);
        break;
      case 1:
        title = "";
        reason = "恭喜，审核已通过！";
        apply = "立即上传商品";
        break;
      case 2:
        title = "审核失败";
        reason = reason2;
        apply = "重新申请";
        break;
      case 3:
        mPresenter.requestOrder(String.valueOf(mPage),String.valueOf(mSize));
        break;
      default:
        break;
    }
    sh_title_tv.setText(title);
    reasonTv.setText(reason);
    apply_btn.setText(apply);
  }

  @OnClick({R2.id.my_qr_code, R2.id.upload_shop,R2.id.apply_btn})
  public void onClick(View view) {
    if (view.getId() == R.id.my_qr_code) {
      ARouter.getInstance()
        .build(Uri.parse(BridgeRouter.PAGE_ACTIVITY_QR_CODE))
        .navigation();
    } else if (view.getId() == R.id.upload_shop) {
//      int status = ShopStatusManager.getInstance().getShopStatus().getStatus();
//      if (status==1 || status==3){
//        ARouter.getInstance()
//          .build(Uri.parse(BridgeRouter.PAGE_ACTIVITY_ADD_SHOP))
//          .navigation();
//      }else {
//        Toastor.showToast("您目前还不是小蜜淘商家,请申请再添加");
//      }
      ARouter.getInstance()
        .build(Uri.parse(BridgeRouter.PAGE_ACTIVITY_ADD_SHOP))
        .navigation();
    } else if (view.getId() == R.id.apply_btn){
      int status = ShopStatusManager.getInstance().getShopStatus().getStatus();
      if (status==-1 || status==2){
        ARouter.getInstance()
          .build(Uri.parse(BridgeRouter.PAGE_ACTIVITY_APPLY_SHOP))
          .navigation();
      }else if (status == 1){
        ARouter.getInstance()
          .build(Uri.parse(BridgeRouter.PAGE_ACTIVITY_ADD_SHOP))
          .navigation();
      }
    }
  }

  @Override
  public void getShop(int status,String reason) {
    setShopInfo(status,reason);
  }

  @Override
  public void getOrderList(boolean isOk, OrderListBean bean) {
    if (isOk){
      mRefreshLayout.finishRefresh(true);
      setData(bean.getData());
    }else {
      if (mPage == 1) {
        mRefreshLayout.finishRefresh(false);
      } else {
        mRefreshLayout.finishLoadMore(false);
      }
    }
  }

  @Override
  public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
    mPage++;
    mPresenter.requestOrder(String.valueOf(mPage),String.valueOf(mSize));
  }

  @Override
  public void onRefresh(@NonNull RefreshLayout refreshLayout) {
    mPage = 1;
    mPresenter.requestOrder(String.valueOf(mPage),String.valueOf(mSize));
  }


  private void setData(List<OrderListBean.DataBean> data) {
    final int size = data == null ? 0 : data.size();
    boolean isRefresh = mPage == 1;
    if (isRefresh) {
      mOrderListAdapter.replaceData(data);
    } else {
      if (size > 0) {
        mOrderListAdapter.addData(data);
      }
    }
    if (size < mSize) {
      //第一页如果不够一页就不显示没有更多数据布局
      mRefreshLayout.finishLoadMoreWithNoMoreData();
      if (size==0){
        mOrderListAdapter.setEmptyView(mNotDataView);
      }else {
        if (mPage>1){
          Toastor.showToast("没有更多数据了");
        }
      }
    } else {
      mRefreshLayout.finishLoadMore();
    }
  }
}
