package com.smallmitao.apphome.ui.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.smallmitao.apphome.R;
import com.smallmitao.apphome.R2;
import com.smallmitao.apphome.bean.AddShopBean;
import com.smallmitao.apphome.bean.CateBean;
import com.smallmitao.apphome.bean.MoreUplLoadBean;
import com.smallmitao.apphome.di.componet.ActivityComponent;
import com.smallmitao.apphome.di.componet.DaggerActivityComponent;
import com.smallmitao.apphome.di.module.ActivityModule;
import com.smallmitao.apphome.mvp.AddShopPresenter;
import com.smallmitao.apphome.mvp.contract.AddShopContract;
import com.smallmitao.apphome.ui.view.AddSaveDialog;
import com.smallmitao.libbase.base.AppManager;
import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.mvp.BaseMvpActivity;
import com.smallmitao.libbase.ui.CustomDialog;
import com.smallmitao.libbase.util.Toastor;
import com.smallmitao.libbridge.router.BridgeRouter;
import com.zhouyou.http.model.HttpParams;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.OnClick;
import cn.bingoogolapple.photopicker.activity.BGAPhotoPickerActivity;
import cn.bingoogolapple.photopicker.activity.BGAPhotoPickerPreviewActivity;
import cn.bingoogolapple.photopicker.widget.BGASortableNinePhotoLayout;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * @author Jackshao
 * @date 2019/1/3
 */

@Route(path = BridgeRouter.PAGE_ACTIVITY_ADD_SHOP, name = "新增商品")
public class AddShopActivity extends BaseMvpActivity<AddShopPresenter> implements AddShopContract.View , EasyPermissions.PermissionCallbacks, BGASortableNinePhotoLayout.Delegate {

  @BindView(R2.id.title_tv)
  TextView mTitleTv;
  @BindView(R2.id.toolbar)
  Toolbar mToolbar;
  @BindView(R2.id.back_btn)
  ImageView mBackBtn;
  @BindView(R2.id.fmt_pic)
  BGASortableNinePhotoLayout mFmtPic;
  @BindView(R2.id.main_pic)
  BGASortableNinePhotoLayout mMainPic;
  @BindView(R2.id.nine_pic)
  BGASortableNinePhotoLayout mNinePic;
  @BindView(R2.id.returned_goods_tv)
  TextView mReturnedGoodsTv;
  @BindView(R2.id.shop_class_tv)
  TextView mShopClassTv;
  @BindView(R2.id.gys_tv)
  TextView mGysTv;
  @BindView(R2.id.shop_address_tv)
  TextView mShopAddressTv;
  @BindView(R2.id.goods_name_et)
  EditText goodsNameEt;
  @BindView(R2.id.goods_desc_et)
  EditText goodsDescEt;
  @BindView(R2.id.bar_code_et)
  EditText barCodeEt;
  @BindView(R2.id.shop_price_et)
  EditText shopPriceEt;
  @BindView(R2.id.market_price_et)
  EditText marketPriceEt;
  @BindView(R2.id.goods_number_et)
  EditText goodsNumberEt;
  @BindView(R2.id.goods_weight_et)
  EditText goodsWeightEt;
  @BindView(R2.id.huo_hao_et)
  EditText huo_hao_et;


  private static final int PRC_PHOTO_PICKER = 1;

  private static final int FM_CHOOSE_PHOTO = 111;
  private static final int FM_PHOTO_PREVIEW = 222;

  private static final int MAIN_CHOOSE_PHOTO = 112;
  private static final int MAIN_PHOTO_PREVIEW = 223;

  private static final int NINE_CHOOSE_PHOTO = 113;
  private static final int NINE_PHOTO_PREVIEW = 224;

  private static final int RETURN_GOODS = 888;
  private CateBean.ListBean mReturnedGoods ;

  private static final int SHOP_CLASSIFICATION = 889;
  private CateBean.ListBean mShopClassification ;

  private static final int RETURN_DW = 900;
  private CateBean.ListBean mShopDw ;

  private String mFmPath;
  private HttpParams httpParams;
  private List<String> picList1 = new ArrayList<>();
  private List<String> picList2 = new ArrayList<>();

//  private AddSaveDialog addSaveDialog;

  @Override
  protected int getLayout(@Nullable Bundle savedInstanceState) {
    return R.layout.activity_add_shop;
  }


  @Override
  protected void initImmersionBar() {
    super.initImmersionBar();
    mImmersionBar.titleBar(mToolbar).keyboardEnable(true).init();
  }

  @Override
  protected void initInject() {
    getFragmentComponent().inject(this);
  }

  public ActivityComponent getFragmentComponent() {
    return DaggerActivityComponent.builder()
      .baseAppComponent(BaseApp.getInstance().getBaseAppComponent())
      .activityModule(new ActivityModule(this))
      .build();
  }

  @Override
  protected void initData(@Nullable Bundle savedInstanceState) {
    mTitleTv.setText("新增商品");
    mBackBtn.setVisibility(View.VISIBLE);
    httpParams = new HttpParams();
//    addSaveDialog = new AddSaveDialog(this,R.style.MyMiddleDialogStyle,mOnclick);
    initFm();
  }

  private View.OnClickListener mOnclick = new View.OnClickListener() {
    @Override
    public void onClick(View view) {
    }
  };

  public void initFm() {
    mFmtPic.setMaxItemCount(1);
    mFmtPic.setPlusEnable(true);
    mFmtPic.setDelegate(this);

    mMainPic.setMaxItemCount(4);
    mMainPic.setPlusEnable(true);
    // 设置拖拽排序控件的代理
    mMainPic.setDelegate(this);

    mNinePic.setMaxItemCount(9);
    mNinePic.setPlusEnable(true);
    mNinePic.setDelegate(this);

    mGysTv.setText("小蜜淘商家示范店001");
//    mPresenter.requestGys();
  }



  @OnClick({R2.id.back_btn,R2.id.thh_lay,R2.id.sp_fl_lay,R2.id.sp_dw_lay,R2.id.comit_btn})
  public void onClick(View view) {
    if (view.getId() == R.id.back_btn) {
      finish();
    } else if (view.getId() == R.id.sp_dw_lay){
      //商品单位选择
      ARouter.getInstance()
        .build(Uri.parse(BridgeRouter.PAGE_ACTIVITY_RETURN_GOODS))
        .withString("title",mShopDw!=null?mShopDw.getCat_name():"")
        .withInt("type",0)
        .navigation(this,RETURN_DW);
    } else if (view.getId() == R.id.thh_lay){
      //退换货选择
      ARouter.getInstance()
        .build(Uri.parse(BridgeRouter.PAGE_ACTIVITY_RETURN_GOODS))
        .withString("title",mReturnedGoods!=null?mReturnedGoods.getCat_name():"")
        .withInt("type",1)
        .navigation(this,RETURN_GOODS);
    } else if (view.getId() == R.id.sp_fl_lay){
      //商品分类
      ARouter.getInstance()
        .build(Uri.parse(BridgeRouter.PAGE_ACTIVITY_RETURN_GOODS))
        .withString("title",mShopClassification!=null?mShopClassification.getCat_name():"")
        .withInt("type",2)
        .navigation(this,SHOP_CLASSIFICATION);
    } else if (view.getId() == R.id.comit_btn){

      if (TextUtils.isEmpty(mFmPath)){
        Toastor.showToast("请上传商品封面");
        return;
      }
      if (TextUtils.isEmpty(goodsNameEt.getText().toString())){
        Toastor.showToast("请填写商品名称");
        return;
      }
      if (mShopDw==null){
        Toastor.showToast("请选择商品单位");
        return;
      }
      if (mReturnedGoods==null){
        Toastor.showToast("请选择退换货");
        return;
      }
      if (mShopClassification==null){
        Toastor.showToast("请选择一级分类");
        return;
      }
      if (TextUtils.isEmpty(shopPriceEt.getText().toString())){
        Toastor.showToast("请填写商品售价");
        return;
      }
      if (TextUtils.isEmpty(goodsNumberEt.getText().toString())){
        Toastor.showToast("请填写库存数量");
        return;
      }

      httpParams.put("cat_id",mShopClassification.getCat_id()+"");
      httpParams.put("goods_img",mFmPath);
      httpParams.put("goods_name",goodsNameEt.getText().toString());
      httpParams.put("return_service",mReturnedGoods.getCat_id()+"");
      httpParams.put("shippig_service",mShopDw.getCat_id()+"");
      httpParams.put("shop_price",shopPriceEt.getText().toString());
      httpParams.put("goods_number",goodsNumberEt.getText().toString());

      if (!TextUtils.isEmpty(marketPriceEt.getText().toString())){
        httpParams.put("market_price",marketPriceEt.getText().toString());
      }
      if (!TextUtils.isEmpty(huo_hao_et.getText().toString())){
        httpParams.put("goods_sn",huo_hao_et.getText().toString());
      }
      if (!TextUtils.isEmpty(barCodeEt.getText().toString())){
        httpParams.put("bar_code",barCodeEt.getText().toString());
      }
      if (!TextUtils.isEmpty(goodsWeightEt.getText().toString())){
        httpParams.put("goods_weight",goodsWeightEt.getText().toString());
      }
      if (!TextUtils.isEmpty(goodsDescEt.getText().toString())){
        httpParams.put("goods_desc",goodsDescEt.getText().toString());
      }

      Toastor.showToast("添加商品成功，请等待后台审核");
      finish();
//      if (mMainPic.getData().size()==0 && mNinePic.getData().size()==0){
//        //没有
//        mPresenter.requestAdd(httpParams);
//      }else {
//        List<File> mList1 = new ArrayList<>();
//        List<File> mList2 = new ArrayList<>();
//        if (mMainPic.getData().size()>0){
//          for (String url : mMainPic.getData()){
//            mList1.add(new File(url));
//          }
//        }
//        if (mNinePic.getData().size()>0){
//          for (String url : mNinePic.getData()){
//            mList2.add(new File(url));
//          }
//        }
//        mPresenter.requestMoreImg(mList1,mList2);
//      }
    }
  }

  /**
   * 选择图片的入口
   * @param type
   */
  @AfterPermissionGranted(PRC_PHOTO_PICKER)
  private void choicePhotoWrapper(int type) {
    String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    if (EasyPermissions.hasPermissions(this, perms)) {
      // 拍照后照片的存放目录，改成你自己拍照后要存放照片的目录。如果不传递该参数的话就没有拍照功能
      File takePhotoDir = new File(Environment.getExternalStorageDirectory(), "smallmitaoBusinessTakePhoto");
      int maxCount = 1;
      int mResult = 0;
      switch (type){
        case 1:
          maxCount =  mFmtPic.getMaxItemCount() - mFmtPic.getItemCount();
          mResult = FM_CHOOSE_PHOTO;
          break;
        case 2:
          maxCount =  mMainPic.getMaxItemCount() - mMainPic.getItemCount();
          mResult = MAIN_CHOOSE_PHOTO;
          break;
        case 3:
          maxCount =  mNinePic.getMaxItemCount() - mNinePic.getItemCount();
          mResult = NINE_CHOOSE_PHOTO;
          break;
        default:
          break;
      }
      Intent photoPickerIntent = new BGAPhotoPickerActivity.IntentBuilder(this)
        // 拍照后照片的存放目录，改成你自己拍照后要存放照片的目录。如果不传递该参数的话则不开启图库里的拍照功能
        .cameraFileDir(takePhotoDir)
        // 图片选择张数的最大值
        .maxChooseCount(maxCount)
        // 当前已选中的图片路径集合
        .selectedPhotos(null)
        // 滚动列表时是否暂停加载图片
        .pauseOnScroll(false)
        .build();
      startActivityForResult(photoPickerIntent, mResult);
    } else {
      EasyPermissions.requestPermissions(this, "图片选择需要以下权限:\n\n1.访问设备上的照片\n\n2.拍照", PRC_PHOTO_PICKER, perms);
    }
  }

  @Override
  public void onPermissionsGranted(int requestCode, List<String> perms) {
    if (requestCode == PRC_PHOTO_PICKER) {
      Toast.makeText(this, "您拒绝了「图片选择」所需要的相关权限!", Toast.LENGTH_SHORT).show();
    }
  }

  @Override
  public void onPermissionsDenied(int requestCode, List<String> perms) {

  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
  }

  @Override
  public void onClickAddNinePhotoItem(BGASortableNinePhotoLayout sortableNinePhotoLayout, View view, int position, ArrayList<String> models) {
    if (sortableNinePhotoLayout == mFmtPic){
      choicePhotoWrapper(1);
    } else if (sortableNinePhotoLayout == mMainPic){
      choicePhotoWrapper(2);
    } else if (sortableNinePhotoLayout == mNinePic){
      choicePhotoWrapper(3);
    }
  }

  @Override
  public void onClickDeleteNinePhotoItem(BGASortableNinePhotoLayout sortableNinePhotoLayout, View view, int position, String model, ArrayList<String> models) {
    if (sortableNinePhotoLayout == mFmtPic){
      mFmtPic.removeItem(position);
      mFmPath = "";
    } else if (sortableNinePhotoLayout == mMainPic){
      mMainPic.removeItem(position);
    } else if (sortableNinePhotoLayout == mNinePic){
      mNinePic.removeItem(position);
    }
  }

  @Override
  public void onClickNinePhotoItem(BGASortableNinePhotoLayout sortableNinePhotoLayout, View view, int position, String model, ArrayList<String> models) {
    int maxCount = 0;
    int mResult = 0;
    if (sortableNinePhotoLayout == mFmtPic){
      maxCount = mFmtPic.getMaxItemCount();
      mResult = FM_PHOTO_PREVIEW;
    } else if (sortableNinePhotoLayout == mMainPic){
      maxCount = mMainPic.getMaxItemCount();
      mResult = MAIN_PHOTO_PREVIEW;
    }  else if (sortableNinePhotoLayout == mNinePic){
      maxCount = mNinePic.getMaxItemCount();
      mResult = NINE_PHOTO_PREVIEW;
    }

    Intent photoPickerPreviewIntent = new BGAPhotoPickerPreviewActivity.IntentBuilder(this)
      // 当前预览的图片路径集合
      .previewPhotos(models)
      // 当前已选中的图片路径集合
      .selectedPhotos(models)
      // 图片选择张数的最大值
      .maxChooseCount(maxCount)
      // 当前预览图片的索引
      .currentPosition(position)
      // 是否是拍完照后跳转过来
      .isFromTakePhoto(false)
      .build();
    startActivityForResult(photoPickerPreviewIntent, mResult);
  }

  @Override
  public void onNinePhotoItemExchanged(BGASortableNinePhotoLayout sortableNinePhotoLayout, int fromPosition, int toPosition, ArrayList<String> models) {
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == RESULT_OK && requestCode == FM_CHOOSE_PHOTO) {
      mFmtPic.setData(BGAPhotoPickerActivity.getSelectedPhotos(data));
      String path = BGAPhotoPickerActivity.getSelectedPhotos(data).get(0);
      mPresenter.getLunBanPath(path);
    } else if (resultCode == RESULT_OK && requestCode == MAIN_CHOOSE_PHOTO) {
      mMainPic.addMoreData(BGAPhotoPickerActivity.getSelectedPhotos(data));
    } else if (resultCode == RESULT_OK && requestCode == NINE_CHOOSE_PHOTO) {
      mNinePic.addMoreData(BGAPhotoPickerActivity.getSelectedPhotos(data));
    } else if (requestCode == FM_PHOTO_PREVIEW) {
      mFmtPic.setData(BGAPhotoPickerPreviewActivity.getSelectedPhotos(data));
    } else if (requestCode == MAIN_PHOTO_PREVIEW) {
      mMainPic.setData(BGAPhotoPickerPreviewActivity.getSelectedPhotos(data));
    } else if (requestCode == NINE_PHOTO_PREVIEW) {
      mNinePic.setData(BGAPhotoPickerPreviewActivity.getSelectedPhotos(data));
    } else if (requestCode == RETURN_GOODS && resultCode == Activity.RESULT_OK){
      mReturnedGoods = data.getParcelableExtra("mReturnedGoods");
      mReturnedGoodsTv.setText(mReturnedGoods.getCat_name());
    } else if (requestCode == SHOP_CLASSIFICATION && resultCode == Activity.RESULT_OK){
      mShopClassification = data.getParcelableExtra("mReturnedGoods");
      mShopClassTv.setText(mShopClassification.getCat_name());
    } else if (requestCode == RETURN_DW && resultCode == Activity.RESULT_OK){
      mShopDw = data.getParcelableExtra("mReturnedGoods");
      mShopAddressTv.setText(mShopDw.getCat_name());
    }
  }

  @Override
  public void getGys(AddShopBean model) {
    mGysTv.setText(model.getSuppliers_name());
  }

  @Override
  public void getMoreImg(int type, MoreUplLoadBean model) {
    if (type==1){
      picList1 = model.getUrl();
    }else {
      picList2 = model.getUrl();
    }
    if (picList1.size()>0){
      StringBuffer sb = new StringBuffer();
      for (int i = 0;i<picList1.size();i++){
        sb.append(picList1.get(i)).append(",");
      }
      httpParams.put("goods_gallery_t",sb.substring(0,sb.length()-1));
    }
    if (picList2.size()>0){
      StringBuffer sb = new StringBuffer();
      for (int i = 0;i<picList2.size();i++){
        sb.append(picList2.get(i)).append(",");
      }
      httpParams.put("desc_mobile_arr_t",sb.substring(0,sb.length()-1));
    }

    if (picList1.size() == mMainPic.getData().size() &&
      picList2.size() == mNinePic.getData().size()){
      mPresenter.requestAdd(httpParams);
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    picList1.clear();
    picList2.clear();
  }

  /**
   * 上传封面
   * @param path
   */
  @Override
  public void getFmFile(String path) {
    mFmPath = path;
  }
}
