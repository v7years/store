package com.smallmitao.apphome.di.componet;


import android.app.Activity;
import com.smallmitao.apphome.di.module.ActivityModule;
import com.smallmitao.apphome.ui.activity.AddShopActivity;
import com.smallmitao.apphome.ui.activity.ApplyShopActivity;
import com.smallmitao.apphome.ui.activity.HandIDCardActivity;
import com.smallmitao.apphome.ui.activity.QrCodeActivity;
import com.smallmitao.apphome.ui.activity.QualificationCertificateActivity;
import com.smallmitao.apphome.ui.activity.ReturnedGoodsActivity;
import com.smallmitao.libbase.di.component.BaseAppComponent;
import com.smallmitao.libbase.di.scope.ActivityScope;
import dagger.Component;

/**
 * 注解桥梁
 *
 * @author Jackshao
 */
@ActivityScope
@Component(modules = ActivityModule.class, dependencies = BaseAppComponent.class)
public interface ActivityComponent {

    /**
     * 初始化
     * @return
     */
    Activity getActivity();

    void inject(QrCodeActivity activity);

    void inject(ApplyShopActivity activity);

    void inject(HandIDCardActivity activity);

    void inject(QualificationCertificateActivity activity);

    void inject(AddShopActivity activity);

    void inject(ReturnedGoodsActivity activity);

}
