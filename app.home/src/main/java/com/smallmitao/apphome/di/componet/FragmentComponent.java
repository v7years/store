package com.smallmitao.apphome.di.componet;

import android.app.Activity;

import com.smallmitao.apphome.di.module.FragmentModule;
import com.smallmitao.apphome.ui.fragment.HomePageFragment;
import com.smallmitao.libbase.di.component.BaseAppComponent;
import com.smallmitao.libbase.di.scope.FragmentScope;

import dagger.Component;


/**
 * @author Jackshao
 */
@FragmentScope
@Component(dependencies = BaseAppComponent.class, modules = FragmentModule.class)
public interface FragmentComponent {

    Activity getActivity();

    void inject(HomePageFragment homePageFragment);
}
