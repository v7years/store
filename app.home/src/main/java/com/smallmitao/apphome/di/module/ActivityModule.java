package com.smallmitao.apphome.di.module;


import android.app.Activity;

import com.smallmitao.libbase.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

/**
 * @author Jackshao
 */

@Module
public class ActivityModule {

    private Activity mActivity;

    public ActivityModule(Activity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityScope
    public Activity provideActivity() {
        return mActivity;
    }

}
