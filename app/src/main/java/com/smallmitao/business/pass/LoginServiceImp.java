package com.smallmitao.business.pass;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.smallmitao.libbase.manager.UserManager;
import com.smallmitao.libbridge.router.BridgePass;
import com.smallmitao.libbridge.router.base.BaseRouterService;
import com.smallmitao.libbridge.router.bean.LoginBean;
import com.smallmitao.libbridge.router.bean.StoreLoginBean;
import com.smallmitao.libbridge.router.service.ILoginInfoService;

/**
 * 暴露登录信息给其他模块
 * @author Jackshao
 * @date 2018/9/26
 */

@Route(path = BridgePass.SERVICE_PASS_LOGIN,name = "登录数据暴露")
public class LoginServiceImp extends BaseRouterService implements ILoginInfoService {

  @Override
  public boolean isLogin() {
    return UserManager.getInstance().isLogin();
  }

  @Override
  public LoginBean getLoginInfo() {
    return isLogin()?UserManager.getInstance().getLogin():null;
  }

  @Override
  public void setLoginOut() {
    UserManager.getInstance().clearLogin();
  }



  @Override
  public boolean isStoreLogin() {
    return  UserManager.getInstance().isStoreLogin();
  }

  @Override
  public StoreLoginBean getStoreLoginInfo() {
    return isStoreLogin()?UserManager.getInstance().getStoreLogin():null;
  }

  @Override
  public void setStoreLoginOut() {
    UserManager.getInstance().clearStoreLogin();
  }

}
