package com.smallmitao.business.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.smallmitao.business.R;
import com.smallmitao.business.di.componet.ActivityComponent;
import com.smallmitao.business.di.componet.DaggerActivityComponent;
import com.smallmitao.business.di.module.ActivityModule;
import com.smallmitao.business.mvp.LoginPresenter;
import com.smallmitao.business.mvp.contract.LoginContract;
import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.mvp.BaseMvpActivity;
import com.smallmitao.libbase.util.EditTextSizeCheckUtil;
import com.smallmitao.libbase.util.Toastor;
import com.smallmitao.libbase.util.Validator;
import com.smallmitao.libbridge.router.BridgeRouter;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author Jackshao
 */

@Route(path = BridgeRouter.PAGE_MAIN_ACTIVITY_LOGIN, name = "登录")
public class LoginActivity extends BaseMvpActivity<LoginPresenter> implements LoginContract.View {

    @BindView(R.id.phone_tv)
    EditText phoneTv;
    @BindView(R.id.code_tv)
    EditText codeTv;
    @BindView(R.id.xy_check)
    CheckBox xyCheck;
    @BindView(R.id.service_info_tv)
    TextView serviceInfoTv;
    @BindView(R.id.login_btn)
    Button loginBtn;
    @BindView(R.id.code_ma_tv)
    TextView codeMaTv;
    @BindView(R.id.back_btn)
    ImageView backBtn;
    @BindView(R.id.title_tv)
    TextView titleTv;
    @BindView(R.id.title_rightTv)
    TextView titleRightTv;
    @BindView(R.id.toolbar)
    Toolbar toolbar;


    @Autowired
    int login_type;

    private EditTextSizeCheckUtil mEditTextSizeCheckUtil;

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(toolbar).init();
    }

    @Override
    protected int getLayout(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_login;
    }

    @Override
    protected void initInject() {
        getFragmentComponent().inject(this);
    }

    public ActivityComponent getFragmentComponent() {
        return DaggerActivityComponent.builder()
                .baseAppComponent(BaseApp.getInstance().getBaseAppComponent())
                .activityModule(new ActivityModule(this))
                .build();
    }

    @Override
    public boolean isSlideClose() {
        return false;
    }


    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {
        ARouter.getInstance().inject(this);
        mEditTextSizeCheckUtil = new EditTextSizeCheckUtil(this);
        mEditTextSizeCheckUtil.setButtonEdit(loginBtn, phoneTv, codeTv);
        if(login_type == 1){
            titleTv.setText("商家登录");
        }else {
            titleTv.setText("供应商登录");
        }

        backBtn.setVisibility(View.VISIBLE);
    }

    @OnClick({R.id.login_btn, R.id.code_ma_tv, R.id.service_info_tv, R.id.back_btn})
    public void onClick(View view) {
        if (view.getId() == R.id.login_btn) {
            if (!xyCheck.isChecked()) {
                Toastor.showToast("请勾选用户协议");
                return;
            }
            if (login_type == 1){
                mPresenter.requestStoreLogin(phoneTv.getText().toString(), codeTv.getText().toString());
            }
            else
                mPresenter.requestLogin(phoneTv.getText().toString(), codeTv.getText().toString());
        } else if (view.getId() == R.id.code_ma_tv) {
            if (!Validator.isPhoneNum(phoneTv.getText().toString().trim())) {
                Toastor.showToast("手机格式不正确");
                return;
            }
            mPresenter.requestSms(codeMaTv, phoneTv.getText().toString());
        } else if (view.getId() == R.id.service_info_tv) {

        } else if (view.getId() == R.id.back_btn) {
            finish();
        }
    }
}
