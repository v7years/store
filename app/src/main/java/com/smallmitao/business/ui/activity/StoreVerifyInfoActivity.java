package com.smallmitao.business.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.smallmitao.business.R;
import com.smallmitao.business.di.componet.DaggerStoreVerifyInfoComponent;
import com.smallmitao.business.di.componet.StoreVerifyInfoComponent;
import com.smallmitao.business.di.module.StoreVerifyInfoModule;
import com.smallmitao.business.mvp.StoreVerifyPresenter;
import com.smallmitao.business.mvp.contract.StoreVerityContract;
import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.bean.AreaBean;
import com.smallmitao.libbase.manager.UserManager;
import com.smallmitao.libbase.mvp.BaseMvpActivity;
import com.smallmitao.libbase.ui.AreaDialogFragment;
import com.smallmitao.libbase.util.Toastor;
import com.smallmitao.libbridge.router.BridgeRouter;
import com.smallmitao.libbridge.router.bean.StoreLoginBean;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

@Route(path = BridgeRouter.PAGE_MAIN_ACTIVITY_STORE_VERIFY, name = "信息确认")
public class StoreVerifyInfoActivity extends BaseMvpActivity<StoreVerifyPresenter> implements StoreVerityContract.View {


    @BindView(R.id.back_btn)
    ImageView backBtn;
    @BindView(R.id.title_tv)
    TextView titleTv;
    @BindView(R.id.title_rightTv)
    TextView titleRightTv;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.inviter_id)
    EditText inviterId;
    @BindView(R.id.shop_name)
    EditText shopName;
    @BindView(R.id.choose_address)
    TextView chooseAddress;
    @BindView(R.id.address_detail)
    EditText addressDetail;
    private String mAddressCode;

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(toolbar).init();
    }


    @Override
    protected int getLayout(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_store_verify_info;
    }

    @Override
    protected void initInject() {
        getStoreVerifyInfoComponent().inject(this);
    }

    public StoreVerifyInfoComponent getStoreVerifyInfoComponent() {
        return DaggerStoreVerifyInfoComponent.builder()
                .baseAppComponent(BaseApp.getInstance().getBaseAppComponent())
                .storeVerifyInfoModule(new StoreVerifyInfoModule(this))
                .build();
    }

    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {
        titleTv.setText(getString(R.string.info_confirm));
        backBtn.setVisibility(View.VISIBLE);
        StoreLoginBean storeLogin = UserManager.getInstance().getStoreLogin();
        long parentId = storeLogin.getParent_user_no();
        if (parentId > 0) {
            inviterId.setText(String.valueOf(parentId));
            inviterId.setFocusable(false);
            inviterId.setFocusableInTouchMode(false);
        }

    }

    @OnClick({R.id.submit_info, R.id.back_btn, R.id.choose_address})
    public void onViewClicked(View view) {
        if (view.getId() == R.id.submit_info) {
            if (TextUtils.isEmpty(inviterId.getText().toString()) || TextUtils.isEmpty(shopName.getText().toString()) ||
                    TextUtils.isEmpty(mAddressCode) || TextUtils.isEmpty(addressDetail.getText().toString())) {
                Toastor.showToast("请完善信息");
            } else {
                mPresenter.submitInfo(inviterId.getText().toString(), shopName.getText().toString(), mAddressCode, addressDetail.getText().toString());
            }

        } else if (view.getId() == R.id.back_btn) {
            finish();
        } else if (view.getId() == R.id.choose_address) {
            FragmentManager fm = getSupportFragmentManager();
            AreaDialogFragment mAreaDialogFragment = new AreaDialogFragment();
            mAreaDialogFragment.setOnAreaCallBack(new AreaDialogFragment.AreaCallback() {
                @Override
                public void onAreaCallBack(List<AreaBean> list) {
                    String address = "";
                    mAddressCode="";
                    for (int i = 0; i < list.size(); i++) {
                        AreaBean myAddressInfo = list.get(i);
                        address += myAddressInfo.getValue() + "-";
                        mAddressCode+=myAddressInfo.getKey()+"-";
                    }
                    chooseAddress.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    chooseAddress.setText(address.substring(0, address.length() - 1));
                }
            });
            mAreaDialogFragment.show(fm, "");
        }

    }
}
