package com.smallmitao.business.ui.activity;

import android.net.Uri;
import android.os.Bundle;

import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.facade.callback.NavCallback;
import com.alibaba.android.arouter.launcher.ARouter;
import com.smallmitao.business.R;
import com.smallmitao.libbase.base.BaseActivity;
import com.smallmitao.libbase.manager.UserManager;
import com.smallmitao.libbridge.router.BridgeRouter;
import com.smallmitao.libbridge.router.bean.StoreLoginBean;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Description: 欢迎页
 * Creator: jackshao
 * date: 2018/8/10
 *
 * @author Jackshao
 */
public class WelcomeActivity extends BaseActivity {

    private Disposable mDisposable;

    @Override
    protected int getLayout(Bundle savedInstanceState) {
        return R.layout.activity_welcome;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        setDisposable();
    }

    @Override
    public boolean isSlideClose() {
        return false;
    }

    public void setDisposable() {
        mDisposable = Observable.timer(1000, TimeUnit.MILLISECONDS).subscribe(new Consumer<Long>() {
            @Override
            public void accept(Long aLong) throws Exception {
                if (UserManager.getInstance().getLogin() != null) {
                    ARouter.getInstance()
                            .build(Uri.parse(BridgeRouter.PAGE_MAIN))
                            .greenChannel()
                            .withTransition(R.anim.fade_in, R.anim.fade_out)
                            .navigation(WelcomeActivity.this, new NavCallback() {
                                @Override
                                public void onArrival(Postcard postcard) {
                                    WelcomeActivity.this.finish();
                                }
                            });
                } else if (UserManager.getInstance().getStoreLogin() != null) {
                    StoreLoginBean storeLogin = UserManager.getInstance().getStoreLogin();
                    if (storeLogin.getStore_id() == 0) {
                        UserManager.getInstance().clearStoreLogin();
                        ARouter.getInstance()
                                .build(Uri.parse(BridgeRouter.PAGE_MAIN_ACTIVITY_IDENTITY))
                                .greenChannel()
                                .withTransition(R.anim.fade_in, R.anim.fade_out)
                                .navigation(WelcomeActivity.this, new NavCallback() {
                                    @Override
                                    public void onArrival(Postcard postcard) {
                                        WelcomeActivity.this.finish();
                                    }
                                });
                    } else
                        ARouter.getInstance()
                                .build(Uri.parse(BridgeRouter.PAGE_ACTIVITY_STORE_MAIN))
                                .greenChannel()
                                .withTransition(R.anim.fade_in, R.anim.fade_out)
                                .navigation(WelcomeActivity.this, new NavCallback() {
                                    @Override
                                    public void onArrival(Postcard postcard) {
                                        WelcomeActivity.this.finish();
                                    }
                                });
                } else
                    ARouter.getInstance()
                            .build(Uri.parse(BridgeRouter.PAGE_MAIN_ACTIVITY_IDENTITY))
                            .greenChannel()
                            .withTransition(R.anim.fade_in, R.anim.fade_out)
                            .navigation(WelcomeActivity.this, new NavCallback() {
                                @Override
                                public void onArrival(Postcard postcard) {
                                    WelcomeActivity.this.finish();
                                }
                            });
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mDisposable != null && !mDisposable.isDisposed()) {
            mDisposable.dispose();
        }
    }
}