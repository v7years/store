package com.smallmitao.business.ui.activity;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.smallmitao.business.R;
import com.smallmitao.libbase.base.BaseActivity;
import com.smallmitao.libbase.manager.UserManager;
import com.smallmitao.libbridge.router.BridgeRouter;

import butterknife.OnClick;

@Route(path = BridgeRouter.PAGE_MAIN_ACTIVITY_IDENTITY, name = "选择登录身份")
public class SelectiveIdentityActivity extends BaseActivity {

    @Override
    protected int getLayout(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_selective_identity;
    }

    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {
        ARouter.getInstance().inject(this);
    }

    @OnClick({R.id.login_store, R.id.login_supplier})
    public void onViewClicked(View view) {
        if (view.getId() == R.id.login_store) {//商家
            ARouter.getInstance()
                    .build(Uri.parse(BridgeRouter.PAGE_MAIN_ACTIVITY_LOGIN))
                    .withInt("login_type", 1)
                    .navigation();
        } else if (view.getId() == R.id.login_supplier) {//供应商
            ARouter.getInstance()
                    .build(Uri.parse(BridgeRouter.PAGE_MAIN_ACTIVITY_LOGIN))
                    .withInt("login_type", 2)
                    .navigation();
        }
    }
}
