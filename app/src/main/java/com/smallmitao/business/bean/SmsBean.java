package com.smallmitao.business.bean;

/**
 *
 * @author Jackshao
 * @date 2019/1/23
 */

public class SmsBean {

  /**
   * error : 0
   */

  private int error;

  public int getError() {
    return error;
  }

  public void setError(int error) {
    this.error = error;
  }
}
