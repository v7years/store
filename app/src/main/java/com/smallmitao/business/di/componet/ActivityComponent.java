package com.smallmitao.business.di.componet;


import android.app.Activity;
import com.smallmitao.business.di.module.ActivityModule;
import com.smallmitao.business.ui.activity.LoginActivity;
import com.smallmitao.libbase.di.component.BaseAppComponent;
import com.smallmitao.libbase.di.scope.ActivityScope;
import dagger.Component;

/**
 * 注解桥梁
 *
 * @author Jackshao
 */
@ActivityScope
@Component(modules = ActivityModule.class, dependencies = BaseAppComponent.class)
public interface ActivityComponent {

    /**
     * 初始化
     * @return
     */
    Activity getActivity();

    void inject(LoginActivity activity);

}
