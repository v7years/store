package com.smallmitao.business.di.componet;


import android.app.Activity;

import com.smallmitao.business.di.module.StoreVerifyInfoModule;
import com.smallmitao.business.ui.activity.StoreVerifyInfoActivity;
import com.smallmitao.libbase.di.component.BaseAppComponent;
import com.smallmitao.libbase.di.scope.ActivityScope;

import dagger.Component;

/**
 * 注解桥梁
 *
 * @author Jackshao
 */
@ActivityScope
@Component(modules = StoreVerifyInfoModule.class, dependencies = BaseAppComponent.class)
public interface StoreVerifyInfoComponent {

    /**
     * 初始化
     * @return
     */
    Activity getActivity();

    void inject(StoreVerifyInfoActivity activity);

}
