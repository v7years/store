package com.smallmitao.business.di.module;


import android.app.Activity;

import com.smallmitao.libbase.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

/**
 * @author Jackshao
 */

@Module
public class StoreVerifyInfoModule {

    private Activity mActivity;

    public StoreVerifyInfoModule(Activity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityScope
    public Activity provideActivity() {
        return mActivity;
    }

}
