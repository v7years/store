package com.smallmitao.business.mvp.contract;

import com.smallmitao.libbase.mvp.BasePresenter;
import com.smallmitao.libbase.mvp.BaseView;

public interface StoreVerityContract {
    interface View extends BaseView {

    }

    interface Presenter extends BasePresenter<View> {
        void submitInfo(String id,String shopName,String address,String detailAddress);
    }
}
