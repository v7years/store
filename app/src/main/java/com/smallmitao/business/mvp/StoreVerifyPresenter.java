package com.smallmitao.business.mvp;

import android.app.Activity;
import android.net.Uri;

import com.alibaba.android.arouter.launcher.ARouter;
import com.smallmitao.business.mvp.contract.StoreVerityContract;
import com.smallmitao.libbase.http.RetrofitHelper;
import com.smallmitao.libbase.http.api.BaseApiResult;
import com.smallmitao.libbase.http.appinterface.HttpInter;
import com.smallmitao.libbase.manager.UserManager;
import com.smallmitao.libbase.manager.app.BaseUserManager;
import com.smallmitao.libbase.mvp.RxPresenter;
import com.smallmitao.libbase.util.Toastor;
import com.smallmitao.libbridge.router.BridgeRouter;
import com.smallmitao.libbridge.router.bean.StoreLoginBean;
import com.zhouyou.http.callback.SimpleCallBack;
import com.zhouyou.http.exception.ApiException;

import javax.inject.Inject;

public class StoreVerifyPresenter extends RxPresenter<StoreVerityContract.View> implements StoreVerityContract.Presenter {

    private RetrofitHelper mHelper;
    private Activity mActivity;

    @Inject
    public StoreVerifyPresenter(Activity activity, RetrofitHelper retrofitHelper) {
        this.mActivity = activity;
        this.mHelper = retrofitHelper;
    }

    @Override
    public void submitInfo(final String id, final String shopName, String address, String detailAddress) {

        String[] split = address.split("-");
        String province = split[0];
        String city = split[1];
        String area = split[2];
        addDisposable(mHelper
                .postRequest(HttpInter.StoreBindSuper.PATH)
                .params(HttpInter.StoreBindSuper.TYPE, "store")
                .params(HttpInter.StoreBindSuper.PARENT_ID, id)
                .params(HttpInter.StoreBindSuper.NAME, shopName)
                .params(HttpInter.StoreBindSuper.PROVINCE, province)
                .params(HttpInter.StoreBindSuper.CITY, city)
                .params(HttpInter.StoreBindSuper.AREA, area)
                .params(HttpInter.StoreBindSuper.ADDRESS, detailAddress)
                .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getStoreToken())

                .execute(new SimpleCallBack<BaseApiResult>() {

                    @Override
                    public void onError(ApiException e) {
                        Toastor.showToast(e.getMessage().toString());
                    }

                    @Override
                    public void onSuccess(BaseApiResult o) {
                        StoreLoginBean storeLogin = UserManager.getInstance().getStoreLogin();
                        storeLogin.setStore_id(0);
                        UserManager.getInstance().setStoreLogin(storeLogin);
                        ARouter.getInstance()
                                .build(Uri.parse(BridgeRouter.PAGE_ACTIVITY_STORE_MAIN))
                                .greenChannel()
                                .navigation();
                        mActivity.finish();
                    }

                }));
    }
}
