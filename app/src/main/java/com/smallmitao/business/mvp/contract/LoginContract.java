package com.smallmitao.business.mvp.contract;


import android.widget.TextView;

import com.smallmitao.libbase.mvp.BasePresenter;
import com.smallmitao.libbase.mvp.BaseView;

/**
 * 获取验证码
 * @author Jackshao
 */
public interface LoginContract {

    interface View extends BaseView {
    }

    interface Presenter extends BasePresenter<View> {
        /**
         * 请求
         */
        void requestSms(TextView textView,String phone);

        void requestLogin(String phone,String code);

        void requestStoreLogin(String phone,String code);

        void requestShop();
    }
}
