package com.smallmitao.business.mvp;

import android.app.Activity;
import android.net.Uri;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.smallmitao.business.mvp.contract.LoginContract;
import com.smallmitao.libbase.di.scope.ActivityScope;
import com.smallmitao.libbase.http.RetrofitHelper;
import com.smallmitao.libbase.http.api.BaseApiResult;
import com.smallmitao.libbase.http.appinterface.HttpInter;
import com.smallmitao.libbase.manager.ShopStatusManager;
import com.smallmitao.libbase.manager.UserManager;
import com.smallmitao.libbase.manager.app.BaseUserManager;
import com.smallmitao.libbase.mvp.RxPresenter;
import com.smallmitao.libbase.util.Toastor;
import com.smallmitao.libbridge.router.BridgeRouter;
import com.smallmitao.libbridge.router.bean.LoginBean;
import com.smallmitao.libbridge.router.bean.ShopStatusBean;
import com.smallmitao.libbridge.router.bean.StoreLoginBean;
import com.zhouyou.http.callback.SimpleCallBack;
import com.zhouyou.http.exception.ApiException;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;

/**
 * date: 2018/8/10
 *
 * @author Jackshao
 */

@ActivityScope
public class LoginPresenter extends RxPresenter<LoginContract.View> implements LoginContract.Presenter {

    private RetrofitHelper mHelper;
    private Activity mActivity;

    @Inject
    public LoginPresenter(Activity activity, RetrofitHelper retrofitHelper) {
        this.mActivity = activity;
        this.mHelper = retrofitHelper;
    }

    @Override
    public void requestSms(final TextView textView, String phone) {
        addDisposable(mHelper
                .postRequest(HttpInter.SmsSend.PATH)
                .params(HttpInter.SmsSend.MOBILE, phone)
                .params(HttpInter.SmsSend.TYPE, "1")
                .execute(new SimpleCallBack<BaseApiResult>() {

                    @Override
                    public void onError(ApiException e) {
                        Toastor.showToast(e.getMessage());
                    }

                    @Override
                    public void onSuccess(BaseApiResult o) {
                        codeCountdown(textView);
                    }
                }));
    }

    public void codeCountdown(final TextView codeTv) {
        codeTv.setEnabled(false);
        addDisposable(Flowable.intervalRange(0, 60, 0, 1, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Consumer<Long>() {
                    @Override
                    public void accept(Long aLong) throws Exception {
                        codeTv.setText((60 - aLong) + "s");
                    }
                })
                .doOnComplete(new Action() {
                    @Override
                    public void run() throws Exception {
                        //倒计时完毕置为可点击状态
                        codeTv.setEnabled(true);
                        codeTv.setText("获取验证码");
                    }
                })
                .subscribe());
    }


    @Override
    public void requestLogin(String phone, String code) {
        addDisposable(mHelper
                .postRequest(HttpInter.Login.PATH)
                .params(HttpInter.Login.MOBILE, phone)
                .params(HttpInter.Login.CODE, code)
                .execute(new SimpleCallBack<LoginBean>() {

                    @Override
                    public void onError(ApiException e) {
                        Toastor.showToast(e.getMessage());
                    }

                    @Override
                    public void onSuccess(LoginBean loginBean) {
                        UserManager.getInstance().setLogin(loginBean);
                        requestShop();
                    }
                }));
    }

    @Override
    public void requestStoreLogin(String phone, String code) {
        addDisposable(mHelper
                .postRequest(HttpInter.StoreLogin.PATH)
                .params(HttpInter.StoreLogin.PHONE, phone)
                .params(HttpInter.StoreLogin.CODE, code)
                .execute(new SimpleCallBack<StoreLoginBean>() {

                    @Override
                    public void onError(ApiException e) {
                        Toastor.showToast(e.getMessage());
                    }

                    @Override
                    public void onSuccess(StoreLoginBean loginBean) {
                        UserManager.getInstance().setStoreLogin(loginBean);
                        if (loginBean.getStore_id() == 0) {//完善信息
                            ARouter.getInstance()
                                    .build(Uri.parse(BridgeRouter.PAGE_MAIN_ACTIVITY_STORE_VERIFY))
                                    .greenChannel()
                                    .navigation();
                        } else {
                            ARouter.getInstance()
                                    .build(Uri.parse(BridgeRouter.PAGE_ACTIVITY_STORE_MAIN))
                                    .greenChannel()
                                    .navigation();
                        }
                        mActivity.finish();

                    }
                }));
    }

    @Override
    public void requestShop() {
        addDisposable(mHelper
                .postRequest(HttpInter.ShopStatus.PATH)
                .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getToken())
                .execute(new SimpleCallBack<ShopStatusBean>() {

                    @Override
                    public void onError(ApiException e) {
                        Toastor.showToast(e.getMessage());
                    }

                    @Override
                    public void onSuccess(ShopStatusBean o) {
                        ShopStatusManager.getInstance().setShopStatus(o);
                        ARouter.getInstance()
                                .build(Uri.parse(BridgeRouter.PAGE_MAIN))
                                .greenChannel()
                                .navigation();
                    }
                }));
    }

}
