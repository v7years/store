package com.smallmitao.business.app;

import android.content.Context;
import android.support.multidex.MultiDex;
import com.alibaba.android.arouter.launcher.ARouter;
import com.smallmitao.business.BuildConfig;
import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.http.HttpConfig;
import com.smallmitao.libbridge.router.BridgePass;
import com.smallmitao.libbridge.service.IX5Service;
import com.zhouyou.http.EasyHttp;

/**
 *
 * @author Jackshao
 * @date 2018/9/3
 */

public class MainApp extends BaseApp {

  @Override
  public void onCreate() {
    super.onCreate();

    //网络请求
    EasyHttp.init(this);

    //初始化路由
    if (BuildConfig.DEBUG) {
      ARouter.openLog();
      ARouter.openDebug();
    }
    ARouter.init(this);

    //初始化okHttp配置
    HttpConfig.initConfig(this);

    //初始化x5
//    initX5Qb();
//        初始化内存泄漏检测LeakCanary
//    if (LeakCanary.isInAnalyzerProcess(this)) {
//      return;
//    }
//    LeakCanary.install(this);
//    //初始化过度绘制检测anr
//    BlockCanary.install(this, new AppBlockCanaryContext()).start();

  }


  /**
   * 初始化x5内核
   */
  public void initX5Qb(){
    IX5Service mIx5 = (IX5Service) ARouter.getInstance().build(BridgePass.SERVICE_PASS_WEB_X5).navigation();
    mIx5.initX5(this);
  }

  @Override
  protected void attachBaseContext(Context base) {
    super.attachBaseContext(base);
    MultiDex.install(base);
  }
}
