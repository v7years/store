package com.smallmitao.business;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.jpeng.jptabbar.JPTabBar;
import com.jpeng.jptabbar.OnTabSelectListener;
import com.jpeng.jptabbar.anno.NorIcons;
import com.jpeng.jptabbar.anno.SeleIcons;
import com.jpeng.jptabbar.anno.Titles;
import com.smallmitao.libbase.base.AppManager;
import com.smallmitao.libbase.base.BaseActivity;
import com.smallmitao.libbase.base.BaseFragmentPagerAdapter;
import com.smallmitao.libbase.ui.CustomViewPager;
import com.smallmitao.libbase.util.Toastor;
import com.smallmitao.libbridge.router.BridgeRouter;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;

/**
 * @author Jackshao
 */

@Route(path = BridgeRouter.PAGE_MAIN, name = "app主页")
public class MainActivity extends BaseActivity implements OnTabSelectListener {


  @BindView(R.id.view_pager)
  CustomViewPager mViewPager;
  @BindView(R.id.tab_bar)
  JPTabBar mTabBar;

  private static long clickTime;
  private static long EXIT_APP = 2000;

  private List<Fragment> mFragmentList = new ArrayList<>();

  @Titles
  private static final int[] mTitles = {R.string.app_tab1, R.string.app_tab2, R.string.app_tab3, R.string.app_tab4};

  @SeleIcons
  private static final int[] mSelIcons = {R.mipmap.tab1_press, R.mipmap.tab2_press, R.mipmap.tab3_press, R.mipmap.tab4_press};

  @NorIcons
  private static final int[] mNormalIcons = {R.mipmap.tab1_normal, R.mipmap.tab2_normal, R.mipmap.tab3_normal, R.mipmap.tab4_normal};


  @Override
  protected int getLayout(@Nullable Bundle savedInstanceState) {
    return R.layout.activity_main;
  }

  @Override
  public void onTabSelect(int index) {

  }

  @Override
  public boolean isSlideClose() {
    return false;
  }

  @Override
  public boolean onInterruptSelect(int index) {
    return false;
  }

  @Override
  protected void initImmersionBar() {
    super.initImmersionBar();
    mImmersionBar.statusBarColorTransformEnable(false)
      //部分手机底部导航栏颜色（魅族，谷歌）
      .navigationBarColor(R.color.cp_main_tab_bar_color)
      .init();
  }


  @Override
  protected void initData(@Nullable Bundle savedInstanceState) {
    mFragmentList.add((Fragment) ARouter.getInstance().build(BridgeRouter.PAGE_FRAGMENT_HOME).navigation());
    mFragmentList.add((Fragment) ARouter.getInstance().build(BridgeRouter.PAGE_FRAGMENT_FIND).navigation());
    mFragmentList.add((Fragment) ARouter.getInstance().build(BridgeRouter.PAGE_FRAGMENT_VIP).navigation());
    mFragmentList.add((Fragment) ARouter.getInstance().build(BridgeRouter.PAGE_FRAGMENT_MINE).navigation());
    mViewPager.setScroll(false);
    mViewPager.setOffscreenPageLimit(3);
    mViewPager.setAdapter(new BaseFragmentPagerAdapter(getSupportFragmentManager(), mFragmentList));
    mTabBar.setContainer(mViewPager);
    mTabBar.setTabListener(this);
    mViewPager.setCurrentItem(0);
  }

  @Override
  public boolean onKeyDown(int keyCode, KeyEvent event) {
    if (keyCode == KeyEvent.KEYCODE_BACK) {
      if ((System.currentTimeMillis() - clickTime) > EXIT_APP) {
        Toastor.showToast("再按一次返回键退出程序");
        clickTime = System.currentTimeMillis();
      } else {
        AppManager.getInstance().exitApp();
      }
      return true;
    }
    return super.onKeyDown(keyCode, event);
  }

}
