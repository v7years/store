package com.smallmitao.libbase.base;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SlidingPaneLayout;
import android.view.InflateException;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import com.gyf.barlibrary.ImmersionBar;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import me.imid.swipebacklayout.lib.app.SwipeBackActivity;

/**
 * @author Jackshao
 * @date 2018/8/10
 * android:configChanges="orientation|keyboardHidden|screenSize" 如果需要做旋转屏幕，则配置文件设置
 */
public abstract class BaseActivity extends SwipeBackActivity implements SlidingPaneLayout.PanelSlideListener {

  private Activity mContext;
  private Unbinder mUnBinder;
  private InputMethodManager imm;

  /**
   * 沉浸式
   */
  protected ImmersionBar mImmersionBar;

  /**
   *  临时数据使用onSaveInstanceState保存恢复，永久性数据使用onPause方法保存。
   */
  private Bundle mBundle;


  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    init();
    try {
      int layoutResID = getLayout(savedInstanceState);
      //如果initView返回0,框架则不会调用setContentView(),当然也不会 Bind ButterKnife
      if (layoutResID != 0) {
        setContentView(layoutResID);
        mContext = this;
        //绑定到butterKnife
        mUnBinder = ButterKnife.bind(this);
      }
    } catch (Exception e) {
      if (e instanceof InflateException) {
        throw e;
      }
      e.printStackTrace();
    }
    //初始化沉浸式
    if (isImmersionBarEnabled()){
      initImmersionBar();
    }
    //是否侧滑
    getSwipeBackLayout().setEnableGesture(isSlideClose());
    initData(savedInstanceState);
  }


  @Override
  public void onPanelSlide(View panel, float slideOffset) {

  }

  @Override
  public void onPanelOpened(View panel) {
    finish();
  }

  @Override
  public void onPanelClosed(View panel) {

  }

  /**
   * 是否开启静止右滑关闭activity开关
   * 默认是开启的
   * @return
   */
  protected boolean isSlideClose() {
    return true;
  }

  /**
   * 是否可以使用沉浸式
   * Is immersion bar enabled boolean.
   *
   * @return the boolean
   */
  protected boolean isImmersionBarEnabled() {
    return true;
  }

  /**
   * 初始化状态栏
   */
  protected void initImmersionBar() {
    mImmersionBar = ImmersionBar.with(this);
    mImmersionBar.statusBarDarkFont(true).init();
  }

  @Override
  public void finish() {
    hideSoftKeyBoard();
    super.finish();
  }

  public void hideSoftKeyBoard() {
    View localView = getCurrentFocus();
    if (this.imm == null) {
      this.imm = ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE));
    }
    if ((localView != null) && (this.imm != null)) {
      this.imm.hideSoftInputFromWindow(localView.getWindowToken(), 2);
    }
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

  }

  @Override
  public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
    super.onSaveInstanceState(outState, outPersistentState);
    if (mBundle != null) {
      outState.putBundle("stateBundle", mBundle);
    }
  }

  @Override
  protected void onRestoreInstanceState (Bundle savedInstanceState) {
    super. onRestoreInstanceState( savedInstanceState) ;
    mBundle = savedInstanceState.getBundle("stateBundle");
  }


  protected void init() {
    AppManager.getInstance().addActivity(this);
  }

  @Override
  protected void onStart() {
    super.onStart();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (mUnBinder != null && mUnBinder != Unbinder.EMPTY){
      mUnBinder.unbind();
    }
    if (mImmersionBar != null){
      mImmersionBar.destroy();
    }
    this.imm = null;
    this.mUnBinder = null;
    if (mContext!=null){
      this.mContext = null;
    }
    AppManager.getInstance().removeActivity(this);
  }


  /**
   * 初始化 View, 如果 {@link #getLayout(Bundle)} 返回 0, 框架则不会调用 {@link Activity#setContentView(int)}
   * @param savedInstanceState
   * @return
   */
  protected abstract int getLayout(@Nullable Bundle savedInstanceState);


  /**
   * 初始化数据
   * @param savedInstanceState
   */
  protected void initData(@Nullable Bundle savedInstanceState){
  }


}
