package com.smallmitao.libbase.base;

import android.support.annotation.NonNull;
import com.smallmitao.libbase.di.component.BaseAppComponent;

/**
 *
 * @author Jackshao
 * @date 2018/9/3
 */

public interface IBaseApp {

  /**
   * 注解
   * @return
   */
  @NonNull
  BaseAppComponent getBaseAppComponent();

}
