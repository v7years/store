package com.smallmitao.libbase.base;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatDelegate;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.DefaultRefreshFooterCreator;
import com.scwang.smartrefresh.layout.api.DefaultRefreshHeaderCreator;
import com.scwang.smartrefresh.layout.api.DefaultRefreshInitializer;
import com.scwang.smartrefresh.layout.api.RefreshFooter;
import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.simple.spiderman.CrashModel;
import com.simple.spiderman.SpiderMan;
import com.smallmitao.libbase.BuildConfig;
import com.smallmitao.libbase.R;
import com.smallmitao.libbase.di.component.BaseAppComponent;
import com.smallmitao.libbase.di.component.DaggerBaseAppComponent;
import com.smallmitao.libbase.di.module.UtilsModule;
import com.smallmitao.libbase.util.DynamicTimeFormat;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.uuzuche.lib_zxing.activity.ZXingLibrary;

/**
 * @author Jackshao
 * @date 2018/9/3
 */
public class BaseApp extends Application implements IBaseApp {

  private static BaseApp instance;
  public static IWXAPI mWxApi;
  public static BaseApp getInstance() {
    return instance;
  }

  //static 代码段可以防止内存泄露
  static {
    //启用矢量图兼容
    AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    //设置全局默认配置（优先级最低，会被其他设置覆盖）
    SmartRefreshLayout.setDefaultRefreshInitializer(new DefaultRefreshInitializer() {
      @Override
      public void initialize(@NonNull Context context, @NonNull RefreshLayout layout) {
        //全局设置（优先级最低）
        //是否启用上拉加载更多
        layout.setEnableLoadMore(false);
        //是否启用列表惯性滑动到底部时自动加载更多
        layout.setEnableAutoLoadMore(true);
        //是否启用越界拖动
        layout.setEnableOverScrollDrag(false);
        //是否启用越界回弹
        layout.setEnableOverScrollBounce(true);
        //是否在列表不满一页时候开启上拉加载功能
        layout.setEnableLoadMoreWhenContentNotFull(true);
        //是否在刷新完成时滚动列表显示新的内容
        layout.setEnableScrollContentWhenRefreshed(true);
        //是否内容跟着偏移
        layout.setEnableHeaderTranslationContent(true);
      }
    });

    SmartRefreshLayout.setDefaultRefreshHeaderCreator(new DefaultRefreshHeaderCreator() {
      @NonNull
      @Override
      public RefreshHeader createRefreshHeader(@NonNull Context context, @NonNull RefreshLayout layout) {
        //全局设置主题颜色（优先级第二低，可以覆盖 DefaultRefreshInitializer 的配置，与下面的ClassicsHeader绑定）
        layout.setPrimaryColorsId(R.color.transparent, R.color.app_title_text_color);
        ClassicsHeader.REFRESH_HEADER_PULLING = context.getResources().getString(R.string.base_pull_down_refreshes);
        ClassicsHeader.REFRESH_HEADER_REFRESHING = context.getResources().getString(R.string.base_refreshing);
        ClassicsHeader.REFRESH_HEADER_LOADING = context.getResources().getString(R.string.base_loading);
        ClassicsHeader.REFRESH_HEADER_RELEASE = context.getResources().getString(R.string.base_im_refresh);
        ClassicsHeader.REFRESH_HEADER_FINISH =  context.getResources().getString(R.string.base_refresh_complete);
        ClassicsHeader.REFRESH_HEADER_FAILED = context.getResources().getString(R.string.base_refresh_failure);
        String update = context.getResources().getString(R.string.base_update_on);
        return new ClassicsHeader(context).setTimeFormat(new DynamicTimeFormat(update+" %s"));
      }
    });

    SmartRefreshLayout.setDefaultRefreshFooterCreator(new DefaultRefreshFooterCreator() {
      @NonNull
      @Override
      public RefreshFooter createRefreshFooter(@NonNull Context context, @NonNull RefreshLayout layout) {
        //指定为经典Footer，默认是 BallPulseFooter
        return new ClassicsFooter(context).setDrawableSize(20);
      }
    });
  }


  @Override
  public void onCreate() {
    super.onCreate();
    instance = this;
    ZXingLibrary.initDisplayOpinion(this);
    //崩溃检测
    installBk();
    registerToWX();//微信第三方
    //x5
//    initX5Qb();
  }


  public void installBk(){
    SpiderMan.getInstance()
      .init(this)
      //设置是否捕获异常，不弹出崩溃框
      .setEnable(BuildConfig.DEBUG)
      //设置是否显示崩溃信息展示页面
      .showCrashMessage(true)
      //是否回调异常信息，友盟等第三方崩溃信息收集平台会用到,
      .setOnCrashListener(new SpiderMan.OnCrashListener() {
        @Override
        public void onCrash(Thread t, Throwable ex, CrashModel model) {
          //CrashModel 崩溃信息记录，包含设备信息
        }
      });
  }

  @NonNull
  @Override
  public BaseAppComponent getBaseAppComponent() {
    return getMainAppComponent();
  }


  public BaseAppComponent appComponent;
  public BaseAppComponent getMainAppComponent() {
    if (appComponent == null) {
      appComponent = DaggerBaseAppComponent.builder()
        .utilsModule(new UtilsModule(instance))
        .build();
    }
    return appComponent;
  }

  private void registerToWX() {
    //第二个参数是指你应用在微信开放平台上的AppID
    mWxApi = WXAPIFactory.createWXAPI(this, "wx0f070d5f7b23bd9f", false);
    // 将该app注册到微信
    mWxApi.registerApp("wx0f070d5f7b23bd9f");
  }

}
