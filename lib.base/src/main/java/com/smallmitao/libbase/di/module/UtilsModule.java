package com.smallmitao.libbase.di.module;

import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.http.RetrofitHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
 @Module
public class UtilsModule {

    private BaseApp application;

    public UtilsModule(BaseApp application) {
        this.application = application;
    }

    @Provides
    @Singleton
    BaseApp provideBaseApp() {
        return application;
    }

    @Provides
    @Singleton
    RetrofitHelper provideHttpHelper() {
        return new RetrofitHelper();
    }
}
