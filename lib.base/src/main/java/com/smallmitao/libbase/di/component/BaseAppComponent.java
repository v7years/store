package com.smallmitao.libbase.di.component;


import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.di.module.UtilsModule;
import com.smallmitao.libbase.http.RetrofitHelper;

import javax.inject.Singleton;

import dagger.Component;

/**
 * @author Jackshao
 */
@Singleton
@Component(modules = {UtilsModule.class})
public interface BaseAppComponent {

    /**
     * 初始化
     * @return
     */
    BaseApp getBaseApp();

    /**
     * 提供接口
     * @return
     */
    RetrofitHelper retrofitHelper();

}
