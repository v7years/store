package com.smallmitao.libbase.http;


import com.smallmitao.libbase.http.api.AppDeleteRequest;
import com.smallmitao.libbase.http.api.AppGetRequest;
import com.smallmitao.libbase.http.api.AppPostRequest;

/**
 * 各个接口的实现类
 * @author Jackshao
 */
public class RetrofitHelper implements HttpHelper {


  @Override
  public AppGetRequest getRequest(String url) {
    return new AppGetRequest(url);
  }

  @Override
  public AppPostRequest postRequest(String url) {
    return new AppPostRequest(url);
  }

  @Override
  public AppDeleteRequest deleteRequest(String url) {
    return new AppDeleteRequest(url);
  }

}
