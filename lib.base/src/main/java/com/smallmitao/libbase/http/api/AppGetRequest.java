package com.smallmitao.libbase.http.api;

import com.zhouyou.http.callback.CallBack;
import com.zhouyou.http.callback.CallBackProxy;
import com.zhouyou.http.callback.CallClazzProxy;
import com.zhouyou.http.request.GetRequest;

import java.lang.reflect.Type;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;


/**
 * <p>描述：扩展GetResult请求，解决自定义ApiResult重复写代理的问题</p>
 *
 * get 请求
 * 返回的数据格式 BaseApiResult这种类型
 *
 * @author Jackshao
 */

public class AppGetRequest extends GetRequest {

    public AppGetRequest(String url) {
        super(url);
    }

    /**
     * 覆写execute方法指定，代理用TestApiResult2
     * @param type
     * @param <T>
     * @return
     */
    @Override
    public <T> Observable<T> execute(Type type) {
        return super.execute(new CallClazzProxy<BaseApiResult<T>, T>(type) {
        });
    }

    @Override
    public <T> Observable<T> execute(Class<T> clazz) {
        return super.execute(new CallClazzProxy<BaseApiResult<T>, T>(clazz) {
        });
    }

    @Override
    public <T> Disposable execute(CallBack<T> callBack) {
        return super.execute(new CallBackProxy<BaseApiResult<T>, T>(callBack) {
        });
    }
}
