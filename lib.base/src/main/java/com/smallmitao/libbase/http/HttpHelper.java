package com.smallmitao.libbase.http;


import com.smallmitao.libbase.http.api.AppDeleteRequest;
import com.smallmitao.libbase.http.api.AppGetRequest;
import com.smallmitao.libbase.http.api.AppPostRequest;

/**
 * @author Jackshao
 */
public interface HttpHelper {

  /**
   * get请求
   * @param url
   * @return
   */
  AppGetRequest getRequest(String url);

  /**
   * post请求
   * @param url
   * @return
   */
  AppPostRequest postRequest(String url);


  /**
   * delete请求
   * @param url
   * @return
   */
  AppDeleteRequest deleteRequest(String url);

}
