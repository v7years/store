package com.smallmitao.libbase.http.interceptor;

import android.annotation.SuppressLint;

import com.smallmitao.libbase.util.MD5;
import com.zhouyou.http.interceptor.BaseDynamicInterceptor;
import com.zhouyou.http.utils.HttpLog;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;
import java.util.TreeMap;
import static com.zhouyou.http.utils.HttpUtil.UTF8;

/**
 * <p>描述：对参数进行签名、添加token、时间戳处理的拦截器</p>
 * 主要功能说明：<br>
 * 因为参数签名没办法统一，签名的规则不一样，签名加密的方式也不同有MD5、BASE64等等，只提供自己能够扩展的能力。<br>
 *
 * @author Jackshao
 */
public class BaseSignInterceptor extends BaseDynamicInterceptor<BaseSignInterceptor> {

    @Override
    public TreeMap<String, String> dynamic(TreeMap<String, String> dynamicMap) {

        //dynamicMap:是原有的全局参数+局部参数
        /**
         * 是否添加时间戳，因为你的字段key可能不是timestamp,这种动态的自己处理
         */
        if (isTimeStamp()) {
            long timeStampSec = System.currentTimeMillis()/1000;
            @SuppressLint("DefaultLocale")
            String timestamp = String.format("%010d", timeStampSec);
//            dynamicMap.put(HttpInter.Common.TIMESTAMP, timestamp);
        }

        /**
         * 是否添加token
         *
         */
        if (isAccessToken()) {
//            ILoginInfoService service = (ILoginInfoService) ARouter.getInstance().build(BridgePass.SERVICE_PASS_LOGIN).navigation();
//            if (service.getLoginInfo()!=null){
//                dynamicMap.put(HttpInter.Common.ACCESSTOKEN, "Bearer "+service.getLoginInfo().getToken());
//            }
        }

        /**
         * 是否签名,因为你的字段key可能不是sign，这种动态的自己处理
         */
        if (isSign()) {
            String sing2 = sign(dynamicMap);
//            dynamicMap.put(HttpInter.Common.SIGN, sing2);
        }
        HttpLog.d(isTimeStamp()+"===="+isSign()+"===请求参数=====>>"+dynamicMap);
        return dynamicMap;
    }

    /**
     * 签名规则：POST+url+参数的拼装+secret
     * @param dynamicMap
     * @return
     */
    private String sign(TreeMap<String, String> dynamicMap) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> entry : dynamicMap.entrySet()) {
            sb.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
        }
        sb.deleteCharAt(sb.length()-1);
        sb.append("x7WYvsCVL9rn1ST3");
        String signStr = sb.toString();
        try {
            signStr = URLDecoder.decode(signStr, UTF8.name());
        } catch (UnsupportedEncodingException exception) {
            exception.printStackTrace();
        }
        return MD5.encode(signStr);
    }
}
