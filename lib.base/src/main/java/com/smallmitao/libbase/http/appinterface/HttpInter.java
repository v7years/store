package com.smallmitao.libbase.http.appinterface;


/**
 * 定义通用常量：接口和配置
 *
 * @author Jackshao
 */

public class HttpInter {

    public final static class Common {
        public final static String ACCEPT = "Accept";
        public final static String SOURCE = "source";
        public final static String ACCESSTOKEN = "Authorization";
        public final static String TIMESTAMP = "timestamp";
        public final static String SIGN = "sign";
    }

    /**
     * 获取验证码
     */
    public final static class SmsSend {
        public final static String PATH = "/sms/send";
        public final static String MOBILE = "mobile";
        public final static String TYPE = "type";
    }

    /**
     * 登录
     */
    public final static class Login {
        public final static String PATH = "/auth/login";
        public final static String MOBILE = "mobile";
        public final static String CODE = "code";

        public final static String LOGIN_MANAGER = "login_manager";
        public final static String LOGIN_STATUS = "login_status";
    }


    /**
     * 获取商家二维码
     */
    public final static class QrCode {
        public final static String PATH = "/shop/QRcode";
    }

    /**
     * 上架商品列表
     */
    public final static class GoodsGetList {

        public final static String PATH = "/goods/getListTest";
        //        public final static String PATH = "/goods/getList";
        public final static String IS_ON_SALE = "is_on_sale";
        public final static String REVIEW_STATUS = "review_status";
        public final static String PAGE = "page";
        public final static String PAGE_SIZE = "pageSize";
    }

    /**
     * 下架商品
     */
    public final static class GoodsUpdate {
        public final static String PATH = "/goods/update";
        public final static String IS_ON_SALE = "is_on_sale";
        public final static String GOODS_ID = "goods_id";
    }

    /**
     * 商家的状态
     */
    public final static class ShopStatus {
        public final static String PATH = "/shop/status";
    }


    /**
     * 上传图片
     */
    public final static class UploadImg {
        public final static String PATH = "/up/img";
        public final static String TYPE = "type";
        public final static String NAME = "file";
    }

    /**
     * 申请店铺
     */
    public final static class ApplyShop {
        public final static String PATH = "/applyShop";
        public final static String real_name = "real_name";
        public final static String identity_num = "identity_num";
        public final static String front_identity = "front_identity";
        public final static String back_identity = "back_identity";
        public final static String hold_identity = "hold_identity";
        public final static String business_licence = "business_licence";
        public final static String brand_name = "brand_name";
        public final static String type = "type";
        public final static String qq = "qq";
        public final static String email = "email";
    }

    /**
     * 收益接口
     */
    public final static class GetIncome {
        public final static String PATH = "/billDetail/getIncome";
    }

    /**
     * 获取店铺信息接口
     */
    public final static class GetShopInfo {
        public final static String PATH = "/getShopInfo";
    }

    /**
     * 获取供应商
     */
    public final static class GetSupplierShop {
        public final static String PATH = "/goods/getSupplierShop";
    }

    /**
     * 上传多张
     */
    public final static class MoreUnloadImg {
        public final static String PATH = "/up/imgs"; // "/up/imgs"
        public final static String type = "type";
        public final static String file = "file";
    }

    /**
     * 商品添加
     */
    public final static class GoodsAdd {
        public final static String PATH = "/goods/add";
    }


    /**
     * 商品分类
     */
    public final static class GetCate {
        public final static String PATH = "/goods/getCate";
        public final static String cate_id = "cate_id";
    }


    /**
     * 订单列表
     */
    public final static class OrderList {
        public final static String PATH = "/order/orderlist";
        public final static String status = "status";
        public final static String page = "page";
        public final static String pagesize = "pagesize";
    }


    /**
     * 商品待提交
     */
    public final static class AddSave {
        public final static String PATH = "/goods/addSave";
    }


    /**
     * 账单明细
     */
    public final static class CashList {
        public final static String PATH = "/billDetail/cashList";
    }

    /**
     * 待入账明细
     */
    public final static class CashDrzList {
        public final static String PATH = "/billDetail/cashDrzList";
    }


    /*============================  实体店     ======================================*/


    /**
     * 实体店登录
     */
    public final static class StoreLogin {
        public final static String PATH = "/loginRegist";
        public final static String PHONE = "phone";
        public final static String CODE = "code";
    }


    /**
     * 实体信息
     */
    public final static class StoreBindSuper {
        public final static String PATH = "/bindSuper";
        public final static String TYPE = "type";
        public final static String PARENT_ID = "parent_user_no";
        public final static String NAME = "name";
        public final static String PROVINCE = "province";
        public final static String CITY = "city";
        public final static String AREA = "area";
        public final static String ADDRESS = "address";
    }

    /**
     * 获取省市列表
     */
    public final static class AddressWapList {
        public final static String PATH = "/addressWapList";
        public final static String REGION_TYPE = "region_type";
        public final static String PARENT_ID = "parent_id";
    }

    /**
     * 分享二维码
     */
    public final static class ShareInfo {
        public final static String PATH = "/store/shareInfo";
    }

    /**
     * 实体店邀请码(支持支付)
     */
    public final static class StoreQrCode {
        public final static String PATH = "/store/QRcode";
    }

    /**
     * 新手折扣修改
     */
    public final static class DiscountUpdate {
        public final static String PATH = "/discountUpdate";
        public final static String DISCOUNT = "new_discount";
    }


    /**
     * 数据首页
     */
    public final static class DataIndexInfo {
        public final static String PATH = "/store/data/index";
    }

    /**
     * 数据首页
     */
    public final static class StoreDataInfo {
        public final static String PATH = "/store/data/getList";
        public final static String TYPE = "type";
        public final static String PAGE = "page";
        public final static String PAGE_SIZE = "pageSize";
    }

    /**
     * 实体店信息
     */
    public final static class StoreShopInfo {
        public final static String PATH = "/getStoreInfo";
        public final static String TYPE = "type";

    }

    /**
     * 获取实体店收益情况
     */
    public final static class StoreIncome {
        public final static String PATH = "/getStoreIncome";

    }


    /**
     * 实体店提现接口
     */
    public final static class StoreAliPayOut {
        public final static String PATH = "/transferStoreAliPayOut";
        public final static String ACCOUNT = "account";
        public final static String PAY_TYPE = "pay_type";
        public final static String PAY_PROVIDER = "pay_provider";
        public final static String AMOUNT = "amount";
        public final static String NAME = "name";

    }

    /**
     * 取现记录
     */
    public final static class StoreCashLog {
        public final static String PATH = "/getStoreCashLog";
        public final static String PAGE = "page";
        public final static String PAGE_SIZE = "pageSize";
    }


    /**
     * 实体店铺信息修改
     */
    public final static class StoreUpdate {
        public final static String PATH = "/storeUpdate";
        public final static String PROVINCE = "province";
        public final static String CITY = "city";
        public final static String AREA = "area";
        public final static String ADDRESS = "address";
        public final static String STORE_PHONE = "store_phone";
        public final static String HEAD_IMG = "head_img";
        public final static String STORE_IMG = "store_img";
    }

    /**
     * 退出
     */
    public final static class Logout {
        public final static String PATH = "/logout";
    }


}
