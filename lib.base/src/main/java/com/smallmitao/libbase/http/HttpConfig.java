package com.smallmitao.libbase.http;

import android.content.Context;
import com.smallmitao.libbase.BuildConfig;
import com.smallmitao.libbase.http.interceptor.BaseSignInterceptor;
import com.smallmitao.libbase.http.interceptor.ChuBiInterceptor;
import com.zhouyou.http.EasyHttp;
import com.zhouyou.http.cache.converter.SerializableDiskConverter;
import com.zhouyou.http.model.HttpHeaders;
import com.zhouyou.http.utils.HttpLog;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

/**
 *
 * @author Jackshao
 * @date 2018/8/20
 *
 */

public class HttpConfig {

//  public static String HOST = "http://shopapi.smallmitao.com/";
  public static String HOST = "http://featrue_index_190220.shopapi.smallmitao.com/";

   /**
   * 配置网络请求参数
   * @param mContent
   */
  public static void initConfig(Context mContent){

    //设置请求头
    HttpHeaders headers = new HttpHeaders();
//    headers.put(HttpInter.Common.SOURCE, "android");
//    headers.put(HttpInter.Common.ACCEPT, "application/vnd.exchange.mobile-v1+json");


    EasyHttp.getInstance()
      .debug("Jack_HTTP_DeBug", BuildConfig.DEBUG)
      .setReadTimeOut(60 * 1000)
      .setWriteTimeOut(60 * 1000)
      .setConnectTimeout(60 * 1000)
      //默认网络不好自动重试3次
      .setRetryCount(0)
      //每次延时500ms重试
      .setRetryDelay(500)
      //每次延时叠加500ms
      .setRetryIncreaseDelay(500)
      .setBaseUrl(HOST)
      //默认缓存使用序列化转化
      .setCacheDiskConverter(new SerializableDiskConverter())
      //设置缓存大小为50M
      .setCacheMaxSize(50 * 1024 * 1024)
      //缓存版本为1
      .setCacheVersion(1)
      //全局访问规则
      .setHostnameVerifier(new UnSafeHostnameVerifier(HOST))

      //信任所有证书
      .setCertificates()
      //配置https的域名匹配规则，不需要就不要加入，使用不当会导致https握手失败
//      .setHostnameVerifier(new SafeHostnameVerifier())
      //.addConverterFactory(GsonConverterFactory.create(gson))//本框架没有采用Retrofit的Gson转化，所以不用配置
      //设置全局公共头
      .addCommonHeaders(headers)
//      .addCommonParams(params)//设置全局公共参数
      //添加参数签名拦截器
      .addInterceptor(new BaseSignInterceptor())
      //处理自己业务的拦截器
      .addInterceptor(new ChuBiInterceptor());

  }

  public static class UnSafeHostnameVerifier implements HostnameVerifier {
    private String host;

    public UnSafeHostnameVerifier(String host) {
      this.host = host;
      HttpLog.i("###############　UnSafeHostnameVerifier " + host);
    }

    @Override
    public boolean verify(String hostname, SSLSession session) {
      HttpLog.i("############### verify " + hostname + " " + this.host);
//      if (this.host == null || "".equals(this.host) || !this.host.contains(hostname))
//        return false;
      return true;
    }
  }
}
