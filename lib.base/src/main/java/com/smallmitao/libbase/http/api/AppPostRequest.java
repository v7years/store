package com.smallmitao.libbase.http.api;


import com.zhouyou.http.callback.CallBack;
import com.zhouyou.http.callback.CallBackProxy;
import com.zhouyou.http.callback.CallClazzProxy;
import com.zhouyou.http.request.PostRequest;

import java.lang.reflect.Type;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

/**
 * <p>描述：扩展PostRequest请求，解决自定义ApiResult重复写代理的问题</p>
 *
 * post请求，格式类型BaseApiResult
 *
 * @author Jackshao
 */
public class AppPostRequest extends PostRequest {

    public AppPostRequest(String url) {
        super(url);
    }

//    public PostRequest upJson(Object objBean) {//若果你的是obj,你想很容易转json,可以自己新增一个方法obj转json的
//        return super.upJson(StringUtil.getJson(objBean));
//    }

    @Override
    public <T> Observable<T> execute(Type type) {
        return super.execute(new CallClazzProxy<BaseApiResult<T>, T>(type) {
        });
    }

    @Override
    public <T> Observable<T> execute(Class<T> clazz) {
        return super.execute(new CallClazzProxy<BaseApiResult<T>, T>(clazz) {
        });
    }

    @Override
    public <T> Disposable execute(CallBack<T> callBack) {
        return super.execute(new CallBackProxy<BaseApiResult<T>, T>(callBack) {
        });
    }
}
