package com.smallmitao.libbase.http.api;

import com.zhouyou.http.callback.CallBack;
import com.zhouyou.http.callback.CallBackProxy;
import com.zhouyou.http.request.DeleteRequest;

import io.reactivex.disposables.Disposable;


/**
 *
 * delete 请求
 * 返回的数据格式 BaseApiResult这种类型
 *
 * @author Jackshao
 */

public class AppDeleteRequest extends DeleteRequest {

    public AppDeleteRequest(String url) {
        super(url);
    }

    @Override
    public <T> Disposable execute(CallBack<T> callBack) {
        return super.execute(new CallBackProxy<BaseApiResult<T>, T>(callBack) {
        });
    }
}
