package com.smallmitao.libbase.http.api;

import com.zhouyou.http.model.ApiResult;

/**
 * 主要思想就是：默认标准的是code、msg、data三个字段，如果你的结构字段和默认的不一样，<br>
 * 只需要覆写不一样的就可以了，一样的字段不用覆写，添加反而会出问题。<br>
 *
 *   如果字段不是code，msg，data 基类的需要覆写下面的类
 *
 * @author Jackshao
 */

public class BaseApiResult<T> extends ApiResult<T> {


    /**
     * 对应默认标准ApiResult的code
     */
//    int status_code;
    /**、
     * 对应默认标准ApiResult的msg
     */
    String message;
//    T result;//对应默认标准ApiResult的data
//
//    private int code;
//    private String msg;
//      T ret;

//    @Override
//    public T getData() {//因为库里使用data字段，而你的结构是result,所以覆写getData()方法,setData()方法不用覆写
//        return ret;
//    }

    @Override
    public String getMsg() {//因为库里使用msg字段，而你的结构是reason,所以覆写getMsg()方法，setMsg()方法不用覆写
        return message;
    }

//    @Override
//    public int getCode() {//因为库里使用code字段，而你的结构是error_code,所以覆写getCode()方法，setCode()方法不用覆写
//        return code;
//    }

    /**
     * 因为库里使用code字段，code==0表示成功，但是你的数据结构是error_code==0,所以覆写isOk(),用error_code==0做为判断成功的条件
     * @return
     */
    @Override
    public boolean isOk() {
        return getCode() == 0;
    }
}
