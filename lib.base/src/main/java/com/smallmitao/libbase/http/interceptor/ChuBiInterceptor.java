package com.smallmitao.libbase.http.interceptor;

import com.google.gson.Gson;
import com.zhouyou.http.interceptor.BaseExpiredInterceptor;
import com.zhouyou.http.model.ApiResult;
import com.zhouyou.http.utils.HttpLog;

import okhttp3.Response;

/**
 *  业务拦截器
 * @author Jackshao
 */
public class ChuBiInterceptor extends BaseExpiredInterceptor {

    private ApiResult apiResult;

    @Override
    public boolean isResponseExpired(Response response, String bodyString) {
        apiResult = new Gson().fromJson(bodyString, ApiResult.class);
        if (apiResult != null) {
            HttpLog.d("请求出现错误拦截====>"+apiResult.getCode());
            int code = apiResult.getCode();
//            if (code == ChuBiContact.FailureCode.BAD_REQUEST
//              || code == ChuBiContact.FailureCode.REQUEST_PARAMS_NOT_VALID
//              || code == ChuBiContact.FailureCode.FORBIDDEN
//              || code == ChuBiContact.FailureCode.RESOURCE_CONFLICT
//              || code == ChuBiContact.FailureCode.RESOURCE_NONE
//              || code == ChuBiContact.FailureCode.RESOURCE_LOCKED
//              || code == ChuBiContact.FailureCode.TOKEN_EXPIRED
//              || code == ChuBiContact.FailureCode.TOKEN_INVALID
//              ) {
//                return true;
//            }
        }
        return false;
    }

    /**
     * 只有上面配置的过期情况才会执行这里
     * @param chain
     * @param bodyString
     * @return
     */
    @Override
    public Response responseExpired(Chain chain, String bodyString) {
        try {
//            switch (apiResult.getCode()) {
//                case ChuBiContact.FailureCode.BAD_REQUEST:
//                    break;
//                default:
//                    break;
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
