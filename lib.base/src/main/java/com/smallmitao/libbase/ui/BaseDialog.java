package com.smallmitao.libbase.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.smallmitao.libbase.R;
import com.smallmitao.libbase.util.SystemUtil;


public abstract class BaseDialog extends AlertDialog {
    public Context mContext;
    public View mView;
    public Window window;


    public BaseDialog(Context context, int layoutResID, int theme) {
        super(context, theme);
        this.mContext = context;
        this.mView = LayoutInflater.from(context).inflate(layoutResID, null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(mView);
        setParams();
    }

    public void setParams() {
        window = getWindow();
        window.setGravity(Gravity.BOTTOM);
        window.setWindowAnimations(R.style.mystyle); // 添加动画
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        lp.height = (int) (SystemUtil.getScreenHeight(mContext) * mRatio);     //dialog屏幕占比
        window.setAttributes(lp);
    }

}
