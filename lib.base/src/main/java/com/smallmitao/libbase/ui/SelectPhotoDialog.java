package com.smallmitao.libbase.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.smallmitao.libbase.R;


public class SelectPhotoDialog extends BaseDialog implements View.OnClickListener {

    private TextView cancel;
    private TextView selectPhoto;
    private TextView takePhoto;

    private Context mContext;
    private ClickListener mClickListener;

    public void setClickListener(ClickListener clickListener) {
        mClickListener = clickListener;
    }

    public SelectPhotoDialog(Context context) {
        super(context, R.layout.dialog_select_photo, R.style.Dialog);
        this.mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCanceledOnTouchOutside(true);
        cancel = mView.findViewById(R.id.tv_cancel);
        selectPhoto = mView.findViewById(R.id.select_photo);
        takePhoto = mView.findViewById(R.id.take_photo);
        cancel.setOnClickListener(this);
        selectPhoto.setOnClickListener(this);
        takePhoto.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.tv_cancel) {
            this.dismiss();
        } else if (i == R.id.take_photo) {
            mClickListener.onTakePhotoListener();
        } else if (i == R.id.select_photo) {
            mClickListener.onSelectPhotoListener();
        }
    }

    public interface ClickListener {
        void onTakePhotoListener();
        void onSelectPhotoListener();
    }
}
