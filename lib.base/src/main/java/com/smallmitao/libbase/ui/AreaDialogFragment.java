package com.smallmitao.libbase.ui;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.smallmitao.libbase.R;
import com.smallmitao.libbase.bean.AddressListBean;
import com.smallmitao.libbase.bean.AreaBean;
import com.smallmitao.libbase.http.RetrofitHelper;
import com.smallmitao.libbase.http.appinterface.HttpInter;
import com.smallmitao.libbase.ui.adapter.AreaDialogAdapter;
import com.smallmitao.libbase.util.Toastor;
import com.zhouyou.http.callback.SimpleCallBack;
import com.zhouyou.http.exception.ApiException;
import com.zhouyou.http.request.GetRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.reactivex.disposables.CompositeDisposable;


/**

 */

public class AreaDialogFragment extends DialogFragment implements View.OnClickListener {


    TextView mTvOne;
    TextView mTvTwo;
    TextView mTvThree;
    TextView mConfirm;
    RecyclerView mRvCentent;
    private AreaDialogAdapter mAdapter;
    private int mType = 1;
    private List<AreaBean> dataList;
    private AreaCallback mAreaCallback;

    private long lastClickTime = 0L;
    private static final int FAST_CLICK_DELAY_TIME = 500;  // 快速点击间隔
    private CompositeDisposable mCompositeDisposable;


    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        window.setGravity(Gravity.BOTTOM);
        window.setWindowAnimations(R.style.mystyle); // 添加动画
        final WindowManager m = window.getWindowManager();
        Display d = m.getDefaultDisplay();
        WindowManager.LayoutParams lp = window.getAttributes();
        //这句就是设置dialog横向满屏了。
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = (int) (d.getHeight() * 0.6);     //dialog屏幕占比
        window.setAttributes(lp);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = View.inflate(getContext(), R.layout.view_area_dialog, container);
        mTvOne = view.findViewById(R.id.tv_one);
        mTvTwo = view.findViewById(R.id.tv_two);
        mTvThree = view.findViewById(R.id.tv_three);
        mConfirm = view.findViewById(R.id.confirm);
        mRvCentent = view.findViewById(R.id.rv_centent);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        dataList = new ArrayList<>();
        mRvCentent.setLayoutManager(new LinearLayoutManager(getActivity()));
        mConfirm.setOnClickListener(this);
        getNetData(null);
    }

    public void setOnAreaCallBack(AreaCallback callback) {
        this.mAreaCallback = callback;
    }

    @Override
    public void onClick(View v) {
        if (dataList.size() == 3) {
            mAreaCallback.onAreaCallBack(dataList);
            dismiss();
        } else {
            Toastor.showToast("请选择省市区");
        }
    }

    public void getNetData(String parent_id) {
        mCompositeDisposable = new CompositeDisposable();
        RetrofitHelper helper = new RetrofitHelper();
        GetRequest params = helper.getRequest(HttpInter.AddressWapList.PATH)
                .params(HttpInter.AddressWapList.REGION_TYPE, String.valueOf(mType));
        if (!TextUtils.isEmpty(parent_id)) {
            params.params(HttpInter.AddressWapList.PARENT_ID, parent_id);
        }
        mCompositeDisposable.add(params
                .execute(new SimpleCallBack<AddressListBean>() {
                    @Override
                    public void onError(ApiException e) {
                        Toastor.showToast(e.getMessage());
                    }

                    @Override
                    public void onSuccess(AddressListBean chooseAreaBean) {
                        Map<String, String> map = chooseAreaBean.getList();
                        List<AreaBean> areaBeanList = new ArrayList<>();
                        for (Map.Entry<String, String> entry : map.entrySet()) {
                            areaBeanList.add(new AreaBean(entry.getKey(), entry.getValue()));
                        }
                        if (mAdapter == null) {
                            mAdapter = new AreaDialogAdapter(areaBeanList);
                            mRvCentent.setAdapter(mAdapter);
                            mRvCentent.addOnItemTouchListener(new OnItemClickListener() {
                                @Override
                                public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {

                                    if (System.currentTimeMillis() - lastClickTime < FAST_CLICK_DELAY_TIME) {
                                        return;
                                    }
                                    lastClickTime = System.currentTimeMillis();

                                    AreaBean info = mAdapter.getData().get(position);
                                    if (mType == 1) {
                                        mType++;
                                        getNetData(info.getKey());
                                        mTvOne.setText(info.getValue());
                                        mTvTwo.setText("请选择");
                                        dataList.add(info);
                                    } else if (mType == 2) {
                                        mType++;
                                        getNetData(info.getKey());
                                        mTvTwo.setText(info.getValue());
                                        mTvThree.setText("请选择");
                                        dataList.add(info);
                                    } else if (mType == 3) {
                                        mType++;
                                        getNetData(info.getKey());
                                        mTvThree.setText(info.getValue());
                                        mConfirm.setTextColor(getResources().getColor(R.color.gray_3));
                                        dataList.add(info);
                                        if (dataList != null && dataList.size() > 0)
                                            mAreaCallback.onAreaCallBack(dataList);
                                        dismiss();
                                    } else {
                                        mTvThree.setText(info.getValue());
                                        dataList.remove(dataList.size() - 1);
                                        dataList.add(info);
                                    }
                                }
                            });
                        } else {
                            if (mType <= 3) {
                                mAdapter.setNewData(areaBeanList);
                                if (mType == 3) {
                                    mConfirm.setClickable(true);
                                } else if (mType < 3) {
                                    mConfirm.setClickable(false);
                                }
                            }
                        }
                    }
                }));

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mCompositeDisposable != null)
            mCompositeDisposable.isDisposed();
    }

    public interface AreaCallback {
        void onAreaCallBack(List<AreaBean> list);
    }
}
