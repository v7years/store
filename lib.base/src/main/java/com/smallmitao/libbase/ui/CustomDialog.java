package com.smallmitao.libbase.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import com.smallmitao.libbase.R;

/**
 * @author Jackshao
 */
public class CustomDialog extends Dialog {
    public Context context;
    private View.OnClickListener mOnclick;

    public CustomDialog(Context context) {
        super(context);
        this.context = context;
    }

    public CustomDialog(Context context, int theme,View.OnClickListener onClickListener) {
        super(context, theme);
        this.mOnclick = onClickListener;
        this.context = context;
    }

    public CustomDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.custom_dialog, null);
        setContentView(view);

        setCanceledOnTouchOutside(true);

        view.findViewById(R.id.btn_cancel).setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        view.findViewById(R.id.commit_btn).setOnClickListener(mOnclick);
    }
}
