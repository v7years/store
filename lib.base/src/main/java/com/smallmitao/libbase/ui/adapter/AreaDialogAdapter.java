package com.smallmitao.libbase.ui.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.smallmitao.libbase.R;
import com.smallmitao.libbase.bean.AreaBean;

import java.util.List;

/**
 */

public class AreaDialogAdapter extends BaseQuickAdapter<AreaBean, BaseViewHolder> {


    public AreaDialogAdapter(List<AreaBean> data) {
        super(R.layout.item_textview, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, AreaBean item) {
        helper.setText(R.id.tv_text,item.getValue());
    }

}