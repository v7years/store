package com.smallmitao.libbase.mvp;

/**
 * Description: BasePresenter
 * date: 18/8/10
 * 控制界面的生命周期
 * @author Jackshao
 */
public interface BasePresenter<T extends BaseView> {

    /**
     * 控制页面的启动
     * @param view
     */
    void attachView(T view);

    /**
     * 控制页面的结束
     */
    void detachView();
}
