package com.smallmitao.libbase.mvp;


import java.lang.ref.WeakReference;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * 基于Rx的Presenter封装,控制订阅的生命周期
 * @author Jackshao
 */
public class RxPresenter<T extends BaseView> implements BasePresenter<T> {

    private WeakReference<T> mView;

    private CompositeDisposable mCompositeDisposable;

    public void clearDisposable() {
        if (null != mCompositeDisposable) {
            mCompositeDisposable.clear();
        }
    }

    protected void addDisposable(Disposable disposable) {
        if (null == disposable){
            return;
        }
        if (null == mCompositeDisposable) {
            mCompositeDisposable = new CompositeDisposable();
        }
        mCompositeDisposable.add(disposable);
    }


    public T getView() {
        return mView!=null?mView.get():null;
    }


    @Override
    public void attachView(T view) {
        this.mView = new WeakReference<>(view);
    }

    @Override
    public void detachView() {
        clearDisposable();
        if (mView != null) {
            mView.clear();
            mView = null;
        }
        this.mCompositeDisposable = null;
    }

}
