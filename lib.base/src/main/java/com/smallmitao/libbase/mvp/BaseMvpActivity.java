package com.smallmitao.libbase.mvp;

import android.os.Bundle;

import com.smallmitao.libbase.base.BaseActivity;

import javax.inject.Inject;

/**
 * Description: MVP Activity基类
 * Creator: jackshao
 * date: 17/9/14
 * @author Jackshao
 */
public abstract class BaseMvpActivity<T extends BasePresenter> extends BaseActivity implements BaseView {

    @Inject
    protected T mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        initInject();
        if (mPresenter != null){
            mPresenter.attachView(this);
        }

        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPresenter != null) {
            mPresenter.detachView();
            this.mPresenter = null;
        }
    }

    protected abstract void initInject();

}
