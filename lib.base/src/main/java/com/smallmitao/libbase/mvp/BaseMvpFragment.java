package com.smallmitao.libbase.mvp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import com.smallmitao.libbase.base.BaseFragment;
import javax.inject.Inject;

/**
 * @author Jackshao
 */

public abstract class BaseMvpFragment<T extends BasePresenter> extends BaseFragment implements BaseView {

    @Inject
    protected T mPresenter;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        initInject();
        if (mPresenter != null){
            mPresenter.attachView(this);
        }
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroy() {
        if (mPresenter != null) {
            mPresenter.detachView();
        }
        mPresenter = null;
        super.onDestroy();
    }

    /**
     * 初始化
     */
    protected abstract void initInject();
}
