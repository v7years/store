package com.smallmitao.libbase.bean;

import java.util.Map;

public class AddressListBean {
    private Map<String, String> list;

    public Map<String, String> getList() {
        return list;
    }

    public void setList(Map<String, String> list) {
        this.list = list;
    }
}
