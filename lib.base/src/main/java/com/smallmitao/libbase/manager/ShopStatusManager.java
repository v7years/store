package com.smallmitao.libbase.manager;

import android.text.TextUtils;

import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.http.appinterface.HttpInter;
import com.smallmitao.libbase.util.ACache;
import com.smallmitao.libbridge.router.bean.LoginBean;
import com.smallmitao.libbridge.router.bean.ShopStatusBean;


/**
 * 用户信息管理
 * @author Jackshao
 */
public class ShopStatusManager {

    private static ShopStatusManager instance = null;
    private final static String key = "business_user_key";
    private ShopStatusBean shopStatusBean;
    private ACache aCache;

    public ShopStatusManager() {
        this.aCache = ACache.get(BaseApp.getInstance(), key);
    }

    public static ShopStatusManager getInstance() {
        if (instance == null) {
            synchronized (ShopStatusManager.class) {
                if (instance == null) {
                    instance = new ShopStatusManager();
                }
            }
        }
        return instance;
    }


    /**
     * @param model
     */
    public void setShopStatus(ShopStatusBean model) {
        this.shopStatusBean = model;
        this.aCache.put(HttpInter.Login.LOGIN_STATUS, this.shopStatusBean);
    }


    public ShopStatusBean getShopStatus() {
        if (this.shopStatusBean == null) {
            Object object = aCache.getAsObject(HttpInter.Login.LOGIN_STATUS);
            if (object != null) {
                this.shopStatusBean = (ShopStatusBean) object;
            }
        }
        return this.shopStatusBean;
    }


    /**
     */
    public void clearShopStatus() {
        ShopStatusBean loginBean = new ShopStatusBean();
        loginBean.setStatus(-1);
        this.shopStatusBean = loginBean;
        aCache.put(HttpInter.Login.LOGIN_STATUS, this.shopStatusBean);
        aCache.clear();
    }


}
