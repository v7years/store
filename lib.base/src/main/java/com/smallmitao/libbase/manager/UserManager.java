package com.smallmitao.libbase.manager;

import android.text.TextUtils;

import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.http.appinterface.HttpInter;
import com.smallmitao.libbase.util.ACache;
import com.smallmitao.libbridge.router.bean.LoginBean;
import com.smallmitao.libbridge.router.bean.StoreLoginBean;


/**
 * 用户信息管理
 * @author Jackshao
 */
public class UserManager {

    private static UserManager instance = null;
    private final static String key = "business_user_key";
    private LoginBean loginBean;
    private StoreLoginBean mStoreLoginBean;
    private ACache aCache;

    public UserManager() {
        this.aCache = ACache.get(BaseApp.getInstance(), key);
    }

    public static UserManager getInstance() {
        if (instance == null) {
            synchronized (UserManager.class) {
                if (instance == null) {
                    instance = new UserManager();
                }
            }
        }
        return instance;
    }

    /**
     * 是否登录
     * @return
     */
    public boolean isLogin() {
        if (getLogin() != null && !TextUtils.isEmpty(getLogin().getToken())) {
            return true;
        }
        return false;
    }

    /**
     * 得到登录信息
     * @return
     */
    public LoginBean getLogin() {
        if (this.loginBean == null || TextUtils.isEmpty(loginBean.getToken())) {
            Object object = aCache.getAsObject(HttpInter.Login.LOGIN_MANAGER);
            if (object != null) {
                if(object instanceof LoginBean){
                    this.loginBean = (LoginBean) object;
                }
            }
        }
        return this.loginBean;
    }

    /**
     * 设置登录信息
     * @param model
     */
    public void setLogin(LoginBean model) {
        this.loginBean = model;
        this.aCache.put(HttpInter.Login.LOGIN_MANAGER, this.loginBean);
    }

    /**
     * 清空登录信息
     */
    public void clearLogin() {
        LoginBean loginBean = new LoginBean();
        loginBean.setToken("");
        this.loginBean = loginBean;
        aCache.put(HttpInter.Login.LOGIN_MANAGER, this.loginBean);
        aCache.clear();
    }


    /**
     *
     * 实体店
     */

    /**
     * 是否登录
     * @return
     */
    public boolean isStoreLogin() {
        if (getStoreLogin() != null && !TextUtils.isEmpty(getStoreLogin().getToken())) {
            return true;
        }
        return false;
    }

    /**
     * 得到登录信息
     * @return
     */
    public StoreLoginBean getStoreLogin() {
        if (this.mStoreLoginBean == null || TextUtils.isEmpty(mStoreLoginBean.getToken())) {
            Object object = aCache.getAsObject(HttpInter.Login.LOGIN_MANAGER);
            if (object != null) {
                this.mStoreLoginBean = (StoreLoginBean) object;
            }
        }
        return this.mStoreLoginBean;
    }

    /**
     * 设置登录信息
     * @param model
     */
    public void setStoreLogin(StoreLoginBean model) {
        this.mStoreLoginBean = model;
        this.aCache.put(HttpInter.Login.LOGIN_MANAGER, this.mStoreLoginBean);
    }

    /**
     * 清空登录信息
     */
    public void clearStoreLogin() {
        StoreLoginBean loginBean = new StoreLoginBean();
        loginBean.setToken("");
        this.mStoreLoginBean = loginBean;
        aCache.put(HttpInter.Login.LOGIN_MANAGER, this.mStoreLoginBean);
        aCache.clear();
    }


}
