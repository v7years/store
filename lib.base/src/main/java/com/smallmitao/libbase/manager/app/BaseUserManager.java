package com.smallmitao.libbase.manager.app;

import com.alibaba.android.arouter.launcher.ARouter;
import com.smallmitao.libbridge.router.BridgePass;
import com.smallmitao.libbridge.router.service.ILoginInfoService;

/**
 * 登录用户信息管理
 *
 * @author Jackshao
 * @date 2018/9/28
 */

public class BaseUserManager {

    private static BaseUserManager instance = null;

    public BaseUserManager() {
    }

    public static BaseUserManager getInstance() {
        if (instance == null) {
            synchronized (BaseUserManager.class) {
                if (instance == null) {
                    instance = new BaseUserManager();
                }
            }
        }
        return instance;
    }

    public ILoginInfoService getILoginInfoService() {
        return (ILoginInfoService) ARouter.getInstance().build(BridgePass.SERVICE_PASS_LOGIN).navigation();
    }

    /**
     * 得到token
     *
     * @return
     */
    public String getToken() {
        ILoginInfoService service = getILoginInfoService();
        if (service.isLogin() && service.getLoginInfo() != null) {
            return "Bearer " + service.getLoginInfo().getToken();
        } else {
            return "";
        }
    }

    /**
     * 得到token
     *
     * @return
     */
    public String getStoreToken() {
        ILoginInfoService service = getILoginInfoService();
        if (service.isStoreLogin() && service.getStoreLoginInfo() != null) {
            return "Bearer " + service.getStoreLoginInfo().getToken();
        } else {
            return "";
        }
    }

    /**
     * @return
     */
    public int getUserStatus() {
        ILoginInfoService service = getILoginInfoService();
        if (service.isLogin()) {
            return service.getLoginInfo().getStatus();
        }
        return 0;
    }


    public String getUserReason() {
        ILoginInfoService service = getILoginInfoService();
        if (service.isLogin()) {
            return service.getLoginInfo().getReason();
        }
        return "";
    }


    public void setStoreLoginOut() {
        ILoginInfoService service = getILoginInfoService();
        service.setStoreLoginOut();
    }
}
