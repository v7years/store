package com.smallmitao.libbase.util;


import android.graphics.BitmapFactory;
import android.os.Environment;
import com.smallmitao.libbase.base.BaseApp;
import com.zhouyou.http.utils.HttpLog;
import java.io.File;
import java.util.List;
import java.util.Locale;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import top.zibin.luban.Luban;

/**
 * 压缩多张图片
 * jackshao
 * @author Jackshao
 */

public class LuBanUtils {

  private LuBanUtils() {
  }
  private static class SingletonLoader {
    private static final LuBanUtils INSTANCE = new LuBanUtils();
  }
  public static LuBanUtils getInstance() {
    return LuBanUtils.SingletonLoader.INSTANCE;
  }

  public interface OnLFinishListener {
    void luBanFinish(List<File> onResultFiles);
  }

  /**
   * 压缩多张照片
   * @param mDisposable
   * @param photos
   * @param <T>
   */
  public <T> void withRx(CompositeDisposable mDisposable, final List<T> photos, final OnLFinishListener onLFinishListener) {

    mDisposable.add(Flowable.just(photos)
      .observeOn(Schedulers.io())
      .map(new Function<List<T>, List<File>>() {
        @Override
        public List<File> apply(@NonNull List<T> list) throws Exception {
          return Luban.with(BaseApp.getInstance())
//            .setTargetDir(getPath())//保存的路径
            .putGear(3)
            .ignoreBy(200)
            .load(list)
            .get();
        }
      })
      .observeOn(AndroidSchedulers.mainThread())
      .doOnError(new Consumer<Throwable>() {
        @Override
        public void accept(Throwable throwable) {
          HttpLog.d("压缩工程失败====>"+throwable.getMessage());
        }
      })
      .onErrorResumeNext(Flowable.<List<File>>empty())
      .subscribe(new Consumer<List<File>>() {
        @Override
        public void accept(@NonNull List<File> list) {
          if (list.size() == photos.size()){
//            List<LuBanFileModel> modelList = new ArrayList<>();
//            for (File file : list) {
//              int[] thumbSize = computeSize(file);
//              modelList.add(new LuBanFileModel(file,thumbSize[0], thumbSize[1]));
//            }
            if (onLFinishListener!=null){
              onLFinishListener.luBanFinish(list);
            }
          }
        }
      }));
  }


  private void showResult(File file) {
//    int[] originSize = computeSize(photos.get(0));
    int[] thumbSize = computeSize(file);
//    String originArg = String.format(Locale.CHINA, "原图参数：%d*%d, %dk", originSize[0], originSize[1], photos.get(0).length() >> 10);
    String thumbArg = String.format(Locale.CHINA, "压缩后参数：%d*%d, %dk", thumbSize[0], thumbSize[1], file.length() >> 10);
  }

  private int[] computeSize(File srcImg) {
    int[] size = new int[2];

    BitmapFactory.Options options = new BitmapFactory.Options();
    options.inJustDecodeBounds = true;
    options.inSampleSize = 1;

    BitmapFactory.decodeFile(srcImg.getAbsolutePath(), options);
    size[0] = options.outWidth;
    size[1] = options.outHeight;

    return size;
  }


  private String getPath() {
    String path = Environment.getExternalStorageDirectory() + "smallmitaoBusiness/image/";
    File file = new File(path);
    if (file.mkdirs()) {
      return path;
    }
    return path;
  }






}
