package com.smallmitao.libbase.util;

import android.app.Dialog;
import android.content.Context;
import android.widget.TextView;

import com.smallmitao.libbase.R;


/**
 *
 * @author Jackshao
 * @date 2018/6/28
 */


public class DialogUtil extends Dialog {

  private TextView tvInfo;

  private Context context;

  public DialogUtil(Context context, String content) {
    super(context, R.style.LoadingDialog);
    this.context = context;
    initDialog(content);
  }
  private void initDialog(String content) {
    setContentView(R.layout.loading_dialog_view);
    tvInfo = findViewById(R.id.tv);
    tvInfo.setText(content);
  }

  public void showDialog() {
    show();
  }
  public void cancelDialog() {
    this.cancel();
  }

}
