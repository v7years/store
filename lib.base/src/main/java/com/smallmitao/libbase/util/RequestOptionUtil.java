package com.smallmitao.libbase.util;


import android.graphics.Bitmap;

import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.RequestOptions;
import com.smallmitao.libbase.R;
import com.smallmitao.libbase.base.BaseApp;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;


/**
 * @author Jackshao
 */
public class RequestOptionUtil {

    private static class SingletonLoader {
        private static final RequestOptionUtil INSTANCE = new RequestOptionUtil();
    }

    public static RequestOptionUtil getInstance() {
        return SingletonLoader.INSTANCE;
    }


    public RequestOptions getRequestOptions() {
        return getRequestOptions(5);
    }

    /**
     * 全圆角默认配置
     *
     * @return
     */
    public RequestOptions getRequestOptions(int radio) {
        return new RequestOptions()
//      .placeholder(R.drawable.indicator_normal)
//      .error(R.drawable.indicator_normal)
//      .fallback(R.drawable.indicator_normal)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontAnimate() //不加载过度动画
//      .encodeQuality(10)
                .centerCrop()
                .transform(getMultiTransformation(0, radio));
    }


    /**
     * 默认配置
     *
     * @return
     */
    public RequestOptions getRequestDfOptions() {
        return new RequestOptions()
//      .placeholder(R.mipmap.default_image)
//      .error(R.mipmap.default_image)
//      .fallback(R.mipmap.default_image)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL);
    }


    public RequestOptions getRequestDfOptions(int w, int h) {
        return new RequestOptions()
//      .placeholder(R.drawable.shape_image_default_bg)
//      .error(R.drawable.shape_image_default_bg)
//      .fallback(R.drawable.shape_image_default_bg)
                .override(w, h)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL);
    }

    /**
     * 头像
     *
     * @return
     */
    public RequestOptions requestHeadOptions() {
        return new RequestOptions()
//      .placeholder(R.mipmap.default_avatar)
                .circleCrop()
                .encodeQuality(90)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
    }

    /**
     * 需要穿宽和高的配置
     *
     * @param w
     * @param h
     * @return
     */
    public RequestOptions getRequestDfTopOptionsHW(int w, int h) {
        return new RequestOptions()
//      .placeholder(R.drawable.shape_image_default_bg)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .override(w, h)
                .encodeQuality(10) //质量
                .dontAnimate() //不加载过度动画
                .transform(getMultiTransformation(1, 5));
    }


    public RequestOptions getRequestDfTopOptions() {
        return new RequestOptions()
//      .placeholder(R.drawable.shape_image_default_bg)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontAnimate() //不加载过度动画
                .transform(getMultiTransformation(1, 5));
    }


    public RequestOptions getRequestDfBottomOptions() {
        return new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .dontAnimate() //不加载过度动画
                .fitCenter()
                .transform(getMultiTransformation(2, 5));
    }

    /**
     * 圆形图
     *
     * @return
     */
    public RequestOptions getCircleOptions() {
        return new RequestOptions()
                .placeholder(R.mipmap.default_header)
                .error(R.mipmap.default_header)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontAnimate() //不加载过度动画
//      .encodeQuality(10)
//            .centerCrop()
                .transform(new GlideCircleTransform(BaseApp.getInstance(), 1, BaseApp.getInstance().getResources().getColor(R.color.white)));
    }


    /**
     * 设置圆角和方向
     *
     * @return
     */
    public MultiTransformation<Bitmap> getMultiTransformation(int type, int radio) {
        RoundedCornersTransformation.CornerType cornerType = null;
        switch (type) {
            case 0:
                cornerType = RoundedCornersTransformation.CornerType.ALL;
                break;
            case 1:
                cornerType = RoundedCornersTransformation.CornerType.TOP;
                break;
            case 2:
                cornerType = RoundedCornersTransformation.CornerType.BOTTOM;
                break;
            default:
                break;
        }
        return new MultiTransformation<>(
                new CenterCrop(),
                new RoundedCornersTransformation(SystemUtil.dp2px(BaseApp.getInstance(), radio), 0, cornerType));
    }


}
