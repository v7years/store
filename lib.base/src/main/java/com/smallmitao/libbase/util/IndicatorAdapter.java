package com.smallmitao.libbase.util;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.smallmitao.libbase.R;
import com.smallmitao.libbase.ui.ColorFlipPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.UIUtil;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jackshao
 * @date 2018/10/11
 */

public class IndicatorAdapter extends CommonNavigatorAdapter {

  private List<String> mDataList = new ArrayList<>();
  private ViewPager mViewPager;

  public IndicatorAdapter(List<String> mDataList, ViewPager mViewPager){
    this.mDataList = mDataList;
    this.mViewPager = mViewPager;
  }

  @Override
  public int getCount() {
    return mDataList == null ? 0 : mDataList.size();
  }


  @Override
  public IPagerTitleView getTitleView(Context context, final int index) {
    SimplePagerTitleView simplePagerTitleView = new ColorFlipPagerTitleView(context);
    simplePagerTitleView.setNormalColor(context.getResources().getColor(R.color.black_33));
    simplePagerTitleView.setSelectedColor(context.getResources().getColor(R.color.indicator_title_text_press));
    simplePagerTitleView.setText(mDataList.get(index));
    simplePagerTitleView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        mViewPager.setCurrentItem(index);
      }
    });
    return simplePagerTitleView;
  }

  @Override
  public IPagerIndicator getIndicator(Context context) {
    LinePagerIndicator indicator = new LinePagerIndicator(context);
    indicator.setMode(LinePagerIndicator.MODE_EXACTLY);
    indicator.setLineHeight(UIUtil.dip2px(context, 2));
    indicator.setLineWidth(UIUtil.dip2px(context, 50));
    indicator.setRoundRadius(UIUtil.dip2px(context, 2));
    indicator.setStartInterpolator(new AccelerateInterpolator());
    indicator.setEndInterpolator(new DecelerateInterpolator(2.0f));
    indicator.setColors(context.getResources().getColor(R.color.indicator_title_text_press));
    return indicator;
  }

}
