package com.smallmitao.libbase.util;

import android.content.Context;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.widget.Button;
import android.widget.EditText;

import com.smallmitao.libbase.R;

/**
 *
 * @author Jackshao
 * @date 2018/6/28
 */

public class EditTextSizeCheckUtil {

  private Button button;
  private EditText[] editTexts;
  private Context mContext;

  public EditTextSizeCheckUtil(Context context){
    this.mContext = context;
  }

  /**
   * 设置只能输入数字跟字母
   * @param mEdit
   */
  public void setZmData(EditText mEdit){
    mEdit.setKeyListener(new DigitsKeyListener(){
      @Override
      public int getInputType() {
        return InputType.TYPE_TEXT_VARIATION_PASSWORD;
      }

      @Override
      protected char[] getAcceptedChars() {
        return mContext.getResources().getString(R.string.edit_zm_data).toCharArray();
      }
    });
  }

  /**
   * 设置按钮跟editText绑定
   */
  public void setButtonEdit(Button button,EditText... editTexts){
    this.button = button;
    this.editTexts = editTexts;
    initEditListener();
  }

  private void initEditListener() {
    for (EditText editText:editTexts){
      editText.addTextChangedListener(new TextChange());
    }
  }

  /**
   * edit输入的变化来改变按钮的是否点击
   */
  private class TextChange implements TextWatcher {

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
      if (checkAllEdit()){
        button.setEnabled(true);
      }else {
        button.setEnabled(false);
      }
    }

    @Override
    public void afterTextChanged(Editable editable) {
    }
  }

  /**
   * 检查所有的edit是否输入了数据
   * @return
   */
  private boolean checkAllEdit() {
    for (EditText editText:editTexts){
      if (!TextUtils.isEmpty(editText.getText() + "")){
        continue;
      }else {
        return false;
      }
    }
    return true;
  }

}
