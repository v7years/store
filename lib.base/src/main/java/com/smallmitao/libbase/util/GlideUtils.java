package com.smallmitao.libbase.util;


import android.graphics.Bitmap;
import android.net.Uri;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.smallmitao.libbase.base.BaseApp;

import java.io.ByteArrayOutputStream;


/**
 * glide 加载图片
 * @author Jackshao
 */

public class GlideUtils {

  private GlideUtils() {
  }
  private static class SingletonLoader {
    private static final GlideUtils INSTANCE = new GlideUtils();
  }
  public static GlideUtils getInstance() {
    return GlideUtils.SingletonLoader.INSTANCE;
  }

//  private DrawableTransitionOptions transitionOptions = new DrawableTransitionOptions()
//    .crossFade();

  /**
   * 加载头像
   * @param imageView
   * @param url
   */
  public void loadCircleHead(ImageView imageView,String url){
    Glide.with(BaseApp.getInstance())
      .load(url)
      .apply(RequestOptionUtil.getInstance().requestHeadOptions())
      .into(imageView);
  }


  public void loadUrl(ImageView imageView,String url){
    Glide.with(BaseApp.getInstance())
      .load(url)
      .apply(RequestOptionUtil.getInstance().getRequestOptions(0))
      .into(imageView);
  }

  /**
   * 加载本地图片
   * @param imageView
   * @param url
   */
  public void loadDrawable(ImageView imageView,int url){
    Glide.with(BaseApp.getInstance())
      .load(url)
      .apply(RequestOptionUtil.getInstance().getRequestOptions(0))
      .into(imageView);
  }

  public void loadBitmapImage(Bitmap bitmap,ImageView imageView){
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
    byte[] bytes= baos.toByteArray();
    Glide.with(BaseApp.getInstance())
      .asBitmap()
      .load(bytes)
      .apply(RequestOptionUtil.getInstance().getRequestOptions(0))
      .into(imageView);
  }

  /**
   * 加载本地
   * @param imageView
   * @param url
   */
  public void loadCircleHead(ImageView imageView,Uri url){
    Glide.with(BaseApp.getInstance())
      .load(url)
      .apply(RequestOptionUtil.getInstance().getCircleOptions())
      .into(imageView);
  }




  /**
   * 圆角
   * @param imageView
   * @param url
   */
  public void loadCircleImage(ImageView imageView,String url){
    Glide.with(BaseApp.getInstance())
      .load(url)
      .apply(RequestOptionUtil.getInstance().getCircleOptions())
//      .transition(transitionOptions)
      .into(imageView);
  }


  /**
   * 加载完自定义尺寸
   * @param url
   * @param simpleTarget
   */
  public void loadCircleImage(String url, SimpleTarget<Bitmap> simpleTarget){
    Glide.with(BaseApp.getInstance())
      .asBitmap()
      .load(url)
      .apply(RequestOptionUtil.getInstance().getRequestOptions(0))
      .into(simpleTarget);
  }


  public void loadCircleImageR(String url,int radio, SimpleTarget<Bitmap> simpleTarget){
    Glide.with(BaseApp.getInstance())
      .asBitmap()
      .load(url)
      .apply(RequestOptionUtil.getInstance().getRequestOptions(radio))
      .into(simpleTarget);
  }

  /**
   * 正常 图片
   * @param imageView
   * @param url
   */
  public void loadNormalImage(ImageView imageView,String url){
    Glide.with(BaseApp.getInstance())
      .load(url)
      .apply(RequestOptionUtil.getInstance().getRequestDfOptions())
//      .transition(transitionOptions)
      .into(imageView);
  }


  public void loadNormalFileImage(ImageView imageView,Uri url,int radio){
    Glide.with(BaseApp.getInstance())
      .load(url)
      .apply(RequestOptionUtil.getInstance().getRequestOptions(radio))
      .into(imageView);
  }

  public void loadNormalDrawImage(ImageView imageView,int url,int radio){
    Glide.with(BaseApp.getInstance())
      .load(url)
      .apply(RequestOptionUtil.getInstance().getRequestOptions(radio))
      .into(imageView);
  }

  public void loadNormalWHImage(ImageView imageView,String url,int w,int h){
    Glide.with(BaseApp.getInstance())
      .load(url)
      .apply(RequestOptionUtil.getInstance().getRequestDfOptions(w,h))
//      .transition(transitionOptions)
      .into(imageView);
  }


  public void loadCircleImage(Bitmap bitmap,ImageView imageView){
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
    byte[] bytes= baos.toByteArray();
    Glide.with(BaseApp.getInstance())
      .asBitmap()
      .load(bytes)
      .apply(RequestOptionUtil.getInstance().getRequestOptions())
      .into(imageView);
  }



  /**
   * 上圆角图片
   * @param imageView
   * @param url
   * @param w
   * @param h
   */
  public void loadTopImageWH(ImageView imageView,String url,int w,int h){
    Glide.with(BaseApp.getInstance())
      .load(url)
      .apply(RequestOptionUtil.getInstance().getRequestDfTopOptionsHW(w,h))
//      .transition(transitionOptions)
      .into(imageView);
  }



  public void loadTopImage(ImageView imageView,String url){
    Glide.with(BaseApp.getInstance())
      .load(url)
      .apply(RequestOptionUtil.getInstance().getRequestDfTopOptions())
      .into(imageView);
  }

  public void loadTopIdImage(ImageView imageView,int url){
    Glide.with(BaseApp.getInstance())
      .load(url)
      .apply(RequestOptionUtil.getInstance().getRequestDfTopOptions())
      .into(imageView);
  }

  public void loadBottomImage(ImageView imageView,int url){
    Glide.with(BaseApp.getInstance())
      .load(url)
      .apply(RequestOptionUtil.getInstance().getRequestDfBottomOptions())
      .into(imageView);
  }



  /**
   * 可以配置圆角半径
   * @param imageView
   * @param url
   * @param radio
   */
  public void loadCircleRadioImage(ImageView imageView,String url,int radio){
    Glide.with(BaseApp.getInstance())
      .load(url)
      .apply(RequestOptionUtil.getInstance().getRequestOptions(radio))
      .into(imageView);
  }


}
