package com.smallmitao.appmine.bean;

import java.util.List;

/**
 *
 * @author Jackshao
 * @date 2019/1/3
 */

public class ShopInfoModel {

  /**
   * shop_id : 0
   * brand_name : 小蜜淘商家示范店001
   * head_img :
   * account : 15990019009
   * phone : 15990019009
   * address_detail : 华盛达广场
   * province : 31
   * city : 383
   * area : 3233
   * province_view : 浙江
   * city_view : 杭州
   * area_view : 滨江
   * full_address : 浙江杭州滨江区华盛达广场
   * shop_img : ["http://xmtb.oss-cn-hangzhou.aliyuncs.com/upload/images/20190126/goods/c2a6f5963484782313952310646fddba.jpg?OSSAccessKeyId=LTAIhjHFwn061f0Q&Expires=4702083672&Signature=W8mgVStHGiRjFsS7WYCJmUnOfLI%3D","http://xmtb.oss-cn-hangzhou.aliyuncs.com/upload/images/20190126/goods/419107415744bf3c83cd970a1c68d4b6.png?OSSAccessKeyId=LTAIhjHFwn061f0Q&Expires=4702083740&Signature=ECO7WEaomqvZ8raoagxK9D4NXuc%3D"]
   */

  private int shop_id;
  private String brand_name;
  private String head_img;
  private String account;
  private String phone;
  private String address_detail;
  private String province;
  private String city;
  private String area;
  private String province_view;
  private String city_view;
  private String area_view;
  private String full_address;
  private List<String> shop_img;

  public int getShop_id() {
    return shop_id;
  }

  public void setShop_id(int shop_id) {
    this.shop_id = shop_id;
  }

  public String getBrand_name() {
    return brand_name;
  }

  public void setBrand_name(String brand_name) {
    this.brand_name = brand_name;
  }

  public String getHead_img() {
    return head_img;
  }

  public void setHead_img(String head_img) {
    this.head_img = head_img;
  }

  public String getAccount() {
    return account;
  }

  public void setAccount(String account) {
    this.account = account;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getAddress_detail() {
    return address_detail;
  }

  public void setAddress_detail(String address_detail) {
    this.address_detail = address_detail;
  }

  public String getProvince() {
    return province;
  }

  public void setProvince(String province) {
    this.province = province;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getArea() {
    return area;
  }

  public void setArea(String area) {
    this.area = area;
  }

  public String getProvince_view() {
    return province_view;
  }

  public void setProvince_view(String province_view) {
    this.province_view = province_view;
  }

  public String getCity_view() {
    return city_view;
  }

  public void setCity_view(String city_view) {
    this.city_view = city_view;
  }

  public String getArea_view() {
    return area_view;
  }

  public void setArea_view(String area_view) {
    this.area_view = area_view;
  }

  public String getFull_address() {
    return full_address;
  }

  public void setFull_address(String full_address) {
    this.full_address = full_address;
  }

  public List<String> getShop_img() {
    return shop_img;
  }

  public void setShop_img(List<String> shop_img) {
    this.shop_img = shop_img;
  }
}
