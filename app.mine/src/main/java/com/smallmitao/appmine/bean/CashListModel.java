package com.smallmitao.appmine.bean;

import java.util.List;

/**
 *
 * @author Jackshao
 * @date 2019/1/3
 */

public class CashListModel {

  private List<ListBean> list;

  public List<ListBean> getList() {
    return list;
  }

  public void setList(List<ListBean> list) {
    this.list = list;
  }

  public static class ListBean {
    /**
     * desc : 线上购物：休闲零食组合45克*8杯
     * change_cash : +110
     * created_at : 2018-12-25 20:48:54
     * type : 1
     */

    private String desc;
    private String change_cash;
    private String created_at;
    private int type;

    public String getDesc() {
      return desc;
    }

    public void setDesc(String desc) {
      this.desc = desc;
    }

    public String getChange_cash() {
      return change_cash;
    }

    public void setChange_cash(String change_cash) {
      this.change_cash = change_cash;
    }

    public String getCreated_at() {
      return created_at;
    }

    public void setCreated_at(String created_at) {
      this.created_at = created_at;
    }

    public int getType() {
      return type;
    }

    public void setType(int type) {
      this.type = type;
    }
  }
}
