package com.smallmitao.appmine.bean;

/**
 *
 * @author Jackshao
 * @date 2019/1/3
 */

public class IncomeModel {

  /**
   * total_income : 0
   * drz_income : 0
   * account_remain : 0.00
   */

  private int total_income;
  private int drz_income;
  private String account_remain;
  private String deal_total;

  public int getTotal_income() {
    return total_income;
  }

  public void setTotal_income(int total_income) {
    this.total_income = total_income;
  }

  public int getDrz_income() {
    return drz_income;
  }

  public void setDrz_income(int drz_income) {
    this.drz_income = drz_income;
  }

  public String getAccount_remain() {
    return account_remain;
  }

  public void setAccount_remain(String account_remain) {
    this.account_remain = account_remain;
  }

  public String getDeal_total() {
    return deal_total;
  }

  public void setDeal_total(String deal_total) {
    this.deal_total = deal_total;
  }
}
