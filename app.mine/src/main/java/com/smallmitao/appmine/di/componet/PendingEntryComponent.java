package com.smallmitao.appmine.di.componet;

import android.app.Activity;
import com.smallmitao.appmine.di.module.PendingEntryModule;
import com.smallmitao.appmine.ui.activity.PendingEntryActivity;
import com.smallmitao.libbase.di.component.BaseAppComponent;
import com.smallmitao.libbase.di.scope.ActivityScope;
import dagger.Component;


/**
 * @author Jackshao
 */
@ActivityScope
@Component(dependencies = BaseAppComponent.class, modules = PendingEntryModule.class)
public interface PendingEntryComponent {

    Activity getActivity();

    void inject(PendingEntryActivity pendingEntryActivity);
}
