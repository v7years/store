package com.smallmitao.appmine.di.module;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.smallmitao.libbase.di.scope.ActivityScope;
import com.smallmitao.libbase.di.scope.FragmentScope;

import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;

import dagger.Module;
import dagger.Provides;


/**
 * @author Jackshao
 */
@Module
public class FragmentModule {

    private Fragment mFragment;

    public FragmentModule(Fragment mFragment) {
        this.mFragment = mFragment;
    }

    @FragmentScope
    @Provides
    Activity provideActivity() {
        return mFragment.getActivity();
    }


}
