package com.smallmitao.appmine.di.componet;

import android.app.Activity;

import com.smallmitao.appmine.di.module.PendingEntryFragmentModule;
import com.smallmitao.appmine.di.module.PendingEntryModule;
import com.smallmitao.appmine.ui.activity.PendingEntryActivity;
import com.smallmitao.appmine.ui.fragment.PendingEntryFragment;
import com.smallmitao.libbase.di.component.BaseAppComponent;
import com.smallmitao.libbase.di.scope.ActivityScope;
import com.smallmitao.libbase.di.scope.FragmentScope;

import dagger.Component;


/**
 * @author Jackshao
 */
@FragmentScope
@Component(dependencies = BaseAppComponent.class, modules = PendingEntryFragmentModule.class)
public interface PendingEntryFragmentComponent {

    Activity getActivity();

    void inject(PendingEntryFragment pendingEntryFragment);
}
