package com.smallmitao.appmine.di.module;

import android.app.Activity;

import com.smallmitao.libbase.di.scope.ActivityScope;


import dagger.Module;
import dagger.Provides;


/**
 * @author Jackshao
 */
@Module
public class ActivityModule {

    private Activity mActivity;

    public ActivityModule(Activity mActivity) {
        this.mActivity = mActivity;
    }

    @ActivityScope
    @Provides
    Activity provideActivity() {
        return mActivity;
    }

}
