package com.smallmitao.appmine.di.componet;

import android.app.Activity;

import com.smallmitao.appmine.di.module.ActivityModule;
import com.smallmitao.appmine.ui.activity.MyShopInfoActivity;
import com.smallmitao.appmine.ui.activity.PendingEntryActivity;
import com.smallmitao.libbase.di.component.BaseAppComponent;
import com.smallmitao.libbase.di.scope.ActivityScope;

import dagger.Component;


/**
 * @author Jackshao
 */
@ActivityScope
@Component(dependencies = BaseAppComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    Activity getActivity();

    void inject(MyShopInfoActivity activity);

}
