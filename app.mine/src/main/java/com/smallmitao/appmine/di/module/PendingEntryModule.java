package com.smallmitao.appmine.di.module;

import android.app.Activity;
import com.smallmitao.libbase.di.scope.ActivityScope;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import dagger.Module;
import dagger.Provides;


/**
 * @author Jackshao
 */
@Module
public class PendingEntryModule {

    private Activity mActivity;

    public PendingEntryModule(Activity mActivity) {
        this.mActivity = mActivity;
    }

    @ActivityScope
    @Provides
    Activity provideActivity() {
        return mActivity;
    }

    @ActivityScope
    @Provides
    CommonNavigator provideCommonNavigator(){
        CommonNavigator commonNavigator = new CommonNavigator(mActivity);
        commonNavigator.setScrollPivotX(0.65f);
        commonNavigator.setAdjustMode(true);
        return commonNavigator;
    }


}
