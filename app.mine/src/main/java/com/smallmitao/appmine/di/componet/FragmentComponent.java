package com.smallmitao.appmine.di.componet;

import android.app.Activity;

import com.smallmitao.appmine.di.module.FragmentModule;
import com.smallmitao.appmine.ui.fragment.MineFragment;
import com.smallmitao.libbase.di.component.BaseAppComponent;
import com.smallmitao.libbase.di.scope.FragmentScope;

import dagger.Component;


/**
 * @author Jackshao
 */
@FragmentScope
@Component(dependencies = BaseAppComponent.class, modules = FragmentModule.class)
public interface FragmentComponent {

    Activity getActivity();

    void inject(MineFragment mineFragment);
}
