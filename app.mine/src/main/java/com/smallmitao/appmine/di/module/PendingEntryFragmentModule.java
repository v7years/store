package com.smallmitao.appmine.di.module;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.smallmitao.appmine.ui.adapter.PendingEntryAdapter;
import com.smallmitao.libbase.di.scope.FragmentScope;

import dagger.Module;
import dagger.Provides;


/**
 * @author Jackshao
 */
@Module
public class PendingEntryFragmentModule {

    private Fragment mFragment;

    public PendingEntryFragmentModule(Fragment mFragment) {
        this.mFragment = mFragment;
    }

    @FragmentScope
    @Provides
    Activity provideActivity() {
        return mFragment.getActivity();
    }

    @FragmentScope
    @Provides
    public  RecyclerView.LayoutManager provideLinearLayoutManager(Activity activity){
        return new LinearLayoutManager(activity);
    }

    @FragmentScope
    @Provides
    public PendingEntryAdapter provideDailyExplosionsAdapter(){
        return new PendingEntryAdapter();
    }

}
