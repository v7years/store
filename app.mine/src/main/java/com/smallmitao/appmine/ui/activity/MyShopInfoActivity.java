package com.smallmitao.appmine.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.smallmitao.appmine.R;
import com.smallmitao.appmine.R2;
import com.smallmitao.appmine.bean.ShopInfoModel;
import com.smallmitao.appmine.di.componet.ActivityComponent;
import com.smallmitao.appmine.di.componet.DaggerActivityComponent;
import com.smallmitao.appmine.di.module.ActivityModule;
import com.smallmitao.appmine.mvp.MyShopPresenter;
import com.smallmitao.appmine.mvp.contract.MyShopContract;
import com.smallmitao.appmine.ui.adapter.ShopInfoAdapter;
import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.mvp.BaseMvpActivity;
import com.smallmitao.libbase.util.SpaceItemDecoration;
import com.smallmitao.libbase.util.Toastor;
import com.smallmitao.libbridge.router.BridgeRouter;

import net.lucode.hackware.magicindicator.buildins.UIUtil;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author Jackshao
 * @date 2019/1/2
 */

@Route(path = BridgeRouter.PAGE_ACTIVITY_MY_SHOP_INFO, name = "我的店铺详情")
public class MyShopInfoActivity extends BaseMvpActivity<MyShopPresenter> implements MyShopContract.View {

  @BindView(R2.id.title_tv)
  TextView mTitleTv;
  @BindView(R2.id.toolbar)
  Toolbar mToolbar;
  @BindView(R2.id.shop_et)
  EditText shopEt;
  @BindView(R2.id.account_tv)
  TextView accountTv;
  @BindView(R2.id.ssl_et)
  EditText sslEt;
  @BindView(R2.id.address_et)
  EditText addressEt;
  @BindView(R2.id.phone_et)
  EditText phoneEt;
  @BindView(R2.id.back_btn)
  ImageView backBtn;
  @BindView(R2.id.recycle_view)
  RecyclerView recycleView;

  private ShopInfoAdapter mShopInfoAdapter;

  @Override
  protected void initImmersionBar() {
    super.initImmersionBar();
    mImmersionBar.titleBar(mToolbar).init();
  }

  @Override
  protected int getLayout(@Nullable Bundle savedInstanceState) {
    return R.layout.activity_my_shopinfo;
  }

  @Override
  protected void initData(@Nullable Bundle savedInstanceState) {
    mTitleTv.setText("店铺中心");
    backBtn.setVisibility(View.VISIBLE);
    recycleView.addItemDecoration(new SpaceItemDecoration(2, UIUtil.dip2px(this,10)));
    recycleView.setLayoutManager(new GridLayoutManager(this,2));
    mShopInfoAdapter = new ShopInfoAdapter();
    recycleView.setAdapter(mShopInfoAdapter);
    mPresenter.requestMineInfo();
  }


  @Override
  protected void initInject() {
    getFragmentComponent().inject(this);
  }

  protected ActivityComponent getFragmentComponent() {
    return DaggerActivityComponent.builder()
      .baseAppComponent(BaseApp.getInstance().getBaseAppComponent())
      .activityModule(new ActivityModule(this))
      .build();
  }


  @Override
  public void getMineInfoModel(ShopInfoModel model) {
    shopEt.setText(model.getBrand_name());
    accountTv.setText(model.getAccount());
    sslEt.setText(model.getProvince() + model.getCity() + model.getArea());
    addressEt.setText(model.getAddress_detail());
    phoneEt.setText(model.getPhone());
    mShopInfoAdapter.replaceData(model.getShop_img());
  }

  @OnClick({R2.id.back_btn,R2.id.commit_btn})
  public void onClick(View view) {
    if (view.getId() == R.id.commit_btn){
      Toastor.showToast("保存成功");
      finish();
    }else if (view.getId() == R.id.back_btn){
      finish();
    }
  }

}
