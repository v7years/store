package com.smallmitao.appmine.ui.fragment;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.smallmitao.appmine.R;
import com.smallmitao.appmine.R2;
import com.smallmitao.appmine.bean.CashListModel;
import com.smallmitao.appmine.bean.PendingEntryModel;
import com.smallmitao.appmine.di.componet.DaggerPendingEntryFragmentComponent;
import com.smallmitao.appmine.di.componet.PendingEntryFragmentComponent;
import com.smallmitao.appmine.di.module.PendingEntryFragmentModule;
import com.smallmitao.appmine.mvp.PendingEntryPresenter;
import com.smallmitao.appmine.mvp.contract.PendingEntryContract;
import com.smallmitao.appmine.ui.adapter.PendingEntryAdapter;
import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.mvp.BaseMvpFragment;
import com.smallmitao.libbridge.router.BridgeRouter;
import java.util.List;
import javax.inject.Inject;
import butterknife.BindView;

/**
 * @author Jackshao
 * @date 2019/1/2
 */

@Route(path = BridgeRouter.PAGE_FRAGMENT_PENDING_ENTRY, name = "待入账明细或者账单明细")
public class PendingEntryFragment extends BaseMvpFragment<PendingEntryPresenter> implements PendingEntryContract.View,OnRefreshListener {

  @BindView(R2.id.recycle_view)
  RecyclerView mRecycleView;
  @BindView(R2.id.refresh_layout)
  SmartRefreshLayout mRefreshLayout;

  @Inject
  RecyclerView.LayoutManager mLayoutManager;

  @Inject
  PendingEntryAdapter mPendingEntryAdapter;

  @Autowired
  int mPendingEntryType;


  @Override
  protected int getLayout() {
    return R.layout.fragment_pending_entry;
  }

  @Override
  protected void initData() {
    ARouter.getInstance().inject(this);
    mRefreshLayout.setOnRefreshListener(this);
    mRecycleView.setLayoutManager(mLayoutManager);
    mRecycleView.setAdapter(mPendingEntryAdapter);
    mPresenter.requestPendingEntry(mPendingEntryType);
  }

  @Override
  protected void initInject() {
    getFragmentComponent().inject(this);
  }

  protected PendingEntryFragmentComponent getFragmentComponent() {
    return DaggerPendingEntryFragmentComponent.builder()
      .baseAppComponent(BaseApp.getInstance().getBaseAppComponent())
      .pendingEntryFragmentModule(new PendingEntryFragmentModule(this))
      .build();
  }


  @Override
  public void getPendingEntryModel(boolean isOk, List<CashListModel.ListBean> mList) {
    mRefreshLayout.finishRefresh(isOk);
    if (isOk){
      mPendingEntryAdapter.replaceData(mList);
    }
  }

  @Override
  public void onRefresh(@NonNull RefreshLayout refreshLayout) {
    mPresenter.requestPendingEntry(mPendingEntryType);
  }
}
