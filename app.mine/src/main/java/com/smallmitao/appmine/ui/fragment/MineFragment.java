package com.smallmitao.appmine.ui.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.smallmitao.appmine.R;
import com.smallmitao.appmine.R2;
import com.smallmitao.appmine.bean.IncomeModel;
import com.smallmitao.appmine.bean.ShopInfoModel;
import com.smallmitao.appmine.di.componet.DaggerFragmentComponent;
import com.smallmitao.appmine.di.componet.FragmentComponent;
import com.smallmitao.appmine.di.module.FragmentModule;
import com.smallmitao.appmine.mvp.MineInfoPresenter;
import com.smallmitao.appmine.mvp.contract.MineInfoContract;
import com.smallmitao.appmine.ui.adapter.ShopInfoAdapter;
import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.mvp.BaseMvpFragment;
import com.smallmitao.libbase.util.SpaceItemDecoration;
import com.smallmitao.libbase.util.Toastor;
import com.smallmitao.libbridge.router.BridgeRouter;

import net.lucode.hackware.magicindicator.buildins.UIUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Jackshao
 * @date 2019/1/2
 */

@Route(path = BridgeRouter.PAGE_FRAGMENT_MINE, name = "我的")
public class MineFragment extends BaseMvpFragment<MineInfoPresenter> implements MineInfoContract.View {

  @BindView(R2.id.title_tv)
  TextView mTitleTv;
  @BindView(R2.id.toolbar)
  Toolbar mToolbar;
  @BindView(R2.id.total_price_tv)
  TextView totalPriceTv;
  @BindView(R2.id.ye_price_tv)
  TextView yePriceTv;
  @BindView(R2.id.drz_price_tv)
  TextView drzPriceTv;
  @BindView(R2.id.name_tv)
  TextView nameTv;
  @BindView(R2.id.phone_tv)
  TextView phoneTv;
  @BindView(R2.id.cj_count_tv)
  TextView cjCountTv;
  @BindView(R2.id.address_tv)
  TextView address_tv;
  @BindView(R2.id.recycle_view)
  RecyclerView recycleView;

  private ShopInfoAdapter mShopInfoAdapter;


  @Override
  protected int getLayout() {
    return R.layout.fragment_mine;
  }

  @Override
  protected void initInject() {
    getFragmentComponent().inject(this);
  }

  protected FragmentComponent getFragmentComponent() {
    return DaggerFragmentComponent.builder()
      .baseAppComponent(BaseApp.getInstance().getBaseAppComponent())
      .fragmentModule(new FragmentModule(this))
      .build();
  }


  @Override
  protected void initImmersionBar() {
    super.initImmersionBar();
    mImmersionBar.titleBar(mToolbar).init();
  }

  @Override
  protected void initData() {
    mTitleTv.setText("我的");
    recycleView.addItemDecoration(new SpaceItemDecoration(2, UIUtil.dip2px(getActivity(),10)));
    recycleView.setLayoutManager(new GridLayoutManager(getActivity(),2));
    mShopInfoAdapter = new ShopInfoAdapter();
    recycleView.setAdapter(mShopInfoAdapter);
    mPresenter.requestIncome();
    mPresenter.requestMineInfo();
  }


  @OnClick({R2.id.pending_entry_info, R2.id.my_shop_lay,
    R2.id.dd_tv1, R2.id.dd_tv2, R2.id.dd_tv3, R2.id.dd_tv4, R2.id.dd_tv5
    , R2.id.shop_tv1, R2.id.shop_tv2, R2.id.shop_tv3, R2.id.shop_tv4})
  public void onClick(View view) {
    if (view.getId() == R.id.pending_entry_info) {
      ARouter.getInstance().build(Uri.parse(BridgeRouter.PAGE_ACTIVITY_PENDING_ENTRY)).navigation();
    } else if (view.getId() == R.id.my_shop_lay) {
      ARouter.getInstance().build(Uri.parse(BridgeRouter.PAGE_ACTIVITY_MY_SHOP_INFO)).navigation();
    } else {
      Toastor.showToast("订单状态暂不可用，请移步至PC端查看。");
    }
  }


  @Override
  public void getMineInfoModel(ShopInfoModel model) {
    nameTv.setText(model.getBrand_name());
    phoneTv.setText(model.getAccount());
    address_tv.setText(model.getAddress_detail());
    mShopInfoAdapter.replaceData(model.getShop_img());
  }

  @Override
  public void getIncomeModel(IncomeModel model) {
    totalPriceTv.setText("￥" + model.getTotal_income());
    yePriceTv.setText("￥" + model.getAccount_remain());
    drzPriceTv.setText("￥" + model.getDrz_income());
    cjCountTv.setText(model.getDeal_total() + "");
  }

}
