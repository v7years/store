package com.smallmitao.appmine.ui.adapter;


import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.smallmitao.appmine.R;
import com.smallmitao.appmine.bean.CashListModel;

/**
 * @author Jackshao
 * @date 2018/9/18
 */

public class PendingEntryAdapter extends BaseQuickAdapter<CashListModel.ListBean,BaseViewHolder> {


  public PendingEntryAdapter() {
    super(R.layout.item_pending_entry);
  }

  @Override
  protected void convert(BaseViewHolder helper, CashListModel.ListBean item) {
    helper.setText(R.id.title_tv,item.getDesc());
    helper.setText(R.id.price_tv,item.getChange_cash());
    helper.setText(R.id.time_tv,item.getCreated_at());
  }



}
