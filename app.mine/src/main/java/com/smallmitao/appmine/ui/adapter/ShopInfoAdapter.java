package com.smallmitao.appmine.ui.adapter;


import android.widget.ImageView;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.smallmitao.appmine.R;
import com.smallmitao.libbase.util.GlideUtils;

/**
 * @author Jackshao
 * @date 2018/9/18
 */

public class ShopInfoAdapter extends BaseQuickAdapter<String,BaseViewHolder> {


  public ShopInfoAdapter() {
    super(R.layout.item_img);
  }

  @Override
  protected void convert(BaseViewHolder helper, String item) {
    GlideUtils.getInstance().loadNormalImage((ImageView) helper.getView(R.id.img_iv),item);
  }



}
