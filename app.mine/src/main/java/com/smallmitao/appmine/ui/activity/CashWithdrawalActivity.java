package com.smallmitao.appmine.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.smallmitao.appmine.R;
import com.smallmitao.appmine.R2;
import com.smallmitao.libbase.base.BaseActivity;
import com.smallmitao.libbase.util.Toastor;
import com.smallmitao.libbridge.router.BridgeRouter;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author Jackshao
 * @date 2019/1/3
 */

@Route(path = BridgeRouter.PAGE_ACTIVITY_CASH_WITHDRAWAL, name = "去提现")
public class CashWithdrawalActivity extends BaseActivity {

  @BindView(R2.id.title_tv)
  TextView mTitleTv;
  @BindView(R2.id.toolbar)
  Toolbar mToolbar;
  @BindView(R2.id.back_btn)
  ImageView backBtn;


  @Override
  protected int getLayout(@Nullable Bundle savedInstanceState) {
    return R.layout.activity_cash_withdrawal;
  }

  @Override
  protected void initImmersionBar() {
    super.initImmersionBar();
    mImmersionBar.titleBar(mToolbar).init();
  }

  @Override
  protected void initData(@Nullable Bundle savedInstanceState) {
    mTitleTv.setText("提现");
    backBtn.setVisibility(View.VISIBLE);
  }



  @OnClick({R2.id.back_btn,R2.id.commit_btn})
  public void onClick(View view) {
    if (view.getId() == R.id.back_btn){
      finish();
    } else if (view.getId() == R.id.commit_btn){
      Toastor.showToast("提现仅供体验流程，请先申请注册商家资质，敬请谅解。");
    }
  }
}
