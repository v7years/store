package com.smallmitao.appmine.ui.activity;

import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.smallmitao.appmine.R;
import com.smallmitao.appmine.R2;
import com.smallmitao.appmine.bean.IncomeModel;
import com.smallmitao.appmine.di.componet.ActivityComponent;
import com.smallmitao.appmine.di.componet.DaggerActivityComponent;
import com.smallmitao.appmine.di.componet.DaggerPendingEntryComponent;
import com.smallmitao.appmine.di.componet.PendingEntryComponent;
import com.smallmitao.appmine.di.module.ActivityModule;
import com.smallmitao.appmine.di.module.PendingEntryModule;
import com.smallmitao.appmine.mvp.PendingInfoPresenter;
import com.smallmitao.appmine.mvp.contract.PendingInfoContract;
import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.base.BaseFragmentStatePagerAdapter;
import com.smallmitao.libbase.mvp.BaseMvpActivity;
import com.smallmitao.libbase.util.IndicatorAdapter;
import com.smallmitao.libbridge.router.BridgeRouter;
import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.UIUtil;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author Jackshao
 * @date 2019/1/3
 */

@Route(path = BridgeRouter.PAGE_ACTIVITY_PENDING_ENTRY, name = "待入账")
public class PendingEntryActivity extends BaseMvpActivity<PendingInfoPresenter> implements PendingInfoContract.View, ViewPager.OnPageChangeListener {

  @BindView(R2.id.title_tv)
  TextView mTitleTv;
  @BindView(R2.id.toolbar)
  Toolbar mToolbar;
  @BindView(R2.id.magic_indicator)
  MagicIndicator mMagicIndicator;
  @BindView(R2.id.view_pager)
  ViewPager mViewPager;
  @BindView(R2.id.back_btn)
  ImageView backBtn;
  @BindView(R2.id.total_price_tv)
  TextView totalPriceTv;
  @BindView(R2.id.zh_ye_tv)
  TextView zhYeTv;
  @BindView(R2.id.drz_tv)
  TextView drzTv;

  @Inject
  CommonNavigator mCommonNavigator;

  private static final String[] FIND_TITLE = new String[]{"账单明细", "待入账明细"};


  private List<String> mDataList = Arrays.asList(FIND_TITLE);
  private List<Fragment> mFragmentList = new ArrayList<>();

  @Override
  protected int getLayout(@Nullable Bundle savedInstanceState) {
    return R.layout.activity_pending_entry;
  }

  @Override
  protected void initImmersionBar() {
    super.initImmersionBar();
    mImmersionBar.titleBar(mToolbar).init();
  }


  @Override
  protected void initInject() {
    getFragmentComponent().inject(this);
  }

  protected PendingEntryComponent getFragmentComponent() {
    return DaggerPendingEntryComponent.builder()
      .baseAppComponent(BaseApp.getInstance().getBaseAppComponent())
      .pendingEntryModule(new PendingEntryModule(this))
      .build();
  }

  @Override
  protected void initData(@Nullable Bundle savedInstanceState) {
    mTitleTv.setText("待入账明细");
    backBtn.setVisibility(View.VISIBLE);
    mPresenter.requestIncome();
    mFragmentList.add((Fragment) ARouter.getInstance().build(BridgeRouter.PAGE_FRAGMENT_PENDING_ENTRY).withInt("mPendingEntryType", 1).navigation());
    mFragmentList.add((Fragment) ARouter.getInstance().build(BridgeRouter.PAGE_FRAGMENT_PENDING_ENTRY).withInt("mPendingEntryType", 2).navigation());
    mViewPager.setAdapter(new BaseFragmentStatePagerAdapter(getSupportFragmentManager(), mFragmentList));
    mViewPager.setCurrentItem(0);
    mViewPager.setOffscreenPageLimit(2);
    mViewPager.addOnPageChangeListener(this);
    initMagicIndicator();
  }


  /**
   * 初始化标题栏
   */
  private void initMagicIndicator() {
    mMagicIndicator.setBackgroundColor(this.getResources().getColor(R.color.white));
    mCommonNavigator.setAdapter(new IndicatorAdapter(mDataList, mViewPager));
    mMagicIndicator.setNavigator(mCommonNavigator);
    // must after setNavigator
    LinearLayout titleContainer = mCommonNavigator.getTitleContainer();
    //设置tab的显示模式，SHOW_DIVIDER_MIDDLE为居中
    titleContainer.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
    titleContainer.setDividerDrawable(new ColorDrawable() {
      @Override
      public int getIntrinsicWidth() {
        //tab的宽度
        return UIUtil.dip2px(PendingEntryActivity.this, 15);
      }
    });
    ViewPagerHelper.bind(mMagicIndicator, mViewPager);
  }


  @Override
  public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

  }

  @Override
  public void onPageSelected(int position) {
    mTitleTv.setText(position == 0 ? "待入账明细" : "待入账明细");
  }

  @Override
  public void onPageScrollStateChanged(int state) {

  }

  @OnClick({R2.id.back_btn, R2.id.tx_tv})
  public void onClick(View view) {
    if (view.getId() == R.id.back_btn) {
      finish();
    } else if (view.getId() == R.id.tx_tv) {
      ARouter.getInstance().build(Uri.parse(BridgeRouter.PAGE_ACTIVITY_CASH_WITHDRAWAL)).navigation();
    }
  }

  @Override
  public void getIncomeModel(IncomeModel model) {
    totalPriceTv.setText("￥" + model.getTotal_income());
    zhYeTv.setText("￥" + model.getAccount_remain());
    drzTv.setText("￥" + model.getDrz_income());
  }

}
