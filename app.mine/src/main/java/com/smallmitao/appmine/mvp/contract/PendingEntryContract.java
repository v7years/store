package com.smallmitao.appmine.mvp.contract;


import com.smallmitao.appmine.bean.CashListModel;
import com.smallmitao.appmine.bean.IncomeModel;
import com.smallmitao.appmine.bean.PendingEntryModel;
import com.smallmitao.libbase.mvp.BasePresenter;
import com.smallmitao.libbase.mvp.BaseView;

import java.util.List;

/**
 *
 * @author Jackshao
 */
public interface PendingEntryContract {

    interface View extends BaseView {
        void getPendingEntryModel(boolean isOk, List<CashListModel.ListBean> mList);
    }

    interface Presenter extends BasePresenter<View> {
        /**
         * 请求
         * @param mType
         */
        void requestPendingEntry(int mType);
    }
}
