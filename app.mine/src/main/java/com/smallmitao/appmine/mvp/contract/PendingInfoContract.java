package com.smallmitao.appmine.mvp.contract;

import com.smallmitao.appmine.bean.IncomeModel;
import com.smallmitao.appmine.bean.ShopInfoModel;
import com.smallmitao.libbase.mvp.BasePresenter;
import com.smallmitao.libbase.mvp.BaseView;

/**
 *
 * @author Jackshao
 */
public interface PendingInfoContract {

    interface View extends BaseView {
        void getIncomeModel(IncomeModel model);
    }

    interface Presenter extends BasePresenter<View> {

        void requestIncome();
    }
}
