package com.smallmitao.appmine.mvp.contract;

import com.smallmitao.appmine.bean.ShopInfoModel;
import com.smallmitao.libbase.mvp.BasePresenter;
import com.smallmitao.libbase.mvp.BaseView;

/**
 *
 * @author Jackshao
 */
public interface MyShopContract {

    interface View extends BaseView {
        void getMineInfoModel(ShopInfoModel model);
    }

    interface Presenter extends BasePresenter<View> {
        /**
         * 请求
         */
        void requestMineInfo();
    }
}
