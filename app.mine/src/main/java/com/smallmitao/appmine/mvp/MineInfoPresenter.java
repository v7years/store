package com.smallmitao.appmine.mvp;

import com.smallmitao.appmine.bean.IncomeModel;
import com.smallmitao.appmine.bean.ShopInfoModel;
import com.smallmitao.appmine.mvp.contract.MineInfoContract;
import com.smallmitao.libbase.di.scope.FragmentScope;
import com.smallmitao.libbase.http.RetrofitHelper;
import com.smallmitao.libbase.http.appinterface.HttpInter;
import com.smallmitao.libbase.manager.app.BaseUserManager;
import com.smallmitao.libbase.mvp.RxPresenter;
import com.smallmitao.libbase.util.Toastor;
import com.zhouyou.http.callback.SimpleCallBack;
import com.zhouyou.http.exception.ApiException;
import javax.inject.Inject;

/**
 * date: 2018/8/10
 * @author Jackshao
 *
 */

@FragmentScope
public class MineInfoPresenter extends RxPresenter<MineInfoContract.View> implements MineInfoContract.Presenter {

  private RetrofitHelper mHelper;

  @Inject
  public MineInfoPresenter(RetrofitHelper retrofitHelper) {
    this.mHelper = retrofitHelper;
  }

  @Override
  public void requestMineInfo() {
    addDisposable(mHelper
      .getRequest(HttpInter.GetShopInfo.PATH)
      .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getToken())
      .execute(new SimpleCallBack<ShopInfoModel>() {

        @Override
        public void onError(ApiException e) {
          Toastor.showToast(e.getMessage());
        }

        @Override
        public void onSuccess(ShopInfoModel o) {
          getView().getMineInfoModel(o);
        }
      }));
  }

  @Override
  public void requestIncome() {
    addDisposable(mHelper
      .getRequest(HttpInter.GetIncome.PATH)
      .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getToken())
      .execute(new SimpleCallBack<IncomeModel>() {

        @Override
        public void onError(ApiException e) {
          Toastor.showToast(e.getMessage());
        }

        @Override
        public void onSuccess(IncomeModel o) {
          getView().getIncomeModel(o);
        }
      }));
  }
}
