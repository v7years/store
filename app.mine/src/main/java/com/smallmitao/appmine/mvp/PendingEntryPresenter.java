package com.smallmitao.appmine.mvp;

import com.smallmitao.appmine.bean.CashListModel;
import com.smallmitao.appmine.mvp.contract.PendingEntryContract;
import com.smallmitao.libbase.di.scope.FragmentScope;
import com.smallmitao.libbase.http.RetrofitHelper;
import com.smallmitao.libbase.http.appinterface.HttpInter;
import com.smallmitao.libbase.manager.app.BaseUserManager;
import com.smallmitao.libbase.mvp.RxPresenter;
import com.smallmitao.libbase.util.Toastor;
import com.zhouyou.http.callback.SimpleCallBack;
import com.zhouyou.http.exception.ApiException;
import javax.inject.Inject;

/**
 * date: 2018/8/10
 * @author Jackshao
 *
 */

@FragmentScope
public class PendingEntryPresenter extends RxPresenter<PendingEntryContract.View> implements PendingEntryContract.Presenter {

  private RetrofitHelper mHelper;

  @Inject
  public PendingEntryPresenter( RetrofitHelper retrofitHelper) {
    this.mHelper = retrofitHelper;
  }

  @Override
  public void requestPendingEntry(int mType) {
    String path = "";
    if (mType==1){
      path = HttpInter.CashList.PATH;
    }else {
      path = HttpInter.CashDrzList.PATH;
    }
    addDisposable(mHelper
      .getRequest(path)
      .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getToken())
      .execute(new SimpleCallBack<CashListModel>() {

        @Override
        public void onError(ApiException e) {
          Toastor.showToast(e.getMessage());
        }

        @Override
        public void onSuccess(CashListModel o) {
          getView().getPendingEntryModel(true,o.getList());
        }
      }));

  }

}
