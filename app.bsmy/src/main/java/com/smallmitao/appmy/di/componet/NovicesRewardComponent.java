package com.smallmitao.appmy.di.componet;

import android.app.Activity;

import com.smallmitao.appmy.di.module.NovicesRewardModule;
import com.smallmitao.appmy.ui.activity.NovicesRewardActivity;
import com.smallmitao.libbase.di.component.BaseAppComponent;
import com.smallmitao.libbase.di.scope.ActivityScope;

import dagger.Component;

@ActivityScope
@Component(dependencies = BaseAppComponent.class, modules = NovicesRewardModule.class)
public interface NovicesRewardComponent {

    Activity getActivity();

    void inject(NovicesRewardActivity activity);
}
