package com.smallmitao.appmy.di.module;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.smallmitao.libbase.di.scope.FragmentScope;

import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;

import dagger.Module;
import dagger.Provides;


/**
 * @author Jackshao
 */
@Module
public class MyFragmentModule {

    private Fragment fragment;

    public MyFragmentModule(Fragment fragment) {
        this.fragment = fragment;
    }

    @FragmentScope
    @Provides
    Activity provideActivity() {
        return fragment.getActivity();
    }


    @FragmentScope
    @Provides
    CommonNavigator provideCommonNavigator(){
        CommonNavigator commonNavigator = new CommonNavigator(fragment.getActivity());
        commonNavigator.setScrollPivotX(0.65f);
        commonNavigator.setAdjustMode(true);
        return commonNavigator;
    }


}
