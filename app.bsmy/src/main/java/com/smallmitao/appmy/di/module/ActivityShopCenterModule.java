package com.smallmitao.appmy.di.module;

import android.app.Activity;

import com.smallmitao.libbase.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityShopCenterModule {
    private Activity mActivity;

    public ActivityShopCenterModule(Activity activity) {
        this.mActivity = activity;
    }


    @Provides
    @ActivityScope
    public Activity ProviderActivity() {
        return mActivity;
    }
}
