package com.smallmitao.appmy.di.componet;

import android.app.Activity;

import com.smallmitao.appmy.di.module.ActivityShopCenterModule;
import com.smallmitao.appmy.ui.activity.ShopCenterActivity;
import com.smallmitao.libbase.di.component.BaseAppComponent;
import com.smallmitao.libbase.di.scope.ActivityScope;

import dagger.Component;

@ActivityScope
@Component(dependencies = BaseAppComponent.class, modules = ActivityShopCenterModule.class)
public interface ActivityShopCenterComponent {
    Activity getActivity();

    void inject(ShopCenterActivity activity);
}
