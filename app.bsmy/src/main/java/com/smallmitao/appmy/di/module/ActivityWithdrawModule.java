package com.smallmitao.appmy.di.module;

import android.app.Activity;

import com.smallmitao.libbase.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityWithdrawModule {
    private Activity mActivity;

    public ActivityWithdrawModule(Activity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityScope
    public Activity getActivity() {
        return mActivity;
    }
}
