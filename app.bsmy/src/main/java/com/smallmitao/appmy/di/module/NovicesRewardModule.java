package com.smallmitao.appmy.di.module;

import android.app.Activity;

import com.smallmitao.libbase.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

@Module
public class NovicesRewardModule {
    private Activity mActivity;

    public NovicesRewardModule(Activity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityScope
    public Activity getActivity() {
        return mActivity;
    }
}
