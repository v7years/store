package com.smallmitao.appmy.di.componet;

import android.app.Activity;

import com.smallmitao.appmy.di.module.MyFragmentModule;
import com.smallmitao.appmy.ui.fragment.StoreMyFragment;
import com.smallmitao.libbase.di.component.BaseAppComponent;
import com.smallmitao.libbase.di.scope.FragmentScope;

import dagger.Component;


/**
 * @author Jackshao
 */
@FragmentScope
@Component(dependencies = BaseAppComponent.class, modules = MyFragmentModule.class)
public interface MyFragmentComponent {

    Activity getActivity();

    void inject(StoreMyFragment businessMyFragment);
}
