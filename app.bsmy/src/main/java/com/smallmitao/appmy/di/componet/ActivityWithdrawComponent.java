package com.smallmitao.appmy.di.componet;

import android.app.Activity;

import com.smallmitao.appmy.di.module.ActivityWithdrawModule;
import com.smallmitao.appmy.ui.activity.StoreWithdrawActivity;
import com.smallmitao.libbase.di.component.BaseAppComponent;
import com.smallmitao.libbase.di.scope.ActivityScope;

import dagger.Component;

@ActivityScope
@Component(dependencies = BaseAppComponent.class, modules = ActivityWithdrawModule.class)
public interface ActivityWithdrawComponent {
    Activity getActivity();

    void inject(StoreWithdrawActivity bsWithdrawActivity);
}
