package com.smallmitao.appmy.di.componet;

import android.app.Activity;

import com.smallmitao.appmy.di.module.ActivityWithdrawRecordModule;
import com.smallmitao.appmy.ui.activity.WithdrawRecordActivity;
import com.smallmitao.libbase.di.component.BaseAppComponent;
import com.smallmitao.libbase.di.scope.ActivityScope;

import dagger.Component;

@ActivityScope
@Component(dependencies = BaseAppComponent.class, modules = ActivityWithdrawRecordModule.class)
public interface ActivityWithdrawRecordComponent {
    Activity getActivity();

    void inject(WithdrawRecordActivity withdrawRecordActivity);
}
