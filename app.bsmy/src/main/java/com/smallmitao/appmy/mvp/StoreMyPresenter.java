package com.smallmitao.appmy.mvp;

import android.app.Activity;

import com.smallmitao.appmy.bean.StoreIncomeBean;
import com.smallmitao.appmy.bean.StoreShopBean;
import com.smallmitao.appmy.mvp.contract.StoreMyContract;
import com.smallmitao.libbase.di.scope.FragmentScope;
import com.smallmitao.libbase.http.RetrofitHelper;
import com.smallmitao.libbase.http.appinterface.HttpInter;
import com.smallmitao.libbase.manager.ShopStatusManager;
import com.smallmitao.libbase.manager.app.BaseUserManager;
import com.smallmitao.libbase.mvp.RxPresenter;
import com.smallmitao.libbridge.router.bean.ShopStatusBean;
import com.zhouyou.http.callback.SimpleCallBack;
import com.zhouyou.http.exception.ApiException;

import javax.inject.Inject;

/**
 * date: 2018/8/10
 *
 * @author Jackshao
 * 首页
 */

@FragmentScope
public class StoreMyPresenter extends RxPresenter<StoreMyContract.View> implements StoreMyContract.Presenter {

    private RetrofitHelper mHelper;


    @Inject
    public StoreMyPresenter(Activity mActivity, RetrofitHelper retrofitHelper) {
        this.mHelper = retrofitHelper;
    }

    @Override
    public void requestInfo() {
        addDisposable(mHelper
                .getRequest(HttpInter.StoreShopInfo.PATH)
                .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getStoreToken())
                .execute(new SimpleCallBack<StoreShopBean>() {

                    @Override
                    public void onError(ApiException e) {
                    }

                    @Override
                    public void onSuccess(StoreShopBean storeShopBean) {
                        getView().getStoreShopInfo(storeShopBean);
                    }
                }));
    }

    @Override
    public void requestIncome() {
        addDisposable(mHelper
                .getRequest(HttpInter.StoreIncome.PATH)
                .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getStoreToken())
                .execute(new SimpleCallBack<StoreIncomeBean>() {

                    @Override
                    public void onError(ApiException e) {
                    }

                    @Override
                    public void onSuccess(StoreIncomeBean incomeBean) {
                        getView().getStoreIncomeInfo(incomeBean);
                    }
                }));
    }
}
