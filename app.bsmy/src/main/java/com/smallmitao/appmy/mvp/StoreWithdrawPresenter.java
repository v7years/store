package com.smallmitao.appmy.mvp;

import android.net.Uri;

import com.alibaba.android.arouter.launcher.ARouter;
import com.smallmitao.appmy.mvp.contract.StoreWithdrawContract;
import com.smallmitao.libbase.http.RetrofitHelper;
import com.smallmitao.libbase.http.api.BaseApiResult;
import com.smallmitao.libbase.http.appinterface.HttpInter;
import com.smallmitao.libbase.manager.app.BaseUserManager;
import com.smallmitao.libbase.mvp.RxPresenter;
import com.smallmitao.libbase.util.Toastor;
import com.smallmitao.libbridge.router.BridgeRouter;
import com.zhouyou.http.callback.SimpleCallBack;
import com.zhouyou.http.exception.ApiException;

import javax.inject.Inject;

public class StoreWithdrawPresenter extends RxPresenter<StoreWithdrawContract.View> implements StoreWithdrawContract.Presenter {
    private RetrofitHelper mHelper;

    @Inject
    public StoreWithdrawPresenter(RetrofitHelper retrofitHelper) {
        this.mHelper = retrofitHelper;
    }

    @Override
    public void submitWithdraw(String sum, String account, String name) {
        addDisposable(mHelper
                .postRequest(HttpInter.StoreAliPayOut.PATH)
                .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getStoreToken())
                .params(HttpInter.StoreAliPayOut.PAY_TYPE, "1")//【9、支付宝；17、微信；’20、余额】
                .params(HttpInter.StoreAliPayOut.PAY_PROVIDER, "9")//【1、原生；5、h5】
                .params(HttpInter.StoreAliPayOut.ACCOUNT, account)
                .params(HttpInter.StoreAliPayOut.NAME, name)
                .params(HttpInter.StoreAliPayOut.AMOUNT, sum)
                .execute(new SimpleCallBack<BaseApiResult>() {
                    @Override
                    public void onError(ApiException e) {
                        Toastor.showToast(e.getMessage());
                    }

                    @Override
                    public void onSuccess(BaseApiResult baseApiResult) {
                        if (baseApiResult.isOk()) {
                            ARouter.getInstance()
                                    .build(Uri.parse(BridgeRouter.STORE_ACTIVITY_WITHDRAWAL_RESULT))
                                    .withBoolean("isSuccess", true)
                                    .withString("message", baseApiResult.getMsg())
                                    .navigation();
                        } else {
                            ARouter.getInstance()
                                    .build(Uri.parse(BridgeRouter.STORE_ACTIVITY_WITHDRAWAL_RESULT))
                                    .withBoolean("isSuccess", false)
                                    .withString("message", baseApiResult.getMsg())
                                    .navigation();
                        }
                    }
                }));
    }
}
