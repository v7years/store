package com.smallmitao.appmy.mvp;

import android.net.Uri;

import com.alibaba.android.arouter.launcher.ARouter;
import com.smallmitao.appmy.mvp.contract.NovicesRewardContract;
import com.smallmitao.appmy.mvp.contract.StoreWithdrawContract;
import com.smallmitao.libbase.http.RetrofitHelper;
import com.smallmitao.libbase.http.api.BaseApiResult;
import com.smallmitao.libbase.http.appinterface.HttpInter;
import com.smallmitao.libbase.manager.app.BaseUserManager;
import com.smallmitao.libbase.mvp.RxPresenter;
import com.smallmitao.libbase.util.Toastor;
import com.smallmitao.libbridge.router.BridgeRouter;
import com.zhouyou.http.callback.SimpleCallBack;
import com.zhouyou.http.exception.ApiException;

import javax.inject.Inject;

public class NoviceRewardPresenter extends RxPresenter<NovicesRewardContract.View> implements NovicesRewardContract.Presenter {
    private RetrofitHelper mHelper;

    @Inject
    public NoviceRewardPresenter(RetrofitHelper retrofitHelper) {
        this.mHelper = retrofitHelper;
    }

    @Override
    public void NovicesReward(String new_discount) {
        addDisposable(mHelper
                .postRequest(HttpInter.DiscountUpdate.PATH)
                .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getStoreToken())
                .params(HttpInter.DiscountUpdate.DISCOUNT, new_discount)
                .execute(new SimpleCallBack<BaseApiResult>() {
                    @Override
                    public void onError(ApiException e) {
                        Toastor.showToast(e.getMessage());
                    }

                    @Override
                    public void onSuccess(BaseApiResult baseApiResult) {
                        Toastor.showToast("修改成功");
                    }
                }));
    }
}
