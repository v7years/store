package com.smallmitao.appmy.mvp.contract;

import com.smallmitao.appmy.bean.WithDrawRecordBean;
import com.smallmitao.libbase.mvp.BasePresenter;
import com.smallmitao.libbase.mvp.BaseView;

public interface WithdrawRecordContract {
    interface View extends BaseView {
        void setRecord(boolean isOk, WithDrawRecordBean bean);

        void error(String error);
    }

    interface Presenter extends BasePresenter<View> {
        void getRecord(int page,int size);

    }
}
