package com.smallmitao.appmy.mvp;

import com.smallmitao.appmy.bean.WithDrawRecordBean;
import com.smallmitao.appmy.mvp.contract.WithdrawRecordContract;
import com.smallmitao.libbase.http.RetrofitHelper;
import com.smallmitao.libbase.http.appinterface.HttpInter;
import com.smallmitao.libbase.manager.app.BaseUserManager;
import com.smallmitao.libbase.mvp.RxPresenter;
import com.smallmitao.libbase.util.Toastor;
import com.zhouyou.http.callback.SimpleCallBack;
import com.zhouyou.http.exception.ApiException;

import javax.inject.Inject;

public class WithdrawRecordPresenter extends RxPresenter<WithdrawRecordContract.View> implements WithdrawRecordContract.Presenter {

    private RetrofitHelper mHelper;

    @Inject
    public WithdrawRecordPresenter(RetrofitHelper retrofitHelper) {
        this.mHelper = retrofitHelper;
    }

    @Override
    public void getRecord(int page, int size) {
        addDisposable(mHelper
                .getRequest(HttpInter.StoreCashLog.PATH)
                .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getStoreToken())
                .params(HttpInter.StoreCashLog.PAGE, String.valueOf(page))
                .params(HttpInter.StoreCashLog.PAGE_SIZE, String.valueOf(size))
                .execute(new SimpleCallBack<WithDrawRecordBean>() {

                    @Override
                    public void onError(ApiException e) {
                        Toastor.showToast(e.getMessage());
                        getView().setRecord(false, null);
                    }

                    @Override
                    public void onSuccess(WithDrawRecordBean incomeBean) {
                        getView().setRecord(true, incomeBean);
                    }
                }));
    }
}

