package com.smallmitao.appmy.mvp;

import android.app.Activity;
import android.text.TextUtils;

import com.smallmitao.appmy.bean.HeaderBean;
import com.smallmitao.appmy.bean.ShopImgBean;
import com.smallmitao.appmy.bean.StoreShopBean;
import com.smallmitao.appmy.mvp.contract.ShopCenterContract;
import com.smallmitao.libbase.http.RetrofitHelper;
import com.smallmitao.libbase.http.api.BaseApiResult;
import com.smallmitao.libbase.http.appinterface.HttpInter;
import com.smallmitao.libbase.manager.app.BaseUserManager;
import com.smallmitao.libbase.mvp.RxPresenter;
import com.smallmitao.libbase.util.DialogUtil;
import com.smallmitao.libbase.util.LuBanUtils;
import com.smallmitao.libbase.util.Toastor;
import com.zhouyou.http.callback.SimpleCallBack;
import com.zhouyou.http.exception.ApiException;
import com.zhouyou.http.model.HttpParams;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class ShopCenterPresenter extends RxPresenter<ShopCenterContract.View> implements ShopCenterContract.Presenter {

    private RetrofitHelper mHelper;
    private DialogUtil mLoading;
    private Activity mActivity;
    private HeaderBean mHeaderBean;
    private CompositeDisposable mDisposable;


    @Inject
    public ShopCenterPresenter(Activity activity, RetrofitHelper retrofitHelper) {
        this.mHelper = retrofitHelper;
        this.mActivity = activity;
        mDisposable = new CompositeDisposable();
    }

    @Override
    public void exitLogin() {
        addDisposable(mHelper
                .getRequest(HttpInter.Logout.PATH)
                .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getStoreToken())
                .execute(new SimpleCallBack<BaseApiResult>() {

                    @Override
                    public void onError(ApiException e) {
                        Toastor.showToast(e.getMessage() + ",请重新选择上传");
                    }

                    @Override
                    public void onSuccess(BaseApiResult bean) {
                        getView().setExitLogin();
                    }
                }));
    }

    @Override
    public void requestShopInfo() {

    }

    /*上传头像*/
    @Override
    public void updateHead(String path) {
        addDisposable(mHelper
                .postRequest(HttpInter.UploadImg.PATH)
                .params(HttpInter.UploadImg.TYPE, "store")
                .params(HttpInter.UploadImg.NAME, new File(path), null)
                .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getStoreToken())
                .execute(new SimpleCallBack<HeaderBean>() {

                    @Override
                    public void onError(ApiException e) {
                        Toastor.showToast(e.getMessage() + ",请重新选择上传");
                    }

                    @Override
                    public void onSuccess(HeaderBean bean) {
                        mHeaderBean = bean;
                        Toastor.showToast("上传头像成功！");
                    }
                }));
    }

    /*店铺图片*/
    @Override
    public void updateShopImg( StoreShopBean shopBean, final List<String> lastPath) {
        if (mLoading == null)
            mLoading = new DialogUtil(mActivity, "正在上传中...");
        if (!mLoading.isShowing()) {
            mLoading.showDialog();
        }

        List<File> mFiles = new ArrayList<>();
        for (String path : shopBean.getStore_img()) {
            File file = new File(path);
            mFiles.add(file);
        }
        onLuBanImage(mFiles,shopBean,lastPath);
    }


    /**
     * 压缩多张图片
     *
     * @param mFiles
     */
    public void onLuBanImage(final List<File> mFiles, final StoreShopBean shopBean,final List<String> lastPath) {
        LuBanUtils.getInstance().withRx(mDisposable, mFiles, new LuBanUtils.OnLFinishListener() {
            @Override
            public void luBanFinish(final List<File> onResultFiles) {
                requestLoadUp(onResultFiles,shopBean,lastPath);
            }
        });
    }

    public void requestLoadUp(final List<File> mFiles, final StoreShopBean shopBean,final List<String> lastPath) {
        if (mFiles.size() > 0) {
            new Thread() {
                @Override
                public void run() {
                    upLoadUrl(mFiles,shopBean,lastPath);
                }
            }.start();
        }
    }

    private void upLoadUrl(List<File> mFiles, final StoreShopBean shopBean, final List<String> lastPath){
        HttpParams httpParam = new HttpParams();
        for (File file : mFiles) {
            httpParam.put("file[]", file, null);
        }
        httpParam.put(HttpInter.MoreUnloadImg.type, "store");
        addDisposable(mHelper
                .postRequest(HttpInter.MoreUnloadImg.PATH)
                .params(httpParam)
                .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getStoreToken())
                .execute(new SimpleCallBack<ShopImgBean>() {

                    @Override
                    public void onError(ApiException e) {
                        mLoading.dismiss();
                        Toastor.showToast(e.getMessage() + ",请重新选择上传");
                    }

                    @Override
                    public void onSuccess(ShopImgBean bean) {
                        if (lastPath != null && lastPath.size() > 0) {
                            List<String> url = bean.getUrl();
                            url.addAll(lastPath);
                            shopBean.setStore_img(url);
                        } else
                            shopBean.setStore_img(bean.getUrl());

                        updateInfo(shopBean);
                    }
                }));
    }

    @Override
    public void updateInfo(final StoreShopBean shopBean) {
        if (mLoading == null)
            mLoading = new DialogUtil(mActivity, "正在上传中...");
        if (!mLoading.isShowing()) {
            mLoading.showDialog();
        }
        HttpParams httpParams = new HttpParams();
        httpParams.put(HttpInter.StoreUpdate.STORE_PHONE, shopBean.getStore_phone());
        if (!TextUtils.isEmpty(shopBean.getProvince())) {
            httpParams.put(HttpInter.StoreUpdate.PROVINCE, shopBean.getProvince());
            httpParams.put(HttpInter.StoreUpdate.CITY, shopBean.getCity());
            httpParams.put(HttpInter.StoreUpdate.AREA, shopBean.getArea());
        }
        if (!TextUtils.isEmpty(shopBean.getAddress())) {
            httpParams.put(HttpInter.StoreUpdate.ADDRESS, shopBean.getAddress());
        }
        if (mHeaderBean != null && !TextUtils.isEmpty(mHeaderBean.getUrl())) {
            httpParams.put(HttpInter.StoreUpdate.HEAD_IMG, mHeaderBean.getUrl());
        }
        if (shopBean.getStore_img() != null && shopBean.getStore_img().size() > 0) {
            String paths = "";
            for (String path : shopBean.getStore_img()) {
                paths += path + ",";
            }
            httpParams.put(HttpInter.StoreUpdate.STORE_IMG, paths.substring(0, paths.length() - 1));
        }
        addDisposable(mHelper
                .postRequest(HttpInter.StoreUpdate.PATH)
                .params(httpParams)
                .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getStoreToken())
                .execute(new SimpleCallBack<BaseApiResult>() {

                    @Override
                    public void onError(ApiException e) {
                        mLoading.dismiss();
                        Toastor.showToast(e.getMessage());
                    }

                    @Override
                    public void onSuccess(BaseApiResult o) {
                        mLoading.dismiss();
                        getView().saveSuccess(shopBean);
                        Toastor.showToast("保存成功");
                    }
                }));
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mDisposable != null && !mDisposable.isDisposed()) {
            mDisposable.dispose();
        }
    }

}
