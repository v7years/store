package com.smallmitao.appmy.mvp.contract;

import com.smallmitao.libbase.mvp.BasePresenter;
import com.smallmitao.libbase.mvp.BaseView;

public interface StoreWithdrawContract {
    interface View extends BaseView{

    }
    interface Presenter extends BasePresenter<View> {
        void submitWithdraw(String sum,String account,String name);
    }
}
