package com.smallmitao.appmy.mvp.contract;

import android.graphics.Bitmap;

import com.smallmitao.appmy.bean.StoreShopBean;
import com.smallmitao.libbase.mvp.BasePresenter;
import com.smallmitao.libbase.mvp.BaseView;

import java.io.File;
import java.util.List;

public interface ShopCenterContract {
    interface View extends BaseView{
        void setExitLogin();
        void error(String error);
        void saveSuccess(StoreShopBean shopBean);

    }
    interface Presenter extends BasePresenter<View> {
        void exitLogin();
        void requestShopInfo();
        void updateInfo(StoreShopBean shopBean);
        void updateHead(String path);
        void updateShopImg(StoreShopBean shopBean,List<String> lastPath);
    }
}
