package com.smallmitao.appmy.mvp.contract;


import com.smallmitao.appmy.bean.StoreIncomeBean;
import com.smallmitao.appmy.bean.StoreShopBean;
import com.smallmitao.libbase.http.appinterface.HttpInter;
import com.smallmitao.libbase.mvp.BasePresenter;
import com.smallmitao.libbase.mvp.BaseView;

/**
 * 首页
 * @author Jackshao
 */
public interface StoreMyContract {

    interface View extends BaseView {
//        void getShopInfo(int status ,String reason);
        void getStoreShopInfo(StoreShopBean data);
        void getStoreIncomeInfo(StoreIncomeBean data);
    }

    interface Presenter extends BasePresenter<View> {
        void requestInfo();
        void requestIncome();
    }
}
