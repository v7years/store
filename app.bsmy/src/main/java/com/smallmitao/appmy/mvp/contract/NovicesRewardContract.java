package com.smallmitao.appmy.mvp.contract;

import com.smallmitao.libbase.mvp.BasePresenter;
import com.smallmitao.libbase.mvp.BaseView;

public interface NovicesRewardContract {
    interface View extends BaseView{

    }
    interface Presenter extends BasePresenter<View> {
        void NovicesReward(String new_discount);
    }
}
