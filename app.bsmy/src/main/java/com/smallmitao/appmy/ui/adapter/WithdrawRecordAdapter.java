package com.smallmitao.appmy.ui.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.smallmitao.appmy.R;
import com.smallmitao.appmy.bean.WithDrawRecordBean;

public class WithdrawRecordAdapter extends BaseQuickAdapter<WithDrawRecordBean.ListBean, BaseViewHolder> {


    public WithdrawRecordAdapter() {
        super(R.layout.item_record);
    }

    @Override
    protected void convert(BaseViewHolder helper, WithDrawRecordBean.ListBean item) {
        helper.setText(R.id.desc, item.getDesc());
        helper.setText(R.id.record_time, item.getCreated_at());
        helper.setText(R.id.change_cash, item.getChange_cash());

    }
}
