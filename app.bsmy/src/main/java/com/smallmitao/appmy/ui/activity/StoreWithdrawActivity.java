package com.smallmitao.appmy.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.smallmitao.appmy.R;
import com.smallmitao.appmy.R2;
import com.smallmitao.appmy.di.componet.ActivityWithdrawComponent;
import com.smallmitao.appmy.di.componet.DaggerActivityWithdrawComponent;
import com.smallmitao.appmy.di.module.ActivityWithdrawModule;
import com.smallmitao.appmy.mvp.StoreWithdrawPresenter;
import com.smallmitao.appmy.mvp.contract.StoreWithdrawContract;
import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.mvp.BaseMvpActivity;
import com.smallmitao.libbase.util.Toastor;
import com.smallmitao.libbridge.router.BridgeRouter;

import butterknife.BindView;
import butterknife.OnClick;

@Route(path = BridgeRouter.STORE_ACTIVITY_WITHDRAWAL, name = "提现")
public class StoreWithdrawActivity extends BaseMvpActivity<StoreWithdrawPresenter> implements StoreWithdrawContract.View {

    @BindView(R2.id.back_btn)
    ImageView backBtn;
    @BindView(R2.id.title_tv)
    TextView titleTv;
    @BindView(R2.id.title_rightTv)
    TextView titleRightTv;
    @BindView(R2.id.top_bar)
    Toolbar toolbar;
    @BindView(R2.id.mark)
    TextView mark;
    @BindView(R2.id.withdraw_money)
    EditText withdrawMoney;
    @BindView(R2.id.receiver_account)
    EditText receiverAccount;
    @BindView(R2.id.receiver_name)
    EditText receiverName;
    @BindView(R2.id.hint)
    TextView hint;

    @Autowired
    String accountRemain;
    private String remain;

    @Override
    protected int getLayout(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_store_withdraw;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(toolbar).init();

    }

    @Override
    protected void initInject() {
        getWithdrawComponent().inject(this);
    }

    public ActivityWithdrawComponent getWithdrawComponent() {
        return DaggerActivityWithdrawComponent.builder()
                .baseAppComponent(BaseApp.getInstance().getBaseAppComponent())
                .activityWithdrawModule(new ActivityWithdrawModule(this))
                .build();
    }


    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {
        ARouter.getInstance().inject(this);
        backBtn.setVisibility(View.VISIBLE);
        titleTv.setText(getString(R.string.withdraw));
        remain = TextUtils.isEmpty(accountRemain) ? "0" : accountRemain;
        withdrawMoney.setHint("当前最多可提现" + remain + "元");
    }

    @OnClick({R2.id.confirm, R2.id.title_tv, R2.id.back_btn})
    public void onClick(View view) {
        if (view.getId() == R.id.confirm) {
            if (TextUtils.isEmpty(withdrawMoney.getText().toString()) || TextUtils.isEmpty(receiverAccount.getText().toString()) ||
                    TextUtils.isEmpty(receiverName.getText().toString())) {
                Toastor.showToast("请填写完成信息");
            } else if (Double.parseDouble(withdrawMoney.getText().toString()) > Double.parseDouble(remain)) {
                Toastor.showToast("输入金额有误");
            } else {
                mPresenter.submitWithdraw(withdrawMoney.getText().toString(), receiverAccount.getText().toString(), receiverName.getText().toString());
            }

        } else if (view.getId() == R.id.back_btn) {
            finish();
        }
    }
}
