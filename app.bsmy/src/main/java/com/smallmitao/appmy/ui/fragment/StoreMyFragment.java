package com.smallmitao.appmy.ui.fragment;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.smallmitao.appmy.R;
import com.smallmitao.appmy.R2;
import com.smallmitao.appmy.bean.StoreIncomeBean;
import com.smallmitao.appmy.bean.StoreShopBean;
import com.smallmitao.appmy.di.componet.DaggerMyFragmentComponent;
import com.smallmitao.appmy.di.componet.MyFragmentComponent;
import com.smallmitao.appmy.di.module.MyFragmentModule;
import com.smallmitao.appmy.mvp.StoreMyPresenter;
import com.smallmitao.appmy.mvp.contract.StoreMyContract;
import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.mvp.BaseMvpFragment;
import com.smallmitao.libbase.util.GlideUtils;
import com.smallmitao.libbridge.router.BridgeRouter;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * @author Jackshao
 * @date 2019/1/2
 */

@Route(path = BridgeRouter.STORE_FRAGMENT_MY, name = "我的")
public class StoreMyFragment extends BaseMvpFragment<StoreMyPresenter> implements StoreMyContract.View, OnRefreshListener, OnLoadMoreListener {


    @BindView(R2.id.back_btn)
    ImageView backBtn;
    @BindView(R2.id.title_tv)
    TextView titleTv;
    @BindView(R2.id.title_rightTv)
    TextView titleRightTv;
    @BindView(R2.id.toolbar)
    Toolbar toolbar;
    @BindView(R2.id.shopper_head)
    ImageView shopperHead;
    @BindView(R2.id.shop_name)
    TextView shopName;
    @BindView(R2.id.shop_id)
    TextView shopId;
    @BindView(R2.id.account)
    TextView account;
    @BindView(R2.id.account_money)
    TextView accountMoney;
    @BindView(R2.id.total)
    TextView total;
    @BindView(R2.id.total_money)
    TextView totalMoney;
    @BindView(R2.id.wait_money)
    TextView waitMoney;
    @BindView(R2.id.wait)
    TextView wait;
    @BindView(R2.id.address)
    TextView address;
    @BindView(R2.id.image_two)
    ImageView imageTwo;
    @BindView(R2.id.image_one)
    ImageView imageOne;

    Unbinder unbinder;
    private StoreShopBean mShopBean;
    private StoreIncomeBean mStoreIncomeBean;


    @Override
    protected int getLayout() {
        return R.layout.fragment_store_my;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(toolbar).init();
    }

    @Override
    protected void initInject() {
        getFragmentComponent().inject(this);
    }

    protected MyFragmentComponent getFragmentComponent() {
        return DaggerMyFragmentComponent.builder()
                .baseAppComponent(BaseApp.getInstance().getBaseAppComponent())
                .myFragmentModule(new MyFragmentModule(this))
                .build();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.requestIncome();
        mPresenter.requestInfo();
    }

    @Override
    protected void initData() {
        titleTv.setText(getString(R.string.my));
        mPresenter.requestIncome();
        mPresenter.requestInfo();

    }


    @Override
    public void getStoreShopInfo(StoreShopBean data) {
        mShopBean = data;
        shopName.setText(data.getName());
        shopId.setText(data.getAccount());
        address.setText(data.getFull_address());
        GlideUtils.getInstance().loadCircleImage(shopperHead, data.getHead_img());
        if (data.getStore_img().size() > 0 && data.getStore_img().size() > 1) {
            GlideUtils.getInstance().loadNormalImage(imageOne, data.getStore_img().get(0));
            GlideUtils.getInstance().loadNormalImage(imageTwo, data.getStore_img().get(1));
        } else if (data.getStore_img().size() > 0 && data.getStore_img().size() < 2) {
            GlideUtils.getInstance().loadNormalImage(imageOne, data.getStore_img().get(0));
        } else if (data.getStore_img().size() < 1) {
            imageOne.setVisibility(View.INVISIBLE);
            imageTwo.setVisibility(View.INVISIBLE);
        } else if (data.getStore_img().size() > 0 && data.getStore_img().size() < 2) {
            imageTwo.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public void getStoreIncomeInfo(StoreIncomeBean data) {
        mStoreIncomeBean = data;
        accountMoney.setText(data.getAccount_remain());
        totalMoney.setText(data.getTotal_income());
        waitMoney.setText(data.getDrz_income());

    }

    @OnClick({R2.id.withdraw, R2.id.set_reward, R2.id.look_detail, R2.id.shopper_detail, R2.id.withdraw_record})
    public void onViewClicked(View view) {
        if (view.getId() == R.id.withdraw) {
            ARouter.getInstance()
                    .build(Uri.parse(BridgeRouter.STORE_ACTIVITY_WITHDRAWAL))
                    .withString("accountRemain", mStoreIncomeBean.getAccount_remain()).navigation();

        } else if (view.getId() == R.id.set_reward) {
            ARouter.getInstance()
                    .build(Uri.parse(BridgeRouter.STORE_ACTIVITY_NOVICES_REWARD))
                    .withString("new_discount", String.valueOf(mShopBean.getNew_discount()))
                    .navigation();

        } else if (view.getId() == R.id.look_detail || view.getId() == R.id.shopper_detail) {
            ARouter.getInstance()
                    .build(Uri.parse(BridgeRouter.STORE_MY_ACTIVITY_SHOP_CENTER))
                    .withSerializable("mShopBean", mShopBean)
                    .navigation();
        } else if (view.getId() == R.id.withdraw_record) {
            ARouter.getInstance()
                    .build(Uri.parse(BridgeRouter.STORE_ACTIVITY_WITHDRAWAL_RECORD))
                    .navigation();
        }

    }

    @Override
    public void onLoadMore(@NonNull RefreshLayout refreshLayout) {

    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
