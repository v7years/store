package com.smallmitao.appmy.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.smallmitao.appmy.R;
import com.smallmitao.appmy.R2;
import com.smallmitao.libbase.base.BaseActivity;
import com.smallmitao.libbridge.router.BridgeRouter;

import butterknife.BindView;
import butterknife.OnClick;

@Route(path = BridgeRouter.STORE_ACTIVITY_WITHDRAWAL_RESULT, name = "提现结果")
public class StoreWithdrawResultActivity extends BaseActivity {

    @BindView(R2.id.back_btn)
    ImageView backBtn;
    @BindView(R2.id.title_tv)
    TextView titleTv;
    @BindView(R2.id.top_bar)
    Toolbar toolbar;
    @BindView(R2.id.status_icon)
    ImageView statusIcon;
    @BindView(R2.id.status_title)
    TextView statusTitle;
    @BindView(R2.id.account_data)
    TextView accountData;

    @Autowired
    boolean isSuccess;
    @Autowired
    String message;

    @Override
    protected int getLayout(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_store_withdraw_result;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(toolbar).init();
    }

    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {
        ARouter.getInstance().inject(this);
        backBtn.setVisibility(View.VISIBLE);
        titleTv.setText(getString(R.string.withdraw_result));
        if (isSuccess) {
            accountData.setText(message);
        } else {
            statusIcon.setImageResource(R.mipmap.withdraw_fail);
            statusTitle.setText(message);
        }

    }

    @OnClick({R2.id.finish, R2.id.back_btn})
    public void onViewClicked(View view) {
        if (view.getId() == R.id.finish) {
            finish();
        } else if (view.getId() == R.id.back_btn) {
            finish();
        }
    }
}
