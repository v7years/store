package com.smallmitao.appmy.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.android.arouter.utils.TextUtils;
import com.smallmitao.appmy.R;
import com.smallmitao.appmy.R2;
import com.smallmitao.appmy.di.componet.DaggerNovicesRewardComponent;
import com.smallmitao.appmy.di.componet.NovicesRewardComponent;
import com.smallmitao.appmy.di.module.NovicesRewardModule;
import com.smallmitao.appmy.mvp.NoviceRewardPresenter;
import com.smallmitao.appmy.mvp.contract.NovicesRewardContract;
import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.mvp.BaseMvpActivity;
import com.smallmitao.libbase.util.Toastor;
import com.smallmitao.libbridge.router.BridgeRouter;

import butterknife.BindView;
import butterknife.OnClick;

@Route(path = BridgeRouter.STORE_ACTIVITY_NOVICES_REWARD, name = "新手奖励")
public class NovicesRewardActivity extends BaseMvpActivity<NoviceRewardPresenter> implements NovicesRewardContract.View {


    @BindView(R2.id.back_btn)
    ImageView backBtn;
    @BindView(R2.id.title_tv)
    TextView titleTv;
    @BindView(R2.id.title_rightTv)
    TextView titleRightTv;
    @BindView(R2.id.top_bar)
    Toolbar toolbar;
    @BindView(R2.id.discount)
    EditText discount;

    @Autowired
    String new_discount;

    @Override
    protected int getLayout(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_novices_reward;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(toolbar).init();
    }

    @Override
    protected void initInject() {
        getNovicesRewardComponent().inject(this);
    }

    public NovicesRewardComponent getNovicesRewardComponent() {
        return DaggerNovicesRewardComponent.builder()
                .baseAppComponent(BaseApp.getInstance().getBaseAppComponent())
                .novicesRewardModule(new NovicesRewardModule(this))
                .build();
    }

    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {
        ARouter.getInstance().inject(this);
        backBtn.setVisibility(View.VISIBLE);
        titleTv.setText("新手奖励");
        discount.setText(new_discount);
    }

    @OnClick({R2.id.update, R2.id.back_btn})
    public void onViewClicked(View view) {
        if (view.getId() == R.id.back_btn) {
            finish();
        } else if (view.getId() == R.id.update) {
            if (TextUtils.isEmpty(discount.getText().toString())) {
                Toastor.showToast("请输入折扣比例");
            } else
                mPresenter.NovicesReward(discount.getText().toString());
        }

    }
}
