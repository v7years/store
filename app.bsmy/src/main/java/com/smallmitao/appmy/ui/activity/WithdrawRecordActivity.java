package com.smallmitao.appmy.ui.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.smallmitao.appmy.R;
import com.smallmitao.appmy.R2;
import com.smallmitao.appmy.bean.WithDrawRecordBean;
import com.smallmitao.appmy.di.componet.ActivityWithdrawRecordComponent;
import com.smallmitao.appmy.di.componet.DaggerActivityWithdrawRecordComponent;
import com.smallmitao.appmy.di.module.ActivityWithdrawRecordModule;
import com.smallmitao.appmy.mvp.WithdrawRecordPresenter;
import com.smallmitao.appmy.mvp.contract.WithdrawRecordContract;
import com.smallmitao.appmy.ui.adapter.WithdrawRecordAdapter;
import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.mvp.BaseMvpActivity;
import com.smallmitao.libbase.util.Toastor;
import com.smallmitao.libbridge.router.BridgeRouter;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

@Route(path = BridgeRouter.STORE_ACTIVITY_WITHDRAWAL_RECORD, name = "提现记录")
public class WithdrawRecordActivity extends BaseMvpActivity<WithdrawRecordPresenter> implements WithdrawRecordContract.View, OnRefreshListener, OnLoadMoreListener {

    @BindView(R2.id.back_btn)
    ImageView backBtn;
    @BindView(R2.id.title_tv)
    TextView titleTv;
    @BindView(R2.id.top_bar)
    Toolbar toolbar;
    @BindView(R2.id.record_list)
    RecyclerView recordList;
    @BindView(R2.id.refresh_layout)
    SmartRefreshLayout refreshLayout;

    private int page = 1;
    private int size = 20;
    private WithdrawRecordAdapter mAdapter;
    private View mNotDataView;

    @Override
    protected int getLayout(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_withdraw_record;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(toolbar).init();
    }

    @Override
    protected void initInject() {
        getWithdrawRecordComponent().inject(this);
    }

    public ActivityWithdrawRecordComponent getWithdrawRecordComponent() {
        return DaggerActivityWithdrawRecordComponent.builder()
                .baseAppComponent(BaseApp.getInstance().getBaseAppComponent())
                .activityWithdrawRecordModule(new ActivityWithdrawRecordModule(this))
                .build();
    }

    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {
        mNotDataView=getLayoutInflater().inflate(R.layout.no_data_view, (ViewGroup) recordList.getParent(), false);
        backBtn.setVisibility(View.VISIBLE);
        titleTv.setVisibility(View.VISIBLE);
        titleTv.setText(getString(R.string.withdraw_recored));
        mPresenter.getRecord(page, size);
        recordList.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new WithdrawRecordAdapter();
        recordList.setAdapter(mAdapter);
    }

    @OnClick(R2.id.back_btn)
    public void onViewClicked() {
        finish();
    }

    @Override
    public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
        page++;
        mPresenter.getRecord(page, size);
    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {
        page++;
        mPresenter.getRecord(page, size);
    }

    @Override
    public void setRecord(boolean isOk, WithDrawRecordBean bean) {
        if (isOk) {
            refreshLayout.finishRefresh(true);
            setData(bean.getList());
        } else {
            if (page == 1) {
                refreshLayout.finishRefresh(false);
            } else {
                refreshLayout.finishLoadMore(false);
            }
        }

    }

    private void setData(List<WithDrawRecordBean.ListBean> data) {
        final int size = data == null ? 0 : data.size();
        boolean isRefresh = page == 1;
        if (isRefresh) {
            mAdapter.replaceData(data);
        } else {
            if (size > 0) {
                mAdapter.addData(data);
            }
        }
        if (size < size) {
            //第一页如果不够一页就不显示没有更多数据布局
            refreshLayout.finishLoadMoreWithNoMoreData();
            if (size == 0) {
                mAdapter.setEmptyView(mNotDataView);
            } else {
                if (page > 1) {
                    Toastor.showToast("没有更多数据了");
                }
            }
        } else {
            refreshLayout.finishLoadMore();
        }
    }

    @Override
    public void error(String error) {

    }
}
