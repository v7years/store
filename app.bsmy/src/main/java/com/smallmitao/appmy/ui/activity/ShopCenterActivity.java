package com.smallmitao.appmy.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.smallmitao.appmy.R;
import com.smallmitao.appmy.R2;
import com.smallmitao.appmy.bean.StoreShopBean;
import com.smallmitao.appmy.di.componet.ActivityShopCenterComponent;
import com.smallmitao.appmy.di.componet.DaggerActivityShopCenterComponent;
import com.smallmitao.appmy.di.module.ActivityShopCenterModule;
import com.smallmitao.appmy.mvp.ShopCenterPresenter;
import com.smallmitao.appmy.mvp.contract.ShopCenterContract;
import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.bean.AreaBean;
import com.smallmitao.libbase.manager.app.BaseUserManager;
import com.smallmitao.libbase.mvp.BaseMvpActivity;
import com.smallmitao.libbase.ui.AreaDialogFragment;
import com.smallmitao.libbase.ui.SelectPhotoDialog;
import com.smallmitao.libbase.util.GlideUtils;
import com.smallmitao.libbase.util.Toastor;
import com.smallmitao.libbridge.router.BridgeRouter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.bingoogolapple.photopicker.activity.BGAPhotoPickerActivity;
import cn.bingoogolapple.photopicker.activity.BGAPhotoPickerPreviewActivity;
import cn.bingoogolapple.photopicker.util.BGAPhotoHelper;
import cn.bingoogolapple.photopicker.util.BGAPhotoPickerUtil;
import cn.bingoogolapple.photopicker.widget.BGASortableNinePhotoLayout;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

@Route(path = BridgeRouter.STORE_MY_ACTIVITY_SHOP_CENTER, name = "店铺中心")
public class ShopCenterActivity extends BaseMvpActivity<ShopCenterPresenter> implements ShopCenterContract.View, EasyPermissions.PermissionCallbacks, BGASortableNinePhotoLayout.Delegate {


    @BindView(R2.id.back_btn)
    ImageView backBtn;
    @BindView(R2.id.title_tv)
    TextView titleTv;
    @BindView(R2.id.title_rightTv)
    TextView titleRightTv;
    @BindView(R2.id.toolbar)
    Toolbar toolbar;
    @BindView(R2.id.shop_name)
    TextView shopName;
    @BindView(R2.id.account_id)
    TextView accountId;
    @BindView(R2.id.address_area)
    TextView addressArea;
    @BindView(R2.id.address_detail)
    EditText addressDetail;
    @BindView(R2.id.phone_num)
    EditText phoneNum;
    @BindView(R2.id.shop_head)
    ImageView shopHeader;
    @BindView(R2.id.add_photos)
    BGASortableNinePhotoLayout mPhotoLayout;

    @Autowired
    StoreShopBean mShopBean;

    private BGAPhotoHelper mPhotoHelper;

    private static final int RC_PERMISSION_CHOOSE_PHOTO = 1;
    private static final int RC_PERMISSION_TAKE_PHOTO = 2;
    private static final int RC_ICON_CHOOSE_PHOTO = 1;
    private static final int RC_ICON_TAKE_PHOTO = 2;
    private static final int RC_ICON_CROP = 3;
    private static final int RC_SHOP_CHOOSE_PHOTO = 4;
    private static final int RC_PHOTO_PREVIEW = 5;
    private static final int PRC_PHOTO_PICKER = 1;
    private File takePhotoDir;
    private List<String> addShopImg;
    private List<String> beforeShopImg;


    @Override
    protected int getLayout(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_shop_center;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(toolbar).init();
    }

    @Override
    protected void initInject() {
        getShopCenterComponent().inject(this);
    }

    public ActivityShopCenterComponent getShopCenterComponent() {
        return DaggerActivityShopCenterComponent.builder()
                .baseAppComponent(BaseApp.getInstance().getBaseAppComponent())
                .activityShopCenterModule(new ActivityShopCenterModule(this))
                .build();
    }

    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {
        ARouter.getInstance().inject(this);
        mPhotoLayout.setDelegate(this);
        // 拍照后照片的存放目录，改成你自己拍照后要存放照片的目录。如果不传递该参数的话就没有拍照功能
        takePhotoDir = new File(Environment.getExternalStorageDirectory(), "smallmitaoBusinessTakePhoto");
        mPhotoHelper = new BGAPhotoHelper(takePhotoDir);
        addShopImg = new ArrayList<>();
        titleTv.setText(getString(R.string.shop_center));
        backBtn.setVisibility(View.VISIBLE);
        titleRightTv.setVisibility(View.VISIBLE);
        titleRightTv.setText("退出登录");
        if (!TextUtils.isEmpty(mShopBean.getHead_img()))
            GlideUtils.getInstance().loadCircleImage(shopHeader, mShopBean.getHead_img());
        shopName.setText(mShopBean.getName());
        accountId.setText(mShopBean.getAccount());
        addressArea.setText(mShopBean.getProvince_view() + "-" + mShopBean.getCity_view() + "-" + mShopBean.getArea_view());
        addressDetail.setText(mShopBean.getAddress());
        phoneNum.setText(mShopBean.getStore_phone());
        beforeShopImg = mShopBean.getStore_img();
        if (mShopBean.getStore_img() != null && mShopBean.getStore_img().size() > 0) {
            ArrayList<String> paths = new ArrayList<String>();
            paths.addAll(mShopBean.getStore_img());
            mPhotoLayout.setData(paths);
        }
    }

    @Override
    public void setExitLogin() {
        BaseUserManager.getInstance().setStoreLoginOut();
        ARouter.getInstance()
                .build(Uri.parse(BridgeRouter.PAGE_MAIN_ACTIVITY_IDENTITY))
                .greenChannel()
                .navigation();
    }


    @Override
    public void error(String error) {

    }

    @Override
    public void saveSuccess(StoreShopBean shopBean) {
        addShopImg.clear();
        beforeShopImg.clear();
        beforeShopImg.addAll(shopBean.getStore_img());
    }

    @OnClick({R2.id.shop_head, R2.id.save_info, R2.id.back_btn, R2.id.address_area, R2.id.address_detail,
            R2.id.phone_num, R2.id.title_rightTv})
    public void onViewClicked(View view) {
        if (view.getId() == R.id.shop_head) {
            headDialog();
        } else if (view.getId() == R.id.save_info) {
            if (TextUtils.isEmpty(phoneNum.getText().toString()) || TextUtils.isEmpty(addressDetail.getText().toString())) {
                Toastor.showToast("请填写完成信息");
            } else {
                mShopBean.setStore_phone(phoneNum.getText().toString());
                mShopBean.setAddress(addressDetail.getText().toString());
                mShopBean.setHead_img(mPhotoHelper.getCropFilePath());
                mShopBean.setStore_phone(phoneNum.getText().toString());
                if (addShopImg != null && addShopImg.size() > 0) {
                    mShopBean.setStore_img(addShopImg);
                    mPresenter.updateShopImg(mShopBean, beforeShopImg);
                } else {
                    mShopBean.setStore_img(beforeShopImg);//删除之前的图片
                    mPresenter.updateInfo(mShopBean);
                }

            }

        } else if (view.getId() == R.id.back_btn) {
            finish();
        } else if (view.getId() == R.id.address_area) {
            areaDialog();
        } else if (view.getId() == R.id.address_detail) {
            setFocusable(addressDetail);
        } else if (view.getId() == R.id.phone_num) {
            setFocusable(phoneNum);
        } else if (view.getId() == R.id.title_rightTv) {
            mPresenter.exitLogin();
        }
    }

    private void headDialog() {
        final SelectPhotoDialog dialog = new SelectPhotoDialog(this);
        dialog.setClickListener(new SelectPhotoDialog.ClickListener() {
            @Override
            public void onTakePhotoListener() {
                takePhoto();
                dialog.dismiss();
            }

            @Override
            public void onSelectPhotoListener() {
                choosePhoto();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void areaDialog() {
        FragmentManager fm = getSupportFragmentManager();
        AreaDialogFragment mAreaDialogFragment = new AreaDialogFragment();
        mAreaDialogFragment.setOnAreaCallBack(new AreaDialogFragment.AreaCallback() {
            @Override
            public void onAreaCallBack(List<AreaBean> list) {
                String address = "";
                for (int i = 0; i < list.size(); i++) {
                    AreaBean areaBean = list.get(i);
                    address += areaBean.getValue() + "-";
                    if (i == 0) {
                        mShopBean.setProvince(areaBean.getKey());
                    } else if (i == 1) {
                        mShopBean.setCity(areaBean.getKey());
                    } else {
                        mShopBean.setArea(areaBean.getKey());
                    }
                }
                addressArea.setText(address.substring(0, address.length() - 1));
            }
        });
        mAreaDialogFragment.show(fm, "");
    }


    private void setFocusable(EditText editText) {
        editText.setFocusableInTouchMode(true);
        editText.setFocusable(true);
        editText.requestFocus();
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }

    @AfterPermissionGranted(RC_PERMISSION_CHOOSE_PHOTO)
    public void choosePhoto() {
        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(this, perms)) {
            startActivityForResult(mPhotoHelper.getChooseSystemGalleryIntent(), RC_ICON_CHOOSE_PHOTO);
        } else {
            EasyPermissions.requestPermissions(this, "请开起存储空间权限，以正常使用 Demo", RC_PERMISSION_CHOOSE_PHOTO, perms);
        }
    }

    @AfterPermissionGranted(RC_PERMISSION_TAKE_PHOTO)
    public void takePhoto() {
        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
        if (EasyPermissions.hasPermissions(this, perms)) {
            try {
                startActivityForResult(mPhotoHelper.getTakePhotoIntent(), RC_ICON_TAKE_PHOTO);
            } catch (Exception e) {
                BGAPhotoPickerUtil.show(R.string.bga_pp_not_support_take_photo);
            }
        } else {
            EasyPermissions.requestPermissions(this, "请开起存储空间和相机权限，以正常使用 Demo", RC_PERMISSION_TAKE_PHOTO, perms);
        }
    }

    @AfterPermissionGranted(PRC_PHOTO_PICKER)
    private void choicePhotoWrapper() {
        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
        if (EasyPermissions.hasPermissions(this, perms)) {
            // 拍照后照片的存放目录，改成你自己拍照后要存放照片的目录。如果不传递该参数的话就没有拍照功能
            Intent photoPickerIntent = new BGAPhotoPickerActivity.IntentBuilder(this)
                    .cameraFileDir(takePhotoDir) // 拍照后照片的存放目录，改成你自己拍照后要存放照片的目录。如果不传递该参数的话则不开启图库里的拍照功能
                    .maxChooseCount(mPhotoLayout.getMaxItemCount() - mPhotoLayout.getItemCount()) // 图片选择张数的最大值
                    .selectedPhotos(null) // 当前已选中的图片路径集合
                    .pauseOnScroll(false) // 滚动列表时是否暂停加载图片
                    .build();
            startActivityForResult(photoPickerIntent, RC_SHOP_CHOOSE_PHOTO);
        } else {
            EasyPermissions.requestPermissions(this, "图片选择需要以下权限:\n\n1.访问设备上的照片\n\n2.拍照", PRC_PHOTO_PICKER, perms);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == RC_ICON_CHOOSE_PHOTO) {
                try {
                    startActivityForResult(mPhotoHelper.getCropIntent(mPhotoHelper.getFilePathFromUri(data.getData()), 200, 200), RC_ICON_CROP);
                } catch (Exception e) {
                    mPhotoHelper.deleteCropFile();
                    BGAPhotoPickerUtil.show(R.string.bga_pp_not_support_crop);
                    e.printStackTrace();
                }
            } else if (requestCode == RC_ICON_TAKE_PHOTO) {
                try {
                    startActivityForResult(mPhotoHelper.getCropIntent(mPhotoHelper.getCameraFilePath(), 200, 200), RC_ICON_CROP);
                } catch (Exception e) {
                    mPhotoHelper.deleteCameraFile();
                    mPhotoHelper.deleteCropFile();
                    BGAPhotoPickerUtil.show(R.string.bga_pp_not_support_crop);
                    e.printStackTrace();
                }
            } else if (requestCode == RC_ICON_CROP) {
                GlideUtils.getInstance().loadCircleHead(shopHeader, Uri.fromFile(new File(mPhotoHelper.getCropFilePath())));
                mPresenter.updateHead(mPhotoHelper.getCropFilePath());
            } else if (requestCode == RC_SHOP_CHOOSE_PHOTO) {
                mPhotoLayout.addMoreData(BGAPhotoPickerActivity.getSelectedPhotos(data));
                addShopImg.addAll(BGAPhotoPickerActivity.getSelectedPhotos(data));
                System.out.println("addShopImg====》 " + addShopImg.toString());
            }
        } else if (requestCode == RC_PHOTO_PREVIEW) {
            mPhotoLayout.setData(BGAPhotoPickerPreviewActivity.getSelectedPhotos(data));
        } else {
            if (requestCode == RC_ICON_CROP) {
                mPhotoHelper.deleteCameraFile();
                mPhotoHelper.deleteCropFile();
            }
        }
    }


    @Override
    public void onClickAddNinePhotoItem(BGASortableNinePhotoLayout sortableNinePhotoLayout, View view, int position, ArrayList<String> models) {
        choicePhotoWrapper();
    }

    @Override
    public void onClickDeleteNinePhotoItem(BGASortableNinePhotoLayout sortableNinePhotoLayout, View view, int position, String model, ArrayList<String> models) {
        String remove = mPhotoLayout.getData().get(position);
        if (beforeShopImg != null && beforeShopImg.size() > 0) {
            for (int i = 0; i < beforeShopImg.size(); i++) {
                if (beforeShopImg.get(i).equals(remove)) {
                    beforeShopImg.remove(i);
                }
            }
        }
        if (addShopImg != null && addShopImg.size() > 0) {
            for (int i = 0; i < addShopImg.size(); i++) {
                if (addShopImg.get(i).equals(remove)) {
                    addShopImg.remove(i);
                }
            }
        }
        mPhotoLayout.removeItem(position);
    }

    @Override
    public void onClickNinePhotoItem(BGASortableNinePhotoLayout sortableNinePhotoLayout, View view, int position, String model, ArrayList<String> models) {
        Intent photoPickerPreviewIntent = new BGAPhotoPickerPreviewActivity.IntentBuilder(this)
                .previewPhotos(models) // 当前预览的图片路径集合
                .selectedPhotos(models) // 当前已选中的图片路径集合
                .maxChooseCount(mPhotoLayout.getMaxItemCount()) // 图片选择张数的最大值
                .currentPosition(position) // 当前预览图片的索引
                .isFromTakePhoto(false) // 是否是拍完照后跳转过来
                .build();
        startActivityForResult(photoPickerPreviewIntent, RC_PHOTO_PREVIEW);
    }

    @Override
    public void onNinePhotoItemExchanged(BGASortableNinePhotoLayout sortableNinePhotoLayout, int fromPosition, int toPosition, ArrayList<String> models) {

    }
}
