package com.smallmitao.appmy.bean;


import java.util.List;

/**
 *
 * @author Jackshao
 * @date 2019/1/23
 */

public class WithDrawRecordBean {


  private List<ListBean> list;

  public List<ListBean> getList() {
    return list;
  }

  public void setList(List<ListBean> list) {
    this.list = list;
  }

  public static class ListBean {
    /**
     * desc : 账户余额取现
     * change_cash : -3.00
     * created_at : 2019-02-22 14:17:52
     * type : 2
     */

    private String desc;
    private String change_cash;
    private String created_at;
    private int type;

    public String getDesc() {
      return desc;
    }

    public void setDesc(String desc) {
      this.desc = desc;
    }

    public String getChange_cash() {
      return change_cash;
    }

    public void setChange_cash(String change_cash) {
      this.change_cash = change_cash;
    }

    public String getCreated_at() {
      return created_at;
    }

    public void setCreated_at(String created_at) {
      this.created_at = created_at;
    }

    public int getType() {
      return type;
    }

    public void setType(int type) {
      this.type = type;
    }
  }
}
