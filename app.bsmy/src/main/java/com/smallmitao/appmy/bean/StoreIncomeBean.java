package com.smallmitao.appmy.bean;

/**
 * @author Jackshao
 * @date 2019/1/23
 */

public class StoreIncomeBean {


    /**
     * total_income : 12.00
     * drz_income : 10.00
     * account_remain : 18.50
     */

    private String total_income;
    private String drz_income;
    private String account_remain;

    public String getTotal_income() {
        return total_income;
    }

    public void setTotal_income(String total_income) {
        this.total_income = total_income;
    }

    public String getDrz_income() {
        return drz_income;
    }

    public void setDrz_income(String drz_income) {
        this.drz_income = drz_income;
    }

    public String getAccount_remain() {
        return account_remain;
    }

    public void setAccount_remain(String account_remain) {
        this.account_remain = account_remain;
    }
}
