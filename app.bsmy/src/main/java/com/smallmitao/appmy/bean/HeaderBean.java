package com.smallmitao.appmy.bean;

public class HeaderBean {

    /**
     * url : http://xmtb.oss-cn-hangzhou.aliyuncs.com/upload/images/20190301/store/store88b424f8bfed33220e883af8d68cc4ad1551421889634.jpg?OSSAccessKeyId=LTAIhjHFwn061f0Q&Expires=4705021889&Signature=rUb74rrsl2TJCYUUTRodZPmVQSE%3D
     */

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
