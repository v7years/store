package com.smallmitao.appmy.bean;

import com.smallmitao.libbase.ui.SelectPhotoDialog;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Jackshao
 * @date 2019/1/23
 */

public class StoreShopBean implements Serializable {


  /**
   * store_id : 1
   * account : 18201120544
   * name : 测试
   * head_img : http://gss0.baidu.com/-vo3dSag_xI4khGko9WTAnF6hhy/lvpics/w=1000/sign=a669f57d3a12b31bc76cc929b628377a/503d269759ee3d6d801feef140166d224f4ade2b.jpg
   * store_phone : 1820112054
   * address : 广场路37号
   * province : 31
   * city : 383
   * area : 3233
   * store_img : ["http://hdjahs.oss-cn-shanghai.aliyuncs.com/upload/images/20190122/goods/9a07e99c9b2a99a8dd6074e5c3fca5ac.jpg?OSSAccessKeyId=LTAIaoecDGo0d0Zz&Expires=4701735145&Signature=9rqI64NA4XqGJ80U6vJTvzlQXAU=","http://gss0.baidu.com/-vo3dSag_xI4khGko9WTAnF6hhy/lvpics/w=1000/sign=a669f57d3a12b31bc76cc929b628377a/503d269759ee3d6d801feef140166d224f4ade2b.jpg"]
   * new_discount : 100
   * province_view : 浙江
   * city_view : 杭州
   * area_view : 滨江区
   * full_address : 浙江杭州滨江区广场路37号
   */

  private int store_id;
  private String account;
  private String name;
  private String head_img;
  private String store_phone;
  private String address;
  private String  province;
  private String city;
  private String area;
  private int new_discount;
  private String province_view;
  private String city_view;
  private String area_view;
  private String full_address;
  private List<String> store_img;

  public int getStore_id() {
    return store_id;
  }

  public void setStore_id(int store_id) {
    this.store_id = store_id;
  }

  public String getAccount() {
    return account;
  }

  public void setAccount(String account) {
    this.account = account;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getHead_img() {
    return head_img;
  }

  public void setHead_img(String head_img) {
    this.head_img = head_img;
  }

  public String getStore_phone() {
    return store_phone;
  }

  public void setStore_phone(String store_phone) {
    this.store_phone = store_phone;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getProvince() {
    return province;
  }

  public void setProvince(String province) {
    this.province = province;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getArea() {
    return area;
  }

  public void setArea(String area) {
    this.area = area;
  }

  public int getNew_discount() {
    return new_discount;
  }

  public void setNew_discount(int new_discount) {
    this.new_discount = new_discount;
  }

  public String getProvince_view() {
    return province_view;
  }

  public void setProvince_view(String province_view) {
    this.province_view = province_view;
  }

  public String getCity_view() {
    return city_view;
  }

  public void setCity_view(String city_view) {
    this.city_view = city_view;
  }

  public String getArea_view() {
    return area_view;
  }

  public void setArea_view(String area_view) {
    this.area_view = area_view;
  }

  public String getFull_address() {
    return full_address;
  }

  public void setFull_address(String full_address) {
    this.full_address = full_address;
  }

  public List<String> getStore_img() {
    return store_img;
  }

  public void setStore_img(List<String> store_img) {
    this.store_img = store_img;
  }
}
