package com.smallmitao.appreceipt.di.componet;

import android.app.Activity;

import com.smallmitao.appreceipt.di.module.ReceiptFragmentModule;
import com.smallmitao.appreceipt.ui.fragment.ReceiptFragment;
import com.smallmitao.libbase.di.component.BaseAppComponent;
import com.smallmitao.libbase.di.scope.FragmentScope;

import dagger.Component;


/**
 * @author Jackshao
 */
@FragmentScope
@Component(dependencies = BaseAppComponent.class, modules = ReceiptFragmentModule.class)
public interface ReceiptFragmentComponent {

    Activity getActivity();

    void inject(ReceiptFragment homePageFragment);
}
