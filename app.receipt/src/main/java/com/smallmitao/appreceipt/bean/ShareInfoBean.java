package com.smallmitao.appreceipt.bean;

/**
 * @author Jackshao
 * @date 2019/1/23
 */

public class ShareInfoBean {


    /**
     * url : http://api.smallmitao.com/scan/QRcode?store_id=33&user_id=575036
     * title : 测试店铺
     * content : 邀请您成为小蜜桃用户
     */

    private String url;
    private String title;
    private String content;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
