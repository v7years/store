package com.smallmitao.appreceipt.bean;

/**
 * @author Jackshao
 * @date 2019/1/23
 */

public class QrCodeBean {


    private String url;
    private String store_name;
    private String new_discount;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getNew_discount() {
        return new_discount;
    }

    public void setNew_discount(String new_discount) {
        this.new_discount = new_discount;
    }
}
