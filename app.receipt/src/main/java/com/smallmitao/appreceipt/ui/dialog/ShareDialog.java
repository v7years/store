package com.smallmitao.appreceipt.ui.dialog;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.smallmitao.libbase.R;
import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.appreceipt.bean.ShareInfoBean;
import com.smallmitao.libbase.ui.BaseDialog;
import com.smallmitao.libbase.util.BitmapUtil;
import com.tencent.mm.opensdk.modelmsg.SendMessageToWX;
import com.tencent.mm.opensdk.modelmsg.WXMediaMessage;
import com.tencent.mm.opensdk.modelmsg.WXWebpageObject;


public class ShareDialog extends BaseDialog implements View.OnClickListener {

    private TextView qr;
    private TextView title;
    private TextView cancel;
    private TextView shareFriend;
    private TextView shareCircle;
    private TextView shareQRCode;


    private Context mContext;
    private ShareInfoBean mShareInfoBean;
    private CreateQRListener mQRListener;

    public void setQRListener(CreateQRListener QRListener) {
        mQRListener = QRListener;
    }

    public ShareDialog(Context context, ShareInfoBean bean) {
        super(context, R.layout.dialog_share_pop_layout, R.style.NormalDialogStyle);
        this.mContext = context;
        this.mShareInfoBean = bean;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCanceledOnTouchOutside(true);
        initView();
        listener();
    }

    private void initView() {
        qr = mView.findViewById(R.id.tv_pop_share_to_qr);
        title = mView.findViewById(R.id.title);
        cancel = mView.findViewById(R.id.tv_cancel);
        shareCircle = mView.findViewById(R.id.tv_pop_share_to_wx_fc);
        shareFriend = mView.findViewById(R.id.tv_pop_share_to_wx_f);
        shareQRCode = mView.findViewById(R.id.tv_pop_share_to_qr);
    }

    private void listener() {
        cancel.setOnClickListener(this);
        shareQRCode.setOnClickListener(this);
        shareFriend.setOnClickListener(this);
        shareCircle.setOnClickListener(this);
    }

    private void wxShare(int way) {
        WXWebpageObject webpage = new WXWebpageObject();
        WXMediaMessage msg = null;
        webpage.webpageUrl = mShareInfoBean.getUrl();
        msg = new WXMediaMessage(webpage);
        msg.title = mShareInfoBean.getTitle();
        msg.description = mShareInfoBean.getContent();
        Bitmap bmp = BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.app_login_icon);
        Bitmap thumbBmp = Bitmap.createScaledBitmap(bmp, 150, 150, true);
        msg.thumbData = BitmapUtil.bmpToByteArray(thumbBmp, true);
        bmp.recycle();
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.message = msg;
        req.scene = way == 1 ? SendMessageToWX.Req.WXSceneTimeline : SendMessageToWX.Req.WXSceneSession;
        req.transaction = "webpage";
        BaseApp.mWxApi.sendReq(req);

    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.tv_cancel) {
            this.dismiss();
        } else if (i == R.id.tv_pop_share_to_wx_fc) {
            wxShare(1);
            dismiss();
        } else if (i == R.id.tv_pop_share_to_wx_f) {
            wxShare(0);
            dismiss();
        } else if (i == R.id.tv_pop_share_to_qr) {
            mQRListener.onQRListener();
            dismiss();
        }
    }


    public interface CreateQRListener {
        void onQRListener();
    }
}
