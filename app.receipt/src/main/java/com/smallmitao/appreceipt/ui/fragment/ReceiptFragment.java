package com.smallmitao.appreceipt.ui.fragment;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.smallmitao.appreceipt.R;
import com.smallmitao.appreceipt.R2;
import com.smallmitao.appreceipt.bean.QrCodeBean;
import com.smallmitao.appreceipt.bean.ShareInfoBean;
import com.smallmitao.appreceipt.di.componet.DaggerReceiptFragmentComponent;
import com.smallmitao.appreceipt.di.componet.ReceiptFragmentComponent;
import com.smallmitao.appreceipt.di.module.ReceiptFragmentModule;
import com.smallmitao.appreceipt.mvp.ReceiptPresenter;
import com.smallmitao.appreceipt.mvp.contract.ReceiptContract;
import com.smallmitao.appreceipt.ui.dialog.ShareDialog;
import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.mvp.BaseMvpFragment;
import com.smallmitao.libbase.util.FileUtil;
import com.smallmitao.libbase.util.SystemUtil;
import com.smallmitao.libbase.util.Toastor;
import com.smallmitao.libbridge.router.BridgeRouter;
import com.uuzuche.lib_zxing.activity.CodeUtils;

import net.lucode.hackware.magicindicator.buildins.UIUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.text.Annotation;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * @author Jackshao
 * @date 2019/1/2
 */

@Route(path = BridgeRouter.STORE_FRAGMENT_WITHDRAW_CODE, name = "收款")
public class ReceiptFragment extends BaseMvpFragment<ReceiptPresenter> implements ReceiptContract.View {


    @BindView(R2.id.shop_name)
    TextView shopName;
    @BindView(R2.id.code_iv)
    ImageView codeIv;
    @BindView(R2.id.refresh_tv)
    TextView refreshTv;
    private LinearLayout mShareView;


    Unbinder unbinder;
    private Bitmap mBitmap;
    private QrCodeBean mQrCodeBean;

    @Override
    protected int getLayout() {
        return R.layout.fragment_receipt;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(shopName).init();
    }

    @Override
    protected void initInject() {
        getFragmentComponent().inject(this);
    }

    protected ReceiptFragmentComponent getFragmentComponent() {
        return DaggerReceiptFragmentComponent.builder()
                .baseAppComponent(BaseApp.getInstance().getBaseAppComponent())
                .receiptFragmentModule(new ReceiptFragmentModule(this))
                .build();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.requestInfo();
    }

    @Override
    public void getShopInfo(QrCodeBean qrCodeBean) {
        mQrCodeBean = qrCodeBean;
        shopName.setText(qrCodeBean.getStore_name());
        setCode(qrCodeBean.getUrl(), codeIv);
        ObjectAnimator animator = ObjectAnimator.ofFloat(codeIv, "alpha", 0f, 1f);
        animator.setDuration(500);
        animator.start();
    }

    @Override
    public void getShareInfo(ShareInfoBean qrCodeBean) {
        ShareDialog shareDialog = new ShareDialog(getContext(), qrCodeBean);
        shareDialog.show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @SuppressLint("CheckResult")
    @OnClick({R2.id.save_qr, R2.id.share_link, R2.id.set_subsidy, R2.id.refresh_tv})
    public void onViewClicked(View view) {
        if (view.getId() == R.id.save_qr) {
            if(mQrCodeBean==null){
                Toastor.showToast("数据异常，请重新刷新数据");
                return;
            }
            Disposable subscribe = Observable.create(new ObservableOnSubscribe<Bitmap>() {
                @Override
                public void subscribe(ObservableEmitter<Bitmap> emitter) throws Exception {
                    emitter.onNext(transBitmap());
                }
            }).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<Bitmap>() {
                        @Override
                        public void accept(Bitmap bitmap) throws Exception {
                            if (FileUtil.isSDCardEnable()) {
                                FileOutputStream fos;
                                String fileName = System.currentTimeMillis() + ".png";
                                String filePath = FileUtil.createFile(getContext());
                                File file = new File(filePath + fileName);
                                fos = new FileOutputStream(file);
                                bitmap.compress(Bitmap.CompressFormat.PNG, 90, fos);
                                fos.flush();
                                fos.close();
                                mShareView.destroyDrawingCache();
                                mContext.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));
                                Toastor.showToast("保存成功，请去相册查看");
                            }
                        }
                    });

        } else if (view.getId() == R.id.share_link) {
            mPresenter.requestShareInfo();
        } else if (view.getId() == R.id.set_subsidy) {
            ARouter.getInstance()
                    .build(Uri.parse(BridgeRouter.STORE_ACTIVITY_NOVICES_REWARD))
                    .withString("new_discount", mQrCodeBean.getNew_discount())
                    .navigation();
        } else if (view.getId() == R.id.refresh_tv) {
            mPresenter.requestInfo();
        }
    }


    public void setCode(String code, ImageView imageView) {
        if (!TextUtils.isEmpty(code)) {
            mBitmap = CodeUtils.createImage(code, UIUtil.dip2px(getContext(), 158)
                    , UIUtil.dip2px(getContext(), 158), null);
            imageView.setImageBitmap(mBitmap);
        }
    }

    private Bitmap transBitmap() {
        View layout = LayoutInflater.from(getContext()).inflate(R.layout.share_qr_code_layout, null);
        mShareView = layout.findViewById(R.id.share_layout);
        TextView name = layout.findViewById(R.id.share_shop_name);
        ImageView qr = layout.findViewById(R.id.share_qr_code);
        name.setText(mQrCodeBean.getStore_name());
        setCode(mQrCodeBean.getUrl(), qr);
        mShareView.setDrawingCacheEnabled(true);
        mShareView.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        mShareView.layout(0, 0, SystemUtil.getScreenWidth(getContext()), SystemUtil.getScreenHeight(getContext()));
        Bitmap bitmap = Bitmap.createBitmap(mShareView.getDrawingCache());
        mShareView.setDrawingCacheEnabled(false);
        return bitmap;
    }
}
