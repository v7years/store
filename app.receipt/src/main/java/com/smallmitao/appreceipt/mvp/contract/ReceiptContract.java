package com.smallmitao.appreceipt.mvp.contract;


import com.smallmitao.appreceipt.bean.QrCodeBean;
import com.smallmitao.appreceipt.bean.ShareInfoBean;
import com.smallmitao.libbase.mvp.BasePresenter;
import com.smallmitao.libbase.mvp.BaseView;

/**
 * 首页
 * @author Jackshao
 */
public interface ReceiptContract {

    interface View extends BaseView {
        void getShopInfo(QrCodeBean qrCodeBean);
        void getShareInfo(ShareInfoBean qrCodeBean);
    }

    interface Presenter extends BasePresenter<View> {
        void requestInfo();
        void requestShareInfo();
    }
}
