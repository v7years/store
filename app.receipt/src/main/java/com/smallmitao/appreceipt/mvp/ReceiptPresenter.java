package com.smallmitao.appreceipt.mvp;

import android.app.Activity;

import com.smallmitao.appreceipt.bean.QrCodeBean;
import com.smallmitao.appreceipt.bean.ShareInfoBean;
import com.smallmitao.appreceipt.mvp.contract.ReceiptContract;
import com.smallmitao.libbase.di.scope.FragmentScope;
import com.smallmitao.libbase.http.RetrofitHelper;
import com.smallmitao.libbase.http.appinterface.HttpInter;
import com.smallmitao.libbase.manager.app.BaseUserManager;
import com.smallmitao.libbase.mvp.RxPresenter;
import com.zhouyou.http.callback.SimpleCallBack;
import com.zhouyou.http.exception.ApiException;

import javax.inject.Inject;

/**
 * date: 2018/8/10
 *
 * @author Jackshao
 * 首页
 */

@FragmentScope
public class ReceiptPresenter extends RxPresenter<ReceiptContract.View> implements ReceiptContract.Presenter {

    private RetrofitHelper mHelper;
    private Activity mActivity;


    @Inject
    public ReceiptPresenter(Activity mActivity, RetrofitHelper retrofitHelper) {
        this.mHelper = retrofitHelper;
        this.mActivity = mActivity;
    }

    @Override
    public void requestInfo() {
        addDisposable(mHelper
                .getRequest(HttpInter.StoreQrCode.PATH)
                .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getStoreToken())
                .execute(new SimpleCallBack<QrCodeBean>() {

                    @Override
                    public void onError(ApiException e) {
                    }

                    @Override
                    public void onSuccess(QrCodeBean qrCodeBean) {
                        getView().getShopInfo(qrCodeBean);
                    }
                }));
    }

    @Override
    public void requestShareInfo() {
        addDisposable(mHelper
                .getRequest(HttpInter.ShareInfo.PATH)
                .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getStoreToken())
                .execute(new SimpleCallBack<ShareInfoBean>() {

                    @Override
                    public void onError(ApiException e) {
                    }

                    @Override
                    public void onSuccess(ShareInfoBean qrCodeBean) {
                        getView().getShareInfo(qrCodeBean);
                    }
                }));
    }
}
