package com.smallmitao.appfind.di.module;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import com.smallmitao.appfind.ui.adpater.ShopAdapter;
import com.smallmitao.libbase.di.scope.FragmentScope;
import dagger.Module;
import dagger.Provides;


/**
 * @author Jackshao
 */
@Module
public class FragmentModule {

    private Fragment fragment;

    public FragmentModule(Fragment fragment) {
        this.fragment = fragment;
    }

    @FragmentScope
    @Provides
    Activity provideActivity() {
        return fragment.getActivity();
    }

    @FragmentScope
    @Provides
    ShopAdapter provideShopAdapter(){
        return new ShopAdapter();
    }

    @FragmentScope
    @Provides
    GridLayoutManager provideGridLayoutManager(){
        return new GridLayoutManager(fragment.getActivity(),2);
    }


}
