package com.smallmitao.appfind.di.componet;

import android.app.Activity;

import com.smallmitao.appfind.di.module.FragmentModule;
import com.smallmitao.appfind.ui.fragment.FindFragment;
import com.smallmitao.libbase.di.component.BaseAppComponent;
import com.smallmitao.libbase.di.scope.FragmentScope;

import dagger.Component;


/**
 * @author Jackshao
 */
@FragmentScope
@Component(dependencies = BaseAppComponent.class, modules = FragmentModule.class)
public interface FragmentComponent {

    Activity getActivity();

    void inject(FindFragment homePageFragment);
}
