package com.smallmitao.appfind.ui.adpater;


import android.graphics.Paint;
import android.widget.ImageView;
import android.widget.TextView;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.smallmitao.appfind.R;
import com.smallmitao.appfind.bean.ShopModel;
import com.smallmitao.libbase.util.GlideUtils;

/**
 * @author Jackshao
 * @date 2018/9/18
 */

public class ShopAdapter extends BaseQuickAdapter<ShopModel.ListBean,BaseViewHolder> {


  public ShopAdapter() {
    super(R.layout.item_shop);
  }

  @Override
  protected void convert(BaseViewHolder helper, ShopModel.ListBean item) {
    TextView priceTV = helper.getView(R.id.price_tv);
    helper.setText(R.id.title_tv,"(事例) "+item.getGoods_name());
    helper.setText(R.id.shop_price_tv,"￥"+item.getShop_price());
    helper.setText(R.id.price_tv,"￥"+item.getMarket_price());
    priceTV.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
    ImageView head_img_iv = helper.getView(R.id.head_img_iv);
    GlideUtils.getInstance().loadNormalImage(head_img_iv,item.getGoods_img_url());
  }


}
