package com.smallmitao.appfind.ui.fragment;

import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.smallmitao.appfind.R;
import com.smallmitao.appfind.R2;
import com.smallmitao.appfind.bean.ShopModel;
import com.smallmitao.appfind.di.componet.DaggerFragmentComponent;
import com.smallmitao.appfind.di.componet.FragmentComponent;
import com.smallmitao.appfind.di.module.FragmentModule;
import com.smallmitao.appfind.mvp.ShopPresenter;
import com.smallmitao.appfind.mvp.contract.ShopContract;
import com.smallmitao.appfind.ui.adpater.ShopAdapter;
import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.mvp.BaseMvpFragment;
import com.smallmitao.libbase.ui.CustomDialog;
import com.smallmitao.libbase.util.SpaceItemDecoration;
import com.smallmitao.libbase.util.Toastor;
import com.smallmitao.libbridge.router.BridgeRouter;
import net.lucode.hackware.magicindicator.buildins.UIUtil;
import java.util.List;
import javax.inject.Inject;
import butterknife.BindView;

/**
 * @author Jackshao
 * @date 2019/1/2
 */

@Route(path = BridgeRouter.PAGE_FRAGMENT_FIND, name = "上架商品")
public class FindFragment extends BaseMvpFragment<ShopPresenter> implements ShopContract.View , OnRefreshListener,OnLoadMoreListener {


  @BindView(R2.id.title_tv)
  TextView mTitleTv;
  @BindView(R2.id.toolbar)
  Toolbar mToolbar;
  @BindView(R2.id.refresh_layout)
  SmartRefreshLayout mRefreshLayout;
  @BindView(R2.id.recycle_view)
  RecyclerView mRecycleView;


  @Inject
  GridLayoutManager mGridLayoutManager;

  @Inject
  ShopAdapter mShopAdapter;

  private CustomDialog mCustomDialog;
  private int mPage = 1;
  private int mSize = 20;
  private int mGoodsId;
  private View mNotDataView;

  @Override
  protected int getLayout() {
    return R.layout.fragment_find;
  }

  protected FragmentComponent getFragmentComponent() {
    return DaggerFragmentComponent.builder()
      .baseAppComponent(BaseApp.getInstance().getBaseAppComponent())
      .fragmentModule(new FragmentModule(this))
      .build();
  }

  @Override
  protected void initInject() {
    getFragmentComponent().inject(this);
  }

  @Override
  protected void initImmersionBar() {
    super.initImmersionBar();
    mImmersionBar.titleBar(mToolbar).init();
  }

  @Override
  protected void initData() {
    mTitleTv.setText("上架商品");
    mNotDataView = getLayoutInflater().inflate(R.layout.app_no_data_view, (ViewGroup) mRecycleView.getParent(), false);
    mCustomDialog = new CustomDialog(getActivity(),R.style.MyMiddleDialogStyle,mOnclick);
    mRecycleView.addItemDecoration(new SpaceItemDecoration(2, UIUtil.dip2px(getActivity(),10)));
    mRecycleView.setLayoutManager(mGridLayoutManager);
    mRecycleView.setAdapter(mShopAdapter);
    mRefreshLayout.setEnableLoadMore(true);
    mRefreshLayout.setOnRefreshListener(this);
    mRefreshLayout.setOnLoadMoreListener(this);
    mPresenter.requestShop(String.valueOf(mPage),String.valueOf(mSize));
    mShopAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
      @Override
      public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        mGoodsId = mShopAdapter.getItem(position).getGoods_id();
        mCustomDialog.show();
      }
    });
  }

  private View.OnClickListener mOnclick = new View.OnClickListener() {
    @Override
    public void onClick(View view) {
//      mPresenter.requestUpdateGoods(String.valueOf(mGoodsId));
      mCustomDialog.dismiss();
      Toastor.showToast("示例商品仅供预览，请先申请注册商家资质，敬请谅解。");
    }
  };

  @Override
  public void onRefresh(@NonNull RefreshLayout refreshLayout) {
    mPage = 1;
    mPresenter.requestShop(String.valueOf(mPage),String.valueOf(mSize));
  }

  @Override
  public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
    mPage++;
    mPresenter.requestShop(String.valueOf(mPage),String.valueOf(mSize));
  }


  @Override
  public void getShopModel(boolean isOk, List<ShopModel.ListBean> mList) {
    if (isOk){
      mRefreshLayout.finishRefresh(true);
      setData(mList);
    }else {
      if (mPage == 1) {
        mRefreshLayout.finishRefresh(false);
      } else {
        mRefreshLayout.finishLoadMore(false);
      }
    }
  }

  @Override
  public void getUpdateGoods(boolean isOk) {
    Toastor.showToast(isOk?"下架成功":"下架失败");
    if (mCustomDialog!=null){
      mCustomDialog.dismiss();
    }
  }

  private void setData(List<ShopModel.ListBean> data) {

    final int size = data == null ? 0 : data.size();
    boolean isRefresh = mPage == 1;
    if (isRefresh) {
      mShopAdapter.replaceData(data);
    } else {
      if (size > 0) {
        mShopAdapter.addData(data);
      }
    }
    if (size < mSize) {
      //第一页如果不够一页就不显示没有更多数据布局
      mRefreshLayout.finishLoadMoreWithNoMoreData();
      if (size==0){
        mShopAdapter.setEmptyView(mNotDataView);
      }else {
        if (mPage>1){
          Toastor.showToast("没有更多数据了");
        }
      }
    } else {
      mRefreshLayout.finishLoadMore();
    }
  }


}
