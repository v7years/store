package com.smallmitao.appfind.mvp;

import com.smallmitao.appfind.bean.ShopModel;
import com.smallmitao.appfind.mvp.contract.ShopContract;
import com.smallmitao.libbase.di.scope.FragmentScope;
import com.smallmitao.libbase.http.RetrofitHelper;
import com.smallmitao.libbase.http.api.BaseApiResult;
import com.smallmitao.libbase.http.appinterface.HttpInter;
import com.smallmitao.libbase.manager.app.BaseUserManager;
import com.smallmitao.libbase.mvp.RxPresenter;
import com.smallmitao.libbase.util.Toastor;
import com.zhouyou.http.callback.SimpleCallBack;
import com.zhouyou.http.exception.ApiException;
import com.zhouyou.http.model.HttpParams;
import javax.inject.Inject;

/**
 * date: 2018/8/10
 * @author Jackshao
 *
 */

@FragmentScope
public class ShopPresenter extends RxPresenter<ShopContract.View> implements ShopContract.Presenter {

  private RetrofitHelper mHelper;

  @Inject
  public ShopPresenter(RetrofitHelper retrofitHelper) {
    this.mHelper = retrofitHelper;
  }


  @Override
  public void requestShop(String page,String pageSize) {
    HttpParams httpParams = new HttpParams();
//    httpParams.put(HttpInter.GoodsGetList.IS_ON_SALE,"1");
//    httpParams.put(HttpInter.GoodsGetList.REVIEW_STATUS,"3");
    httpParams.put(HttpInter.GoodsGetList.PAGE,page);
    httpParams.put(HttpInter.GoodsGetList.PAGE_SIZE,pageSize);
    addDisposable(mHelper
      .postRequest(HttpInter.GoodsGetList.PATH)
      .params(httpParams)
      .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getToken())
      .execute(new SimpleCallBack<ShopModel>() {

        @Override
        public void onError(ApiException e) {
          Toastor.showToast(e.getMessage());
        }

        @Override
        public void onSuccess(ShopModel o) {
          getView().getShopModel(true,o.getList());
        }
      }));

  }

  @Override
  public void requestUpdateGoods(String goodsId) {
    HttpParams httpParams = new HttpParams();
    httpParams.put(HttpInter.GoodsUpdate.IS_ON_SALE,"0");
    httpParams.put(HttpInter.GoodsUpdate.GOODS_ID,goodsId);
    addDisposable(mHelper
      .postRequest(HttpInter.GoodsUpdate.PATH)
      .params(httpParams)
      .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getToken())
      .execute(new SimpleCallBack<BaseApiResult>() {

        @Override
        public void onError(ApiException e) {
          Toastor.showToast(e.getMessage());
          getView().getUpdateGoods(false);
        }

        @Override
        public void onSuccess(BaseApiResult o) {
          getView().getUpdateGoods(true);
        }
      }));
  }
}
