package com.smallmitao.appfind.mvp.contract;


import com.smallmitao.appfind.bean.ShopModel;
import com.smallmitao.libbase.mvp.BasePresenter;
import com.smallmitao.libbase.mvp.BaseView;

import java.util.List;

/**
 * 商品
 * @author Jackshao
 */
public interface ShopContract {

    interface View extends BaseView {
        void getShopModel(boolean isOk, List<ShopModel.ListBean> mList);
        void getUpdateGoods(boolean isOk);
    }

    interface Presenter extends BasePresenter<View> {
        /**
         * 请求
         */
        void requestShop(String page,String pageSize);

        void requestUpdateGoods(String goodsId);
    }
}
