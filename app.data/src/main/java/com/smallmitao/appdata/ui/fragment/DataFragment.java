package com.smallmitao.appdata.ui.fragment;


import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.smallmitao.appdata.R;
import com.smallmitao.appdata.R2;
import com.smallmitao.appdata.bean.DataIndexBean;
import com.smallmitao.appdata.di.componet.DaggerDataFragmentComponent;
import com.smallmitao.appdata.di.componet.DataFragmentComponent;
import com.smallmitao.appdata.di.module.DataFragmentModule;
import com.smallmitao.appdata.mvp.DataPresenter;
import com.smallmitao.appdata.mvp.contract.DataContract;
import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.mvp.BaseMvpFragment;
import com.smallmitao.libbridge.router.BridgeRouter;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
@Route(path = BridgeRouter.STORE_FRAGMENT_DATA, name = "数据")
public class DataFragment extends BaseMvpFragment<DataPresenter> implements DataContract.View {


    @BindView(R2.id.title_tv)
    TextView titleTv;
    @BindView(R2.id.toolbar)
    Toolbar toolbar;
    @BindView(R2.id.receipt_money)
    TextView receiptMoney;
    @BindView(R2.id.receipt_num)
    TextView receiptNum;
    @BindView(R2.id.rewards_money)
    TextView rewardsMoney;
    @BindView(R2.id.rewards_num)
    TextView rewardsNum;
    @BindView(R2.id.client_num)
    TextView clientNum;
    Unbinder unbinder;

    @Override
    protected int getLayout() {
        return R.layout.fragment_data;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(toolbar).init();
    }

    @Override
    protected void initData() {
        titleTv.setText("数据");
        mPresenter.requestDataInfo();
    }


    @Override
    public void setDataInfo(DataIndexBean bean) {
        receiptMoney.setText(bean.getInData().getTotalPrice() + getString(R.string.yuan));
        receiptNum.setText(bean.getInData().getTotalNum() + getString(R.string.bi));
        rewardsMoney.setText(bean.getPriceData().getTotalPrice() + getString(R.string.yuan));
        rewardsNum.setText(bean.getPriceData().getTotalNum() + getString(R.string.bi));
        clientNum.setText(bean.getUserData().getTotalLevelNum() + "人");
    }

    @Override
    protected void initInject() {
        getFragmentComponent().inject(this);
    }

    public DataFragmentComponent getFragmentComponent() {
        return DaggerDataFragmentComponent.builder()
                .baseAppComponent(BaseApp.getInstance().getBaseAppComponent())
                .dataFragmentModule(new DataFragmentModule(this))
                .build();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R2.id.receipt_detail, R2.id.commission_detail, R2.id.manage_detail})
    public void onViewClicked(View view) {
        if (view.getId() == R.id.receipt_detail) {
            ARouter.getInstance().build(BridgeRouter.STORE_ACTIVITY_DATA_DETAIL)
                    .withInt("type",1).navigation();
        } else if (view.getId() == R.id.commission_detail) {
            ARouter.getInstance().build(BridgeRouter.STORE_ACTIVITY_DATA_DETAIL)
                    .withInt("type",2).navigation();
        } else if (view.getId() == R.id.manage_detail) {
            ARouter.getInstance().build(BridgeRouter.STORE_ACTIVITY_TEAM).navigation();
        }

    }


}
