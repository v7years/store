package com.smallmitao.appdata.ui.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.smallmitao.appdata.R;
import com.smallmitao.appdata.R2;
import com.smallmitao.appdata.bean.DataDetailBean;
import com.smallmitao.appdata.di.componet.DaggerDataDetailComponent;
import com.smallmitao.appdata.di.componet.DataDetailComponent;
import com.smallmitao.appdata.di.module.DataDetailModule;
import com.smallmitao.appdata.mvp.DataDetailPresenter;
import com.smallmitao.appdata.mvp.contract.DataDetailContract;
import com.smallmitao.appdata.ui.adapter.DataDetailAdapter;
import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.mvp.BaseMvpActivity;
import com.smallmitao.libbase.util.Toastor;
import com.smallmitao.libbridge.router.BridgeRouter;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

@Route(path = BridgeRouter.STORE_ACTIVITY_DATA_DETAIL, name = "收款数据")
public class DataDetailActivity extends BaseMvpActivity<DataDetailPresenter> implements DataDetailContract.View, OnRefreshListener, OnLoadMoreListener {


    @BindView(R2.id.back_btn)
    ImageView backBtn;
    @BindView(R2.id.title_tv)
    TextView titleTv;
    @BindView(R2.id.title_rightTv)
    TextView titleRightTv;
    @BindView(R2.id.toolbar)
    Toolbar toolbar;
    @BindView(R2.id.recycle_view)
    RecyclerView recycleView;
    @BindView(R2.id.refresh_layout)
    SmartRefreshLayout mRefreshLayout;

    @Autowired
    int type;
    private int mPage = 1;
    private int mSize = 20;
    private DataDetailAdapter mAdapter;
    private View mNotDataView;

    @Override
    protected int getLayout(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_data_detail;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(toolbar).init();
    }

    @Override
    protected void initInject() {
        getDataDetailComponent().inject(this);
    }

    public DataDetailComponent getDataDetailComponent() {
        return DaggerDataDetailComponent.builder()
                .baseAppComponent(BaseApp.getInstance().getBaseAppComponent())
                .dataDetailModule(new DataDetailModule(this))
                .build();
    }

    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        ARouter.getInstance().inject(this);
        mNotDataView = getLayoutInflater().inflate(R.layout.no_data_view, (ViewGroup) recycleView.getParent(), false);
        backBtn.setVisibility(View.VISIBLE);
        if (type == 1)
            titleTv.setText("收款数据");
        else {
            titleTv.setText("佣金数据");
        }
        mPresenter.getDataInfo(type,mPage,mSize);
        mAdapter = new DataDetailAdapter();
        recycleView.setLayoutManager(new LinearLayoutManager(this));
        recycleView.setAdapter(mAdapter);
    }

    @Override
    public void setDataInfo(boolean isOk, DataDetailBean infoBean) {
        if (isOk) {
            mRefreshLayout.finishRefresh(true);
            setData(infoBean.getList());
        } else {
            if (mPage == 1) {
                mRefreshLayout.finishRefresh(false);
            } else {
                mRefreshLayout.finishLoadMore(false);
            }
        }
    }

    private void setData(List<DataDetailBean.ListBean> data) {
        final int size = data == null ? 0 : data.size();
        boolean isRefresh = mPage == 1;
        if (isRefresh) {
            mAdapter.replaceData(data);
        } else {
            if (size > 0) {
                mAdapter.addData(data);
            }
        }
        if (size < mSize) {
            //第一页如果不够一页就不显示没有更多数据布局
            mRefreshLayout.finishLoadMoreWithNoMoreData();
            if (size == 0) {
                mAdapter.setEmptyView(mNotDataView);
            } else {
                if (mPage > 1) {
                    Toastor.showToast("没有更多数据了");
                }
            }
        } else {
            mRefreshLayout.finishLoadMore();
        }
    }

    @OnClick(R2.id.back_btn)
    public void onViewClicked() {
        finish();
    }

    @Override
    public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
        mPage++;
        mPresenter.getDataInfo(type, mPage, mSize);
    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {
        mPage = 1;
        mPresenter.getDataInfo(type, mPage, mSize);
    }
}
