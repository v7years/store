package com.smallmitao.appdata.ui.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.smallmitao.appdata.R;
import com.smallmitao.appdata.bean.DataDetailBean;

public class DataDetailAdapter extends BaseQuickAdapter<DataDetailBean.ListBean, BaseViewHolder> {

    public DataDetailAdapter() {
        super(R.layout.item_data);
    }

    @Override
    protected void convert(BaseViewHolder helper, DataDetailBean.ListBean item) {
        helper.setText(R.id.money, item.getChange_cash());
        helper.setText(R.id.time, item.getTime());
        helper.setText(R.id.desc, item.getDesc());
    }
}
