package com.smallmitao.appdata.ui.adapter;

import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.smallmitao.appdata.R;
import com.smallmitao.appdata.bean.TeamInfoBean;
import com.smallmitao.libbase.util.GlideUtils;

public class TeamAdapter extends BaseQuickAdapter<TeamInfoBean.ListBean, BaseViewHolder> {

    public TeamAdapter() {
        super(R.layout.item_team);
    }

    @Override
    protected void convert(BaseViewHolder helper, TeamInfoBean.ListBean item) {
        helper.setText(R.id.user_account, item.getMobile());
        helper.setText(R.id.user_level, item.getLevel());
        ImageView img_iv = helper.getView(R.id.user_head);
        GlideUtils.getInstance().loadCircleRadioImage(img_iv,item.getHeadimgurl(),5);
    }
}
