package com.smallmitao.appdata.ui.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.smallmitao.appdata.R;
import com.smallmitao.appdata.R2;
import com.smallmitao.appdata.bean.TeamInfoBean;
import com.smallmitao.appdata.di.componet.DaggerTeamComponent;
import com.smallmitao.appdata.di.componet.TeamComponent;
import com.smallmitao.appdata.di.module.TeamModule;
import com.smallmitao.appdata.mvp.TeamPresenter;
import com.smallmitao.appdata.mvp.contract.TeamContract;
import com.smallmitao.appdata.ui.adapter.TeamAdapter;
import com.smallmitao.libbase.base.BaseApp;
import com.smallmitao.libbase.mvp.BaseMvpActivity;
import com.smallmitao.libbase.util.Toastor;
import com.smallmitao.libbridge.router.BridgeRouter;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

@Route(path = BridgeRouter.STORE_ACTIVITY_TEAM, name = "团队管理")
public class TeamActivity extends BaseMvpActivity<TeamPresenter> implements TeamContract.View, OnRefreshListener, OnLoadMoreListener {


    @BindView(R2.id.back_btn)
    ImageView backBtn;
    @BindView(R2.id.title_tv)
    TextView titleTv;
    @BindView(R2.id.title_rightTv)
    TextView titleRightTv;
    @BindView(R2.id.toolbar)
    Toolbar toolbar;
    @BindView(R2.id.recycle_view)
    RecyclerView recycleView;
    @BindView(R2.id.refresh_layout)
    SmartRefreshLayout refreshLayout;

    private int mPage = 1;
    private int mSizePage = 20;
    private TeamAdapter mAdapter;
    private View mNotDataView;

    @Override
    protected int getLayout(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_team;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(toolbar).init();
    }

    @Override
    protected void initInject() {
        getTeamComponent().inject(this);
    }

    public TeamComponent getTeamComponent() {
        return DaggerTeamComponent.builder()
                .baseAppComponent(BaseApp.getInstance().getBaseAppComponent())
                .teamModule(new TeamModule(this))
                .build();
    }

    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        backBtn.setVisibility(View.VISIBLE);
        titleTv.setText("团队管理");
        mNotDataView = getLayoutInflater().inflate(R.layout.no_data_view, (ViewGroup) recycleView.getParent(), false);
        recycleView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new TeamAdapter();
        recycleView.setAdapter(mAdapter);
        mPresenter.getTeamInfo(mPage, mSizePage);
    }

    @OnClick(R2.id.back_btn)
    public void onViewClicked() {
        finish();
    }

    @Override
    public void setTeamInfo(boolean isOk, TeamInfoBean infoBean) {
        if (isOk) {
            refreshLayout.finishRefresh(true);
            setData(infoBean.getList());
        } else {
            if (mPage == 1) {
                refreshLayout.finishRefresh(false);
            } else {
                refreshLayout.finishLoadMore(false);
            }
        }
    }


    private void setData(List<TeamInfoBean.ListBean> data) {
        final int size = data == null ? 0 : data.size();
        boolean isRefresh = mPage == 1;
        if (isRefresh) {
            mAdapter.replaceData(data);
        } else {
            if (size > 0) {
                mAdapter.addData(data);
            }
        }
        if (size < mSizePage) {
            //第一页如果不够一页就不显示没有更多数据布局
            refreshLayout.finishLoadMoreWithNoMoreData();
            if (size == 0) {
                mAdapter.setEmptyView(mNotDataView);
            } else {
                if (mPage > 1) {
                    Toastor.showToast("没有更多数据了");
                }
            }
        } else {
            refreshLayout.finishLoadMore();
        }
    }

    @Override
    public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
        mPage++;
        mPresenter.getTeamInfo(mPage, mSizePage);
    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {
        mPage = 1;
        mPresenter.getTeamInfo(mPage, mSizePage);
    }
}
