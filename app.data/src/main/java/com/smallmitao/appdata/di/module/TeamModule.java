package com.smallmitao.appdata.di.module;

import android.app.Activity;

import com.smallmitao.appdata.ui.adapter.TeamAdapter;
import com.smallmitao.libbase.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

@Module
public class TeamModule {
    private Activity mActivity;

    public TeamModule(Activity activity) {
        this.mActivity = activity;
    }

    @ActivityScope
    @Provides
    Activity getActivity() {
        return mActivity;
    }

    @ActivityScope
    @Provides
    TeamAdapter getTeamAdapter(){
        return  new TeamAdapter();
    }
}
