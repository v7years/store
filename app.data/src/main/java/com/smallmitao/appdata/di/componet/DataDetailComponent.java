package com.smallmitao.appdata.di.componet;

import android.app.Activity;

import com.smallmitao.appdata.di.module.DataDetailModule;
import com.smallmitao.appdata.ui.activity.DataDetailActivity;
import com.smallmitao.libbase.di.component.BaseAppComponent;
import com.smallmitao.libbase.di.scope.ActivityScope;

import dagger.Component;

@ActivityScope
@Component(dependencies = BaseAppComponent.class, modules = DataDetailModule.class)
public interface DataDetailComponent {
    Activity getActivity();

    void inject(DataDetailActivity dataDetailActivity);
}
