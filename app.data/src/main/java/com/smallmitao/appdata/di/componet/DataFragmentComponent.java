package com.smallmitao.appdata.di.componet;

import android.app.Activity;

import com.smallmitao.appdata.di.module.DataFragmentModule;
import com.smallmitao.appdata.ui.fragment.DataFragment;
import com.smallmitao.libbase.di.component.BaseAppComponent;
import com.smallmitao.libbase.di.scope.FragmentScope;

import dagger.Component;

@FragmentScope
@Component(dependencies = BaseAppComponent.class,modules = DataFragmentModule.class)
public interface DataFragmentComponent {
    Activity getActivity();

    void inject(DataFragment dataFragment);
}
