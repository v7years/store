package com.smallmitao.appdata.di.componet;

import android.app.Activity;

import com.smallmitao.appdata.di.module.TeamModule;
import com.smallmitao.appdata.ui.activity.TeamActivity;
import com.smallmitao.libbase.di.component.BaseAppComponent;
import com.smallmitao.libbase.di.scope.ActivityScope;

import dagger.Component;

@ActivityScope
@Component(dependencies = BaseAppComponent.class, modules = TeamModule.class)
public interface TeamComponent {
    Activity getActivity();

    void inject(TeamActivity teamActivity);
}
