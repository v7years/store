package com.smallmitao.appdata.di.module;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.smallmitao.libbase.di.scope.FragmentScope;

import dagger.Module;
import dagger.Provides;

@Module
public class DataFragmentModule {
    private Fragment mFragment;

    public DataFragmentModule(Fragment fragment) {
        this.mFragment = fragment;
    }

    @FragmentScope
    @Provides
    Activity provideActivity() {
        return mFragment.getActivity();
    }
}
