package com.smallmitao.appdata.di.module;

import android.app.Activity;

import com.smallmitao.appdata.ui.adapter.DataDetailAdapter;
import com.smallmitao.libbase.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

@Module
public class DataDetailModule {
    private Activity mActivity;

    public DataDetailModule(Activity activity) {
        this.mActivity = activity;
    }

    @ActivityScope
    @Provides
    Activity getActivity() {
        return mActivity;
    }

    @ActivityScope
    @Provides
    DataDetailAdapter getDataDetailAdapter(){
        return  new DataDetailAdapter();
    }
}
