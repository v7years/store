package com.smallmitao.appdata.mvp.contract;

import com.smallmitao.appdata.bean.DataIndexBean;
import com.smallmitao.libbase.mvp.BasePresenter;
import com.smallmitao.libbase.mvp.BaseView;

public interface DataContract {
    interface View extends BaseView {
        void setDataInfo(DataIndexBean indexBean);
    }

    interface Presenter extends BasePresenter<View> {
        void requestDataInfo();
    }
}
