package com.smallmitao.appdata.mvp;

import com.smallmitao.appdata.bean.DataDetailBean;
import com.smallmitao.appdata.mvp.contract.DataDetailContract;
import com.smallmitao.libbase.http.RetrofitHelper;
import com.smallmitao.libbase.http.appinterface.HttpInter;
import com.smallmitao.libbase.manager.app.BaseUserManager;
import com.smallmitao.libbase.mvp.RxPresenter;
import com.smallmitao.libbase.util.Toastor;
import com.zhouyou.http.callback.SimpleCallBack;
import com.zhouyou.http.exception.ApiException;

import javax.inject.Inject;

public class DataDetailPresenter extends RxPresenter<DataDetailContract.View> implements DataDetailContract.Presenter {

    private RetrofitHelper mHelper;

    @Inject
    public DataDetailPresenter(RetrofitHelper helper) {
        this.mHelper = helper;
    }

    @Override
    public void getDataInfo(int type,int page,int pageSize) {
        addDisposable(mHelper
                .getRequest(HttpInter.StoreDataInfo.PATH)
                .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getStoreToken())
                .params(HttpInter.StoreDataInfo.TYPE,String.valueOf(type))
                .params(HttpInter.StoreDataInfo.PAGE,String.valueOf(page))
                .params(HttpInter.StoreDataInfo.PAGE_SIZE,String.valueOf(pageSize))
                .execute(new SimpleCallBack<DataDetailBean>() {

                    @Override
                    public void onError(ApiException e) {
                        Toastor.showToast(e.getMessage());
                        getView().setDataInfo(false, null);
                    }

                    @Override
                    public void onSuccess(DataDetailBean o) {
                        getView().setDataInfo(true, o);
                    }
                }));
    }
}
