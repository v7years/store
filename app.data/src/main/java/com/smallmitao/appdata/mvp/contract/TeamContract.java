package com.smallmitao.appdata.mvp.contract;

import com.smallmitao.appdata.bean.TeamInfoBean;
import com.smallmitao.libbase.mvp.BasePresenter;
import com.smallmitao.libbase.mvp.BaseView;

public class TeamContract {
    public interface View extends BaseView {
        void setTeamInfo(boolean isOk, TeamInfoBean infoBean);
    }

   public interface Presenter extends BasePresenter<View> {
        void getTeamInfo(int page,int pageSize);
    }
}
