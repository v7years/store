package com.smallmitao.appdata.mvp.contract;

import com.smallmitao.appdata.bean.DataDetailBean;
import com.smallmitao.libbase.mvp.BasePresenter;
import com.smallmitao.libbase.mvp.BaseView;

public class DataDetailContract {
    public interface View extends BaseView {
        void setDataInfo(boolean isOk, DataDetailBean infoBean);
    }

   public interface Presenter extends BasePresenter<View> {
        void getDataInfo(int type,int page,int pageSize );
    }
}
