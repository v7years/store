package com.smallmitao.appdata.mvp;

import com.smallmitao.appdata.bean.TeamInfoBean;
import com.smallmitao.appdata.mvp.contract.TeamContract;
import com.smallmitao.libbase.http.RetrofitHelper;
import com.smallmitao.libbase.http.appinterface.HttpInter;
import com.smallmitao.libbase.manager.app.BaseUserManager;
import com.smallmitao.libbase.mvp.RxPresenter;
import com.smallmitao.libbase.util.Toastor;
import com.zhouyou.http.callback.SimpleCallBack;
import com.zhouyou.http.exception.ApiException;

import javax.inject.Inject;

public class TeamPresenter extends RxPresenter<TeamContract.View> implements TeamContract.Presenter {


    private RetrofitHelper mHelper;

    @Inject
    public TeamPresenter(RetrofitHelper helper) {
        this.mHelper = helper;
    }

    @Override
    public void getTeamInfo(int page, int pageSize) {
        addDisposable(mHelper
                .getRequest(HttpInter.StoreDataInfo.PATH)
                .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getStoreToken())
                .params(HttpInter.StoreDataInfo.TYPE, String.valueOf(3))
                .params(HttpInter.StoreDataInfo.PAGE, String.valueOf(page))
                .params(HttpInter.StoreDataInfo.PAGE_SIZE, String.valueOf(pageSize))
                .execute(new SimpleCallBack<TeamInfoBean>() {

                    @Override
                    public void onError(ApiException e) {
                        Toastor.showToast(e.getMessage());
                        getView().setTeamInfo(false, null);
                    }

                    @Override
                    public void onSuccess(TeamInfoBean o) {
                        getView().setTeamInfo(true, o);
                    }
                }));
    }
}
