package com.smallmitao.appdata.mvp;

import com.smallmitao.appdata.bean.DataIndexBean;
import com.smallmitao.appdata.mvp.contract.DataContract;
import com.smallmitao.libbase.di.scope.FragmentScope;
import com.smallmitao.libbase.http.HttpHelper;
import com.smallmitao.libbase.http.RetrofitHelper;
import com.smallmitao.libbase.http.appinterface.HttpInter;
import com.smallmitao.libbase.manager.app.BaseUserManager;
import com.smallmitao.libbase.mvp.RxPresenter;
import com.smallmitao.libbase.util.Toastor;
import com.zhouyou.http.callback.SimpleCallBack;
import com.zhouyou.http.exception.ApiException;

import javax.inject.Inject;

@FragmentScope
public class DataPresenter extends RxPresenter<DataContract.View> implements DataContract.Presenter {

    private RetrofitHelper mHelper;

   @Inject
    public DataPresenter(RetrofitHelper httpHelper) {
        this.mHelper = httpHelper;
    }

    @Override
    public void requestDataInfo() {

        addDisposable(mHelper
                .getRequest(HttpInter.DataIndexInfo.PATH)
                .headers(HttpInter.Common.ACCESSTOKEN, BaseUserManager.getInstance().getStoreToken())
                .execute(new SimpleCallBack<DataIndexBean>() {

                    @Override
                    public void onError(ApiException e) {
                        Toastor.showToast(e.getMessage());
                    }

                    @Override
                    public void onSuccess(DataIndexBean o) {
                        getView().setDataInfo(o);
                    }
                }));
    }
}
