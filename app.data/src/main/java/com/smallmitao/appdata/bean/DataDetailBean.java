package com.smallmitao.appdata.bean;

import java.util.List;

public class DataDetailBean {

    private List<ListBean> list;

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * change_cash : -3.00
         * desc : 账户余额取现
         * time : 2019年02月22日 17:17:52
         */

        private String change_cash;
        private String desc;
        private String time;

        public String getChange_cash() {
            return change_cash;
        }

        public void setChange_cash(String change_cash) {
            this.change_cash = change_cash;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }
    }
}
