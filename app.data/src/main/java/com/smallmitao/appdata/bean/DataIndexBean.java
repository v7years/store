package com.smallmitao.appdata.bean;

public class DataIndexBean {

    /**
     * InData : {"totalPrice":"12.00","totalNum":1}
     * PriceData : {"totalPrice":0,"totalNum":0}
     * UserData : {"totalLevelNum":0}
     */

    private InDataBean InData;
    private PriceDataBean PriceData;
    private UserDataBean UserData;

    public InDataBean getInData() {
        return InData;
    }

    public void setInData(InDataBean InData) {
        this.InData = InData;
    }

    public PriceDataBean getPriceData() {
        return PriceData;
    }

    public void setPriceData(PriceDataBean PriceData) {
        this.PriceData = PriceData;
    }

    public UserDataBean getUserData() {
        return UserData;
    }

    public void setUserData(UserDataBean UserData) {
        this.UserData = UserData;
    }

    public static class InDataBean {
        /**
         * totalPrice : 12.00
         * totalNum : 1
         */

        private String totalPrice;
        private int totalNum;

        public String getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(String totalPrice) {
            this.totalPrice = totalPrice;
        }

        public int getTotalNum() {
            return totalNum;
        }

        public void setTotalNum(int totalNum) {
            this.totalNum = totalNum;
        }
    }

    public static class PriceDataBean {
        /**
         * totalPrice : 0
         * totalNum : 0
         */

        private double totalPrice;
        private int totalNum;

        public double getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(double totalPrice) {
            this.totalPrice = totalPrice;
        }

        public int getTotalNum() {
            return totalNum;
        }

        public void setTotalNum(int totalNum) {
            this.totalNum = totalNum;
        }
    }

    public static class UserDataBean {
        /**
         * totalLevelNum : 0
         */

        private int totalLevelNum;

        public int getTotalLevelNum() {
            return totalLevelNum;
        }

        public void setTotalLevelNum(int totalLevelNum) {
            this.totalLevelNum = totalLevelNum;
        }
    }
}
