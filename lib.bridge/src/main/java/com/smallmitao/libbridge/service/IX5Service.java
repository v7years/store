package com.smallmitao.libbridge.service;

import android.content.Context;

import com.smallmitao.libbridge.router.base.BaseRouterIService;


/**
 * 启动x5服务
 * @author Jackshao
 */
public interface IX5Service extends BaseRouterIService {

    void initX5(Context mContext);
}
