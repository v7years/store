package com.smallmitao.libbridge.router.service;

import android.content.Context;

import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.facade.service.DegradeService;

/**
 * 路由降级（找不到路由页面的时候调用）
 * @author Jackshao
 * @date 2018/8/30
 */

@Route(path = "/app/degrade",name = "全局降级策略")
public class AppDegradeServiceImpl implements DegradeService {

  /**
   * 找不到路由则回调这个方法(可以考虑跳转到错误页面或者其他页面)
   * @param context
   * @param postcard
   *  ARouter.getInstance().build("/router/login").navigation();
   */
  @Override
  public void onLost(Context context, Postcard postcard) {
  }

  @Override
  public void init(Context context) {
  }
}