package com.smallmitao.libbridge.router.base;


/**
 * @author Jackshao
 */
public interface ServiceRouterCallBack<T> {
   void callBack(T t);
}
