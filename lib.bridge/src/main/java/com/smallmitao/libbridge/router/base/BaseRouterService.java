package com.smallmitao.libbridge.router.base;


import android.content.Context;

/**
 * @author Jackshao
 */
public class BaseRouterService<T>  implements BaseRouterIService<T>{

   protected ServiceRouterCallBack serviceCallBack;

   @Override
   public void getDataBack(ServiceRouterCallBack serviceCallBack) {
      this.serviceCallBack = serviceCallBack;
   }
   @Override
   public void init(Context context) {

   }

   @Override
   public void setDataBack(T t) {
      if (null != serviceCallBack) {
         serviceCallBack.callBack(t);
      }
   }
}
