package com.smallmitao.libbridge.router;

/**
 *
 * 传递数据的常量
 * @author Jackshao
 * @date 2018/9/26
 */

public final class BridgePass {

  public static final String SERVICE_PASS_LOGIN = "/service/pass_login";
  public static final String SERVICE_PASS_TRANSACTION = "/service2_transaction/pass_transaction";
  public static final String SERVICE_PASS_USER_INFO_KYC = "/service3_user_info_kyc/pass_user_info_kyc";
  public static final String SERVICE_PASS_WEB_X5 = "/service4_x5/web_x5";

}
