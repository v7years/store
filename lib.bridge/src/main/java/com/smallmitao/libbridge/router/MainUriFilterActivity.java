package com.smallmitao.libbridge.router;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.facade.callback.NavCallback;
import com.alibaba.android.arouter.launcher.ARouter;

/**
 *
 * 总路由开关
 * @author Jackshao
 * @date 2018/8/30
 */

public class MainUriFilterActivity extends Activity {

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
//        直接通过ARouter处理外部Uri
    Uri uri = getIntent().getData();
    ARouter.getInstance().build(uri).navigation(this, new NavCallback() {
      @Override
      public void onArrival(Postcard postcard) {
        finish();
      }
    });
  }
}
