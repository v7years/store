package com.smallmitao.libbridge.router.base;


import com.alibaba.android.arouter.facade.template.IProvider;

/**
 * @author Jackshao
 */
public interface BaseRouterIService<T>  extends IProvider {

   void getDataBack(ServiceRouterCallBack serviceCallBack);

   void setDataBack(T t);
}
