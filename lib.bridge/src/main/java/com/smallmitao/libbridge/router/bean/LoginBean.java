package com.smallmitao.libbridge.router.bean;

import java.io.Serializable;

/**
 *
 * @author Jackshao
 * @date 2019/1/23
 */

public class LoginBean implements Serializable {

  /**
   * token : eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9kZXZlbG9wLnNob3BhcGkuc21hbGxtaXRhby5jb21cL2F1dGhcL2xvZ2luIiwiaWF0IjoxNTQ4Mjk4MTE3LCJleHAiOjE1NDgyOTgxNzcsIm5iZiI6MTU0ODI5ODExNywianRpIjoiWDd2cFdGNDdZUHEzMzV4MSIsInN1YiI6NTc1MDEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.uB9ihalHPe9XwFTiioUmodlcDYcbUA3F6H9SqgIJ-ko
   * status : 0
   * reason :
   */

  private String token;
  private int status;
  private String reason;

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public String getReason() {
    return reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }
}
