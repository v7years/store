package com.smallmitao.libbridge.router.bean;


/**
 *
 * @author Jackshao
 * @date 2019/1/23
 */

public class UpLoadBean {

  /**
   * url : http://xmtb.oss-cn-hangzhou.aliyuncs.com/upload/images/20190124/shop/8d1c462720c699b5340b827b3e4453a8.jpeg?OSSAccessKeyId=LTAIhjHFwn061f0Q&Expires=4701916045&Signature=d7s1p36mU5oiK3hI15FZAAJwLnE%3D
   */

  private String url;

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }
}
