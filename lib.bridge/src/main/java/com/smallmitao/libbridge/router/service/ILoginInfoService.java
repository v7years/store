package com.smallmitao.libbridge.router.service;


import com.smallmitao.libbridge.router.base.BaseRouterIService;
import com.smallmitao.libbridge.router.bean.LoginBean;
import com.smallmitao.libbridge.router.bean.StoreLoginBean;

/**
 * 登录信息
 * @author Jackshao
 */
public interface ILoginInfoService extends BaseRouterIService {

    /**
     * 是否已经登录
     *
     * @return
     */
    boolean isLogin();

    /**
     * 等待用户信息
     * @return
     */
    LoginBean getLoginInfo();


    /**
     * 用户退出
     */
    void setLoginOut();



    /**
     *实体店
     */

    /**
     * 是否已经登录
     *
     * @return
     */
    boolean isStoreLogin();

    /**
     * 等待用户信息
     * @return
     */
    StoreLoginBean getStoreLoginInfo();


    /**
     * 用户退出
     */
    void setStoreLoginOut();
}
