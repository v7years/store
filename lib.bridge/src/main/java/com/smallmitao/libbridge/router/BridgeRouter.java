package com.smallmitao.libbridge.router;

/**
 * app路由
 *
 * @author Jackshao
 * @date 2018/9/4
 */
public final class BridgeRouter {

    /**
     * app层
     * 主页
     */
    public static final String PAGE_MAIN = "/smallmitao_business/main";
    public static final String PAGE_MAIN_ACTIVITY_LOGIN = "/app_business/activity_login";
    public static final String PAGE_MAIN_ACTIVITY_IDENTITY = "/app_business/activity_selective_identity";
    public static final String PAGE_MAIN_ACTIVITY_STORE_VERIFY = "/app_business/activity_verify";
    public static final String PAGE_ACTIVITY_STORE_MAIN = "/app_business/activity_main";


    /**
     * 供应商 模块app.home(首页)
     */
    public static final String PAGE_FRAGMENT_HOME = "/app_home/fragment_homepage";
    public static final String PAGE_ACTIVITY_QR_CODE = "/app_home/activity_qr_code";
    public static final String PAGE_ACTIVITY_ADD_SHOP = "/app_home/activity_add_shop";
    public static final String PAGE_ACTIVITY_RETURN_GOODS = "/app_home/activity_return_goods";
    public static final String PAGE_ACTIVITY_APPLY_SHOP = "/app_home/activity_apply_shop";
    public static final String PAGE_ACTIVITY_HAND_ID_CARD = "/app_home/activity_hand_id_card";
    public static final String PAGE_ACTIVITY_QUA_CER = "/app_home/activity_qua_cer";
    public static final String PAGE_ACTIVITY_APPLY = "/app_home/activity_apply";

    /**
     * 模块app.find(发现)
     */
    public static final String PAGE_FRAGMENT_FIND = "/app_find/fragment_find";


    /**
     * 模块app.vip(中心)
     */
    public static final String PAGE_FRAGMENT_VIP = "/app_vip/fragment_vip";

    /**
     * 模块app.mine(我的)
     */
    public static final String PAGE_FRAGMENT_MINE = "/app_mine/fragment_mine";
    public static final String PAGE_ACTIVITY_PENDING_ENTRY = "/app_mine/activity_pending_entry";
    public static final String PAGE_FRAGMENT_PENDING_ENTRY = "/app_mine/fragment_pending_entry";
    public static final String PAGE_ACTIVITY_CASH_WITHDRAWAL = "/app_mine/fragment_cash_withdrawal";
    public static final String PAGE_ACTIVITY_MY_SHOP_INFO = "/app_mine/activity_my_shop_info";


    /*商家*/

    /**
     * 模块app.find(收款)
     */
    public static final String STORE_FRAGMENT_WITHDRAW_CODE = "/app_receipt/fragment_receipt";


    /**
     * 模块app.data(数据)
     */
    public static final String STORE_FRAGMENT_DATA = "/app_data/fragment_vip";
    public static final String STORE_ACTIVITY_TEAM = "/app_data/activity_team";
    public static final String STORE_ACTIVITY_DATA_DETAIL = "/app_data/activity_data_detail";

    /**
     * 模块app.my(我的)
     */
    public static final String STORE_FRAGMENT_MY = "/app_my/fragment_my";
    public static final String STORE_MY_ACTIVITY_SHOP_CENTER = "/app_my/activity_shop_center";
    public static final String STORE_ACTIVITY_WITHDRAWAL = "/app_my/activity_withdrawal";
    public static final String STORE_ACTIVITY_WITHDRAWAL_RESULT = "/app_my/activity_withdrawal_result";
    public static final String STORE_ACTIVITY_NOVICES_REWARD = "/app_my/activity_novices_reward";
    public static final String STORE_ACTIVITY_WITHDRAWAL_RECORD = "/app_my/activity_withdrawal_record";
}
