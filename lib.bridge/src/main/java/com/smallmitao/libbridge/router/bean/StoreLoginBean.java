package com.smallmitao.libbridge.router.bean;

import java.io.Serializable;

/**
 *
 * @author Jackshao
 * @date 2019/1/23
 */

public class StoreLoginBean implements Serializable {

  private String token;
  private long store_id;
  private long parent_user_no;

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public long getStore_id() {
    return store_id;
  }

  public void setStore_id(long store_id) {
    this.store_id = store_id;
  }

  public long getParent_user_no() {
    return parent_user_no;
  }

  public void setParent_user_no(long parent_user_no) {
    this.parent_user_no = parent_user_no;
  }
}
