package com.smallmitao.libbridge.router.interceptor;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.facade.annotation.Interceptor;
import com.alibaba.android.arouter.facade.callback.InterceptorCallback;
import com.alibaba.android.arouter.facade.template.IInterceptor;
import com.alibaba.android.arouter.launcher.ARouter;
import com.smallmitao.libbridge.router.BridgePass;
import com.smallmitao.libbridge.router.BridgeRouter;

/**
 * 路由拦截器
 * @author Jackshao
 * @date 2018/8/30
 *
 * 1. 添加拦截器的时候，建议clean再打包运行，不然会出现，无效的情况
 * 2. 切记一个项目里面priority的值保证唯一，不然会出毛病
 *
 */

@Interceptor(priority = 1, name = "拦截器")
public class IntentInterceptor implements IInterceptor {

  /**
   *  回调是子线程
   *  1. callback.onInterrupt(new RuntimeException("账号未登录")); //拦截
   *  2. callback.onContinue(postcard); //不拦截，走onLost方法
   *  3. postcard.withString("extra", "我是在拦截器中附加的参数"）；
   *
   *  4.MainLooper.runOnUiThread(new Runnable()  {}); //更新UI
   */
  @Override
  public void process(Postcard postcard, InterceptorCallback callback) {
    String path = postcard.getPath();
//    ILoginInfoService mILoginInfo = (ILoginInfoService) ARouter.getInstance().build(BridgePass.SERVICE_PASS_LOGIN).navigation();
    if (TextUtils.isEmpty(path)){
      callback.onContinue(postcard);
    }else {
      switch (path){
           //个人中心
//        case BridgeRouter.PAGE_MY_INFO:
//          //资产管理
//        case BridgeRouter.PAGE_ASSET_MANAGEMENT:
//          //账户安全
//        case BridgeRouter.PAGE_SECURITY_ACCOUNT:
//          //充币提币
//        case BridgeRouter.PAGE_CENTS_TOP_UP_COIN:
//          //邀请分布
//        case BridgeRouter.PAGE_TRADE_INVITATION:
//          if (mILoginInfo.isLogin()){
//            callback.onContinue(postcard);
//          }else {
//            ARouter.getInstance()
//              .build(Uri.parse(BridgeRouter.PAGE_LOGIN))
//              .greenChannel()
//              .navigation();
//            callback.onInterrupt(new RuntimeException("账号未登录"));
//          }
//          break;
        default:
          callback.onContinue(postcard);
          break;
      }
    }
  }

  @Override
  public void init(Context context) {
  }

}
