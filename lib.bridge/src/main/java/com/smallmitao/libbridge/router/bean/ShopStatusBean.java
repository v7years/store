package com.smallmitao.libbridge.router.bean;

import java.io.Serializable;

/**
 *
 * @author Jackshao
 * @date 2019/1/23
 */

public class ShopStatusBean extends StoreLoginBean implements Serializable{

  /**
   * status : 2
   * reason :
   */

  private int status;
  private String reason;

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public String getReason() {
    return reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }
}
